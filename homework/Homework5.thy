chapter \<open>Homework 5\<close>
theory Homework5
imports Main
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #3
    RELEASED: Mon, Dec 13 2021
    DUE:      Tue, Dec 21, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)

section \<open>1: Array Map (25 Points)\<close>

(*
  In this exercise we define an array map. The array map maps a natural number to some value of 
  type \<open>'v\<close>. Upon looking up a natural number i, the array map returns a value as an option, 
  which is \<open>None\<close> in case the value is not in the map or \<open>Some v\<close> if value v is mapped to i.
  We can retrieve a value from the array map using the \<open>am_\<alpha>\<close> definition.
  
  Note that we represent the array as a list here: while there are no (destructively updated) arrays 
  in Isabelle, we can play some tricks to actually use arrays for lists when we generate code in, 
  e.g., Standard ML. However, this trickery is not part of the homework: here, you are going to 
  prove the idea of how to represent a map by am array-like data structure (i.e., one that you can 
  enlarge, index, and update at an index)
*)

type_synonym 'v am = "'v option list"

definition am_\<alpha> :: "'v am \<Rightarrow> (nat \<Rightarrow> 'v option)" where
  "am_\<alpha> m i = (if i<length m then (m!i) else None)"


(*
  1a) Show: First of all, an empty map must evaluate to \<open>None\<close> for each number we look up. (5 pts)
*)

definition "am_empty \<equiv> []"  

lemma "am_\<alpha> am_empty = (\<lambda> _. None)"
(*<*)
  oops
(*>*)


(*
  1b) Specify and show: We also want to be able to insert elements into our map. For this we define 
    \<open>am_insert\<close>. This function should map the integer to the specified value regardless of the
    size of the underlying list. If the list is not large enough, you have to grow it, e.g., 
    by appending None: \<open>xs @ replicate \<dots> None\<close>
    (10 pts)   
  HINT: for a list \<open>xs\<close>, \<open>xs[i := v]\<close> sets the value of the i-th element to value \<open>v\<close>. Similar 
    syntax can be used for functions, where \<open>f(i := v)\<close> sets the output of \<open>f\<close> to \<open>v\<close> for input 
    \<open>i\<close>. \<open>f(i \<mapsto> v)\<close> equals \<open>f(i := Some v)\<close> (Note that we use parentheses now instead of square
    brackets).
    Also, take a look at the lemmas  below, they may be useful. 
*)
thm fun_eq_iff nth_append nth_Cons'

(* Hint: Start with a function am_insert_aux, which only works if the current list is large enough.
  you'll get 7pts if you only complete this.
*)

definition am_insert_aux :: "nat \<Rightarrow> 'v \<Rightarrow> 'v am \<Rightarrow> 'v am" where
(*<*)
  "am_insert_aux i x m \<equiv> undefined"
(*>*)


lemma am_insert_aux_correct: "i<length m \<Longrightarrow> am_\<alpha> (am_insert_aux i x m) = (am_\<alpha> m)(i \<mapsto> x)"
(*<*)
  oops
(*>*)
  
(*
  Then amend your function to grow the list in case the index is out of bounds of the current list!
  You'll get the remaining 3pt for this. 
*)  

(*<*)
(* AUXILIARY LEMMAS *)
(*>*)

definition am_insert :: "nat \<Rightarrow> 'v \<Rightarrow> 'v am \<Rightarrow> 'v am" where 
(*<*)
  "am_insert i x m \<equiv> undefined"
(*>*)

lemma am_insert_correct: "am_\<alpha> (am_insert i x m) = (am_\<alpha> m)(i \<mapsto> x)"
(*<*)
  oops
(*>*)


(*
  1c) Specify and show: Along the same lines, we want to be able to remove elements. Removing
    an entry for number \<open>i\<close> means that \<open>am_\<alpha> m i = None\<close>. 
    
    Specify and prove the correctness lemma for the delete operation!
    
    (10 pts.)
*)

(* Hint: *) term \<open>f(x:=None)\<close>


definition am_delete :: "nat \<Rightarrow> 'v am \<Rightarrow> 'v am" where 
(*<*)
  "am_delete i m \<equiv> undefined"
(*>*)

(*<*)
(* AUXILIARY LEMMAS *)
(*>*)

lemma am_delete_correct: "undefined Specify and prove this lemma"
(*<*)
  oops
(*>*)


section \<open>2: Left derivation (15 pts.)\<close>

(*
  We are going to look at languages. A language is a set of words where a word is simply a list
  of characters. Characters can be regular letters, but in the most general definition they can
  have any type.
  We can append two languages, which yields a new language which includes exactly the set of words 
  that we obtain by appending any word from the second language to any word of the first language.
  Last of all we can define a left derivative of a language. 
  The left derivative wrt. a character \<open>c\<close>
  of the language \<open>L\<close> yields a language that contains all words such that if we prepend \<open>c\<close> to it, then
  the word is in \<open>L\<close>
*)

text \<open>Hint: \<^url>\<open>https://en.wikipedia.org/wiki/Brzozowski_derivative\<close>
  (though this homework is significantly simplified!)
\<close>


type_synonym 'a lang = \<open>'a list set\<close>

definition "lappend L\<^sub>1 L\<^sub>2 \<equiv> {w\<^sub>1@w\<^sub>2 | w\<^sub>1 w\<^sub>2. w\<^sub>1\<in>L\<^sub>1 \<and> w\<^sub>2\<in>L\<^sub>2}"
definition "lchar c \<equiv> {[c]}"

definition "anychar \<equiv> \<Union>(lchar`UNIV)"

definition "lderiv c L \<equiv> { w . c#w \<in> L}"

(*
  2a) Show: The following lemmas can be used in the bonus exercise. Try to solve them all.
  HINT: Make sure you understand what these lemmas do. It will help you with the bonus exercise
  \<open>[]\<close> represents the empty word (like the empty string "" in programming languages). The empty
  language \<open>{}\<close> is not the same as the language containing only the empty word \<open>{[]}\<close>. The language
  \<open>UNIV\<close> is the language containing every word that can be constructed using the characters of the
  specified type.
*)

lemma Cons_in_L_deriv: "x#xs\<in>L \<longleftrightarrow> xs\<in>lderiv x L"
(*<*)
  oops
(*>*)
  
lemma in_L_deriv: "xs\<in>L \<longleftrightarrow> [] \<in> fold lderiv xs L"
(*<*)  
  oops
(*>*)


lemma ld_empty[simp]: "lderiv c {} = {}" 
(*<*)
  oops
(*>*)

lemma ld_eps[simp]: "lderiv c {[]} = {}"
(*<*)
  oops
(*>*)

lemma ld_univ[simp]: "lderiv c UNIV = UNIV"
(*<*)
  oops
(*>*)

lemma ld_char[simp]: "lderiv c (lchar d) = (if c=d then {[]} else {})" 
(*<*)
  oops
(*<*)

lemma ld_append[simp]: "lderiv c (lappend L\<^sub>1 L\<^sub>2) = lappend (lderiv c L\<^sub>1) L\<^sub>2 \<union> (if []\<in>L\<^sub>1 then lderiv c L\<^sub>2 else {})"
(*<*)
  oops
(*>*)

lemma ld_cont: "lderiv c (\<Union>Ls) = \<Union>(lderiv c`Ls)" 
(*<*)
  oops
(*>*)

lemma ld_comm_un: "lderiv c (L\<^sub>1\<union>L\<^sub>2) = lderiv c L\<^sub>1 \<union> lderiv c L\<^sub>2" 
(*<*)
  oops
(*>*)

lemma ld_anychar[simp]: "lderiv c anychar = {[]}" 
(*<*)
  oops
(*>*)

lemma add_simps[simp]: 
  "[]\<notin>anychar"
  "[]\<notin>lchar c"
  "lappend {} L = {}"
  "lappend L {} = {}"
  "lappend {[]} L = L"
  "lappend L {[]} = L"
(*<*)
  oops
(*<*)

lemma lappend_assoc[simp]: "lappend (lappend a b) c = lappend a (lappend b c)"  
(*<*)
  oops
(*>*)
    
lemma lappend_UNIV_idem[simp]: "lappend UNIV UNIV = UNIV"
(*<*)
  oops
(*>*)

lemma lappend_UNIV_idem_left[simp]: "lappend UNIV (lappend UNIV a) = lappend UNIV a"  
(*<*)
  oops
(*>*)

section \<open>Bonus: Apply derivatives to glob patterns (5 pts.)\<close>

(*
  We can apply the left defivative to check glob patterns. Glob patterns are often used to find
  filenames using wildcard characters. * (STAR) matches any string. ? (ANY) matches any single
  character and a character (CR a) in a glob pattern matches exactly this character in the input.
  For example: the glob pattern Homework?.* matches the file Homework5.thy, but also Homework4.thy
  and Homework2.pdf.
  The syntax for glob patterns according to our specification is given below. We represent a glob
  pattern as a list of these glob characters.
*)

datatype 'a globc = ANY | STAR | CR 'a
type_synonym 'a glob = \<open>'a globc list\<close>

(*
  a) Specify and show: A function that gives the language accepted by a glob pattern and prove 
    that the empty glob pattern only accepts the empty string. (1 pt.)
*)


definition L_glob :: "'a glob \<Rightarrow> 'a lang" where 
(*<*)
  "L_glob gl \<equiv> undefined"
(*>*)

lemma [simp]: "L_glob [] = {[]}" 
(*<*)
  oops
(*>*)

(*
  b) Specify and show: a function which determines whether a glob pattern accepts the empty word.
    Show that the function works correctly. (1 pt.)
*)

fun g_acc_empty :: "'a glob \<Rightarrow> bool" where
(*<*)
  "g_acc_empty _ = undefined"
(*>*)


lemma g_acc_empty_correct: "g_acc_empty gcs \<longleftrightarrow> [] \<in> L_glob gcs"  
(*<*)
  oops
(*>*)


(* 
  c) Specify and show: a function which determines whether a glob pattern matches a given string.
    Then show that this matching algorithm is correct. (3 pts.)
  HINT: You can go for the naive solution, even if it leads to exponential blowup. We will try to
    fix this in the extra bonus exercise.
 *)
fun match where
(*<*)
  "match gcs _ = undefined"
(*>*)

(*<*)
(* AUXILIARY LEMMAS *)
(*>*)


lemma match_correct: "match gcs cs \<longleftrightarrow> cs \<in> L_glob gcs"
(*<*)
  oops
(*>*)
    
value "match (STAR # map CR ''.exe'') ''windows.exe''"
value "match (STAR # map CR ''.exe'') ''windows.dll''"
  
value "match (map CR ''homework'' @ ANY # ANY # map CR ''.thy'') ''homework01.thy''"
value "match (map CR ''homework'' @ ANY # ANY # map CR ''.thy'') ''homework02.thy''"
value "match (map CR ''homework'' @ ANY # ANY # map CR ''.thy'') ''homework2.thy''"
    

section \<open>Extra Bonus: Efficient Glob Patterns (5 points)\<close>

text \<open>Warning, this bonus exercise is much more difficult than the previous one. If you had trouble
      with that, you may want to skip this one. Do not see this as an opportunity to catch up with
      points. This exercise is meant to flex your Isabelle/HOL muscles.\<close>


(*
  The naive matching algorithm is exponential in the size of the glob pattern as the \<open>STAR\<close>
  operator causes a branching which means that for a certain family of glob patterns, the amount
  of branches increases exponentially with the amount of \<open>STAR\<close> operators. By increasing the
  number of replications in the term below, we see an exponential increase in the runtime.
  Try it out yourself! We advise you to use increments of 1.
*)
value "match (concat (replicate 4 [CR CHR ''a'', STAR]) @ map CR ''.exe'') ''aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa.dll''"

(*
  d) Specify and show: We challenge you to implement a matching algorithm that doesn't have this 
    exponential blowup. You have to show that your implementation is correct. You don't have to 
    prove that your algorithm doesn't have this exponential blowup. We will evaluate that.
    You can validate this yourself using the input from the example above.
    
  HINT: Instead of branching, use sets of derivatives. As there are at most O(n) derivatives for
    a glob pattern of length n, you can avoid the exponential blowup!
  HINT: no need to implement the sets as lists or so, as long as "value" can evaluate you functions.
    
  WARNING: This exercise is very difficult, only attempt this if you are sure you can solve this.
  
*)

(*<*)
(* You define and prove everything yourself *)
(*>*)

end

