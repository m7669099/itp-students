theory Refine_Word
imports Refine "HOL-Library.Word"
begin

  subsection \<open>Relation between words and natural numbers\<close>

  definition "rel_word_nat w n \<equiv> unat w = n"

  lemmas [rel_rule] = rel[of rel_word_nat]

  subsection \<open>Utility Lemmas\<close>    
  lemma rel_word_nat_bound: "rel_word_nat l l' \<Longrightarrow> l' < 2 ^ LENGTH('a)" for l :: "'a::len word"
    unfolding rel_word_nat_def
    by auto
  
  lemma rel_word_nat_unat: "rel_word_nat x (unat x)" by (simp add: rel_word_nat_def)
  
  
  
  subsection \<open>Refinement of Operations\<close>
  (* TODO: define a better strategy, maybe based on 
    rewriting and (forcefully) pushing in \<open>unat\<close>, generating 
    in-bound side-conditions
  *)
  
  named_theorems word_nat_refine
  
  lemma word_nat_plus_refine[word_nat_refine]: "
    \<lbrakk>rel_word_nat a a'; rel_word_nat b b'; a'+b'<2^LENGTH('a) \<rbrakk>
    \<Longrightarrow> rel_word_nat (a+b) (a'+b')" for a b :: "'a::len word"
    unfolding rel_word_nat_def by (auto simp add: unat_add_lem)

  lemma word_nat_minus_refine[word_nat_refine]: "
    \<lbrakk>rel_word_nat a a'; rel_word_nat b b'; b'\<le>a' \<rbrakk>
    \<Longrightarrow> rel_word_nat (a-b) (a'-b')" for a b :: "'a::len word"
    unfolding rel_word_nat_def
    by (meson unat_sub_if')

  lemma word_nat_mult_refine[word_nat_refine]: "
    \<lbrakk>rel_word_nat a a'; rel_word_nat b b'; a'*b'<2^LENGTH('a) \<rbrakk>
    \<Longrightarrow> rel_word_nat (a*b) (a'*b')" for a b :: "'a::len word"
    unfolding rel_word_nat_def by (auto simp add: unat_mult_lem)
    
  lemma word_nat_div_refine[word_nat_refine]: "
    \<lbrakk>rel_word_nat a a'; rel_word_nat b b' \<rbrakk>
    \<Longrightarrow> rel_word_nat (a div b) (a' div b')" for a b :: "'a::len word"
    unfolding rel_word_nat_def by (auto simp add: unat_div)
    
  lemma word_nat_mod_refine[word_nat_refine]: "
    \<lbrakk>rel_word_nat a a'; rel_word_nat b b' \<rbrakk>
    \<Longrightarrow> rel_word_nat (a mod b) (a' mod b')" for a b :: "'a::len word"
    unfolding rel_word_nat_def by (auto simp add: unat_mod)

  lemma word_nat_zero_refine[word_nat_refine]: "rel_word_nat 0 0" unfolding rel_word_nat_def by simp
  lemma word_nat_one_refine[word_nat_refine]: "rel_word_nat 1 1" unfolding rel_word_nat_def by simp
  lemma word_nat_SucZ_refine[word_nat_refine]: "rel_word_nat 1 (Suc 0)" unfolding rel_word_nat_def by simp

  lemma word_nat_numeral_refine[word_nat_refine]: "(numeral n :: nat) < 2^LENGTH('a) \<Longrightarrow> rel_word_nat (numeral n :: 'a::len word) (numeral n)"
    unfolding rel_word_nat_def 
    apply (subst unat_numeral)
    by simp
      
  lemma word_nat_eq[word_nat_refine]: "rel_word_nat a a' \<Longrightarrow> rel_word_nat b b' \<Longrightarrow> (a=b) \<longleftrightarrow> (a'=b')"
    unfolding rel_word_nat_def 
    by (simp add: word_unat_eq_iff)
  
  lemma word_nat_le[word_nat_refine]: "rel_word_nat a a' \<Longrightarrow> rel_word_nat b b' \<Longrightarrow> (a\<le>b) \<longleftrightarrow> (a'\<le>b')"
    unfolding rel_word_nat_def 
    by (simp add: word_le_nat_alt)
  
  lemma word_nat_lt[word_nat_refine]: "rel_word_nat a a' \<Longrightarrow> rel_word_nat b b' \<Longrightarrow> (a<b) \<longleftrightarrow> (a'<b')"
    unfolding rel_word_nat_def 
    by (simp add: word_less_nat_alt)
  
  lemma rel_word_nat_Suc[word_nat_refine]: 
    "a'+1<2^LENGTH('a) \<Longrightarrow> rel_word_nat a a' \<Longrightarrow> rel_word_nat (a+1) (Suc a')"
    "a'+1<2^LENGTH('a) \<Longrightarrow> rel_word_nat a a' \<Longrightarrow> rel_word_nat (1+a) (Suc a')" 
    for a :: "'a::len word"
    apply (metis add.commute plus_1_eq_Suc word_nat_one_refine word_nat_plus_refine)
    apply (metis add.commute plus_1_eq_Suc word_nat_one_refine word_nat_plus_refine)
    done

  lemma word_nat_min_refine[word_nat_refine]: 
    "rel_word_nat a a' \<Longrightarrow> rel_word_nat b b' \<Longrightarrow> rel_word_nat (min a b) (min a' b')"
    by (auto simp: min_def word_nat_refine)
  
  lemma word_nat_power_refine[word_nat_refine]: 
    "rel_word_nat m m' \<Longrightarrow> m'^n<2^LENGTH('a) \<Longrightarrow> rel_word_nat (m^n) (m'^n)" for m :: "'a::len word"
    unfolding rel_word_nat_def 
    by (auto simp: of_nat_inverse)
    







end
