theory Option_Monad_Demo
imports Main "HOL-Library.Monad_Syntax" "../thys/Flat_CCPO"
begin

section \<open>Bind and Return\<close>

(* return is just Some *)

thm Option.bind.simps

(* Note the \<bind> syntax for bind *)
(* Always use bind with type-annotations, otherwise it cannot be disambiguated *)
term "bind (m::_ option) f" 

lemma 
  "do { x\<leftarrow>None; f x } = None"
  "do { x\<leftarrow>Some a; f x } = f a"
  by auto

find_theorems Option.bind  

subsection \<open>Monad Laws\<close>


lemma "do { x \<leftarrow> Some a; f x } = f a" by simp
lemma "do { x \<leftarrow> m; Some x } = m" by simp
lemma "do { y \<leftarrow> do { x \<leftarrow> m; f x }; g y } = do { x\<leftarrow>m; y \<leftarrow> f x; g y }"
  for m :: "_ option"
  by simp

thm Option.bind.bind_lunit
thm Option.bind_runit
thm Option.bind_assoc

(** Errors are propagated *)
lemma "do { x \<leftarrow> None; f x } = None" by simp
lemma "do { x \<leftarrow> m; None} = None" by simp

thm Option.bind.bind_lzero
thm Option.bind_rzero


section \<open>Assertions\<close> 
  
(* Assert. Return () if condition is true, None otherwise. *)
  
definition "assert P \<equiv> if P then Some () else None"

lemma assert_simps[simp]:
  "assert False = None"
  "assert True = Some ()"
  by (auto simp: assert_def)

  
section \<open>Example: expressions\<close>  
  
  
type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> val"
  
datatype aexp = N int | V vname | Plus aexp aexp | Div aexp aexp 

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val option" where
  "aval (N i) s = Some i"
| "aval (V x) s = Some (s x)"  
| "aval (Plus a b) s = do {
    ra \<leftarrow> aval a s;
    rb \<leftarrow> aval b s;
    Some (ra+rb)
}"
| "aval (Div a b) s = do {
    ra \<leftarrow> aval a s;
    rb \<leftarrow> aval b s;
    assert (rb \<noteq> 0);
    Some (ra div rb)
}"


fun cprop :: "aexp \<Rightarrow> aexp" where
  "cprop (N v) = N v"
| "cprop (V x) = V x"
| "cprop (Plus a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> N (va + vb)
    | (a,b) \<Rightarrow> Plus a b  
    )"
| "cprop (Div a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> (if vb = 0 then Div (N va) (N vb) else N (va div vb))
    | (a,b) \<Rightarrow> Div a b  
    )"

lemma "aval (cprop e) s = aval e s"
  apply (induction e)
  apply (auto split!: aexp.splits Option.bind_split)
  done
  

(**
  END DEMO HERE!
**)  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
section \<open>Weakest Precondition\<close>

text \<open>Program is correct, iff it returns some value that satisfies spec\<close>
  
fun wp where
  "wp None _ \<longleftrightarrow> False"
| "wp (Some x) P \<longleftrightarrow> P x"  
  
text \<open>wp commutes with bind. 
  This rule breaks down program proof over bind
\<close>
lemma wp_bind[simp]: "wp (do {x\<leftarrow>m; f x}) P = wp m (\<lambda>x. wp (f x) P)"
  by (cases m) auto

text \<open>Consequence rule\<close>  
lemma wp_cons: "wp m P \<Longrightarrow> (\<And>x. P x \<Longrightarrow> Q x) \<Longrightarrow> wp m Q"  
  by (cases m) auto

lemma wp_assert[simp]: "wp (assert \<Phi>) Q \<longleftrightarrow> \<Phi> \<and> Q ()"
  by (cases \<Phi>) auto
  
subsection \<open>Example: build set {0..<n}\<close>  
  

fun bs where
  "bs 0 = Some {}"
| "bs (Suc n) = do { s\<leftarrow>bs n; Some (insert n s) }"  

  
lemma bs_correct: "wp (bs n) (\<lambda>s. s={0..<n})"
  apply (induction n)
  apply (auto elim: wp_cons)
  done


  
section \<open>Refinement\<close>  

fun option_refine where
  "option_refine _ _ None \<longleftrightarrow> True"
| "option_refine R (Some x) (Some y) \<longleftrightarrow> R x y"
| "option_refine _ _ _ \<longleftrightarrow> False"

(* Refinement preserves correctness *)
lemma option_refine_wp: "option_refine R a b \<Longrightarrow> wp b P \<Longrightarrow> wp a (\<lambda>x. \<exists>y. R x y \<and> P y) "
  by (cases a; cases b; auto)

(* Refinement is transitive *)
lemma option_refine_trans: "option_refine R a b \<Longrightarrow> option_refine S b c \<Longrightarrow> option_refine (R OO S) a c"  
  by (cases a; cases b; cases c; auto)
  
(* Refinement and bind *)  
  
lemma option_refine_bind[intro!]:
  assumes "option_refine R' mi m"
  assumes "\<And>xi x. R' xi x \<Longrightarrow> option_refine R (fi xi) (f x)"
  shows "option_refine R (do {xi\<leftarrow>mi; fi xi}) (do {x\<leftarrow>m; f x})"
  using assms apply (cases m; cases mi) by auto
  
  
(* Refinement and assertion *)  

(* Assertion on RHS can be assumed during refinement. 
  (we have proved it already when proving abstract program ) 
*)
lemma refine_assert[simp]:
  "option_refine R m (do {assert P; m'}) \<longleftrightarrow> (P \<longrightarrow> option_refine R m m')"  
  by (cases P) auto

(* Introducing assertion on LHS: we have to prove assertion. non-typical case. *)
lemma assert_refineI:
  assumes "P"
  assumes "option_refine R m m'"
  shows "option_refine R (do {assert P; m}) m'"
  using assms
  by auto
  
    
    
(* Usage of assertions: example distinct lists *)  
definition "rel_dl xs s \<equiv> s=set xs \<and> distinct xs"

definition "dl_empty = Some []"
definition "dl_insert x xs = (if x\<in>set xs then Some xs else Some (x#xs))"
definition "dl_insert_dj x xs = Some (x#xs)"


lemma dl_empty_refine[simp]: "option_refine rel_dl dl_empty (Some {})"
  unfolding dl_empty_def rel_dl_def
  by auto
  
lemma dl_insert_refine[intro!]: 
  "rel_dl xs s \<Longrightarrow> option_refine rel_dl (dl_insert x xs) (Some (insert x s))"  
  unfolding dl_insert_def rel_dl_def
  by auto
  
lemma dl_insert_dj_refine[intro!]: 
  "rel_dl xs s \<Longrightarrow> x\<notin>s \<Longrightarrow> option_refine rel_dl (dl_insert_dj x xs) (Some (insert x s))"  
  unfolding dl_insert_dj_def rel_dl_def
  by auto

  
(* Let's implement bs: *)    
  
fun bsi where
  "bsi 0 = dl_empty"
| "bsi (Suc n) = do { s\<leftarrow>bsi n; dl_insert n s }"  
  
lemma "option_refine rel_dl  (bsi n) (bs n)"
  apply (induction n)
  apply auto
  done


(* Let's implement bs, more efficiently *)    
  
fun bsi' where
  "bsi' 0 = dl_empty"
| "bsi' (Suc n) = do { s\<leftarrow>bsi' n; dl_insert_dj n s }"  
  
lemma "option_refine rel_dl (bsi' n) (bs n)"
  apply (induction n)
  apply auto
  (* We get precondition, that n\<notin>set xs. But at this point, it's hard to discharge! *)
  oops

(* In abstract proof, it's easy to prove, though! *)
  
fun bs' where
  "bs' 0 = Some {}"
| "bs' (Suc n) = do { s\<leftarrow>bs' n; assert (n\<notin>s); Some (insert n s) }"  

  
lemma bs'_correct: "wp (bs' n) (\<lambda>s. s={0..<n})"
  apply (induction n)
  apply (auto elim: wp_cons)
  done
  
(* And the assertion can be assumed during refinement proof! *)  
lemma bsi'_refine: "option_refine rel_dl (bsi' n) (bs' n)"
  apply (induction n)
  apply auto
  done
    
(* Combine correctness lemmas *)  
lemma "wp (bsi' n) (\<lambda>xs. rel_dl xs {0..<n})"  
  using option_refine_wp[OF bsi'_refine bs'_correct]
  by auto
    

(*
  For slides:
  
  option monad: fail

  assertion: fail if not satisfied
  
  refinement: assume abstract does not fail
    b/c we have proved abstract correct already
    thus: when refining assertion, we can assume predicate holds
    can be used to prove things abstractly, that are needed for refinement
  
  
  THEN:  
    recursion: fixed point. unfold rule. refinement.
    present ccpo's as tool, don't go into detail
    
  THEN:
    non-determinism: set monad. e.g.: pick element from set  

  FINALLY: combine both: nres!


*)  
  
section \<open>Recursion\<close>

(* Characterize recursive function by function body:


  f x = if x<2 then return 1 else do {a\<leftarrow>f x; b\<leftarrow>f (x-1); return a+b}

  Function body:
  
  F \<equiv> \<lambda>D x. if x<2 then return 1 else do {a\<leftarrow>D x; b\<leftarrow>D (x-1); return a+b}
  
  F :: (nat \<Rightarrow> nat option) \<Rightarrow> nat = nat option
  
  Here, D is the recursive call.
  
  REC F :: nat \<Rightarrow> nat option - recursive function defined by F.
  
  Properties: Unfold function body, use function for recursive calls

  REC F = F (REC F)
  
  So, REC F is fixed point of F

  Pointwise induction: show property P x (f x).
  
  property must hold for None: 
    \<And>x. P x None
  property preserved by function body:
    \<And>D x. \<lbrakk> \<And>y. P y (D y) \<rbrakk> \<Longrightarrow> P x (F D x)
  then: P x (REC F x)
  
  intuition: 
    function does not terminate \<Longrightarrow> P x None
    or we can justify property along recursions.
    
  Note: for this theory to work, F must be monotonic, i.e.,
  
  D\<le>D' \<Longrightarrow> F D \<le> F D'
  
  where f\<le>f' iff \<forall>x. f x = None \<or> f x = f' x

  Intuition: if D terminates for less arguments than D', 
    then F D terminates for less arguments than F D'.
  
      
    
*)



interpretation flat_rec None .


term REC
thm REC_unfold
thm REC_pointwise_induct

(* Well-founded induction to show wp *)
thm REC_wf_induct

lemma REC_wp:
  assumes "wf R"
  assumes "mono F"
  assumes "\<And>x D. \<lbrakk>\<And>y. (y,x)\<in>R \<Longrightarrow> wp (D y) (P y)\<rbrakk> \<Longrightarrow> wp (F D x) (P x)"
  shows "wp (REC F x) (P x)"
  using assms by (rule REC_wf_induct)

(* fixed-point induction to show refinement *)
lemma REC_refine:
  assumes "Rx xi x"
  assumes "mono Fi"
  assumes "\<And>xi x Di D. \<lbrakk>Rx xi x; \<And>xi x. Rx xi x \<Longrightarrow> option_refine R (Di xi) (D x) \<rbrakk> \<Longrightarrow> option_refine R (Fi Di xi) (F D x)"
  shows "option_refine R (REC Fi xi) (REC F x)"
  using assms(1)
  apply (induction x arbitrary: xi rule: REC_pointwise_induct)
  apply simp
  apply (subst REC_unfold[OF assms(2)])
  apply (rule assms(3))
  .


(* Monotonicity Proofs *)  
lemma const_mono: "mono (\<lambda>D x. f x)"
  apply rule
  by (simp add: fun_ord_def le_def)
  
lemma self_mono: "mono (\<lambda>D x. D (f x))"
  by (simp add: fun_ord_def monotone_def)  
    
lemma if_mono:
  assumes "mono b" "mono c" 
  shows "mono (\<lambda>D x. if a x then b D x else c D x)"  
  using assms
  by (smt (verit, best) fun_ord_def monotoneD monotoneI)

lemma bind_mono:
  assumes "mono m" assumes "\<And>y. mono (f y)"    
  shows "mono (\<lambda>D x. do {y\<leftarrow>m D x; f y D x})"  
  using assms
  unfolding monotone_def fun_ord_def le_def
  apply (auto split: Option.bind_split)
  apply (metis option.distinct(1))
  by (metis option.discI option.sel)  
  
  
  
definition fib :: "nat \<Rightarrow> nat option" 
  where "fib \<equiv> REC (\<lambda>fib n. if n<2 then Some 1 else do { a\<leftarrow>fib (n-2); b\<leftarrow>fib (n-1); Some (a+b) })"

(* Showing unfold rule *)    
lemma fib_unfold: "fib n = (if n<2 then Some 1 else do { a\<leftarrow>fib (n-2); b\<leftarrow>fib (n-1); Some (a+b) })"
  unfolding fib_def
  apply (subst REC_unfold)  
  subgoal (* Can be automated! *)
    apply (rule if_mono)
    apply (rule const_mono)
    apply (rule bind_mono)
    apply (rule self_mono)
    apply (rule bind_mono)
    apply (rule self_mono)
    apply (rule const_mono)
    done    
  by (auto)

(* Proving correctness / properties *)  
    
(* Either use wf-induction + unfold rule directly *)
lemma "wp (fib n) (\<lambda>x. 1\<le>x)"
proof -
  have "wf less_than" by simp
  thus ?thesis proof (induction n)
    case (less n)
    
    
    note IH = less[THEN wp_cons]
    
    show ?case 
      apply (subst fib_unfold)
      apply (auto intro!: IH)
      done
    
  qed
qed

(* Or re-prove monotonicity property *)
lemma "wp (fib n) (\<lambda>x. 1\<le>x)"
  unfolding fib_def
  apply (rule REC_wp[where R=less_than])
  subgoal by simp
  subgoal by (rule if_mono const_mono bind_mono self_mono)+ 
proof -
  fix x :: nat and D :: "nat \<Rightarrow> nat option"
  assume IH: "(\<And>y. (y, x) \<in> less_than \<Longrightarrow> wp (D y) ((\<le>) 1))" 
  show "wp (if x < 2 then Some 1 else do {
                                           a \<leftarrow> D (x - 2);
                                           b \<leftarrow> D (x - 1);
                                           Some (a + b)
                                         })
            ((\<le>) 1)"
 

  (* can be automated \<dots> *)          
              
    by (auto intro!: IH[THEN wp_cons])
    
qed

(*
  Slides: 
  
    recursive function \<rightarrow> fixed point (tech. mono)
    unfold property, gives wf-induct
    
    fp-induct: gives refine
    
    many proofs can be automated.


*)



  
  
    
end
