theory While_Lib
imports "HOL-Library.While_Combinator"
begin

  text \<open>A while loop terminates, iff iterating the body 
    ultimately yields a state for which the condition does not hold.
    
    Note that this lemma is stronger than @{thm [source] while_option_stop2}.
  \<close>
  lemma while_option_some_iff: 
    "while_option b c s = Some t \<longleftrightarrow> (\<exists>k. t = (c^^k) s \<and> \<not>b t \<and> (\<forall>i<k. b ((c^^i) s)))"
    unfolding while_option_def
    apply auto
    subgoal
      by (metis (no_types, lifting) LeastI_ex not_less_Least)
    subgoal
      by (metis (mono_tags, lifting) LeastI_ex less_linear not_less_Least)
    done

  text \<open>Version of @{thm [source] while_option_rule} rule, 
    analogous to @{thm [source] while_rule}\<close>
  lemma while_option_rule':
    assumes "P s" \<comment> \<open>Invariant holds for initial state\<close>
      and "\<And>s. \<lbrakk>P s; b s\<rbrakk> \<Longrightarrow> P (c s)"  \<comment> \<open>Invariant preserved by loop iteration\<close>
      and "\<And>s. \<lbrakk>P s; \<not> b s\<rbrakk> \<Longrightarrow> Q (Some s)"  \<comment> \<open>Invariant at end of loop implies statement\<close>
      and "wf r"                      \<comment> \<open>Relation is well-founded\<close>  
      and "\<And>s. \<lbrakk>P s; b s\<rbrakk> \<Longrightarrow> (c s, s) \<in> r" \<comment> \<open>Loop iteration decreases state (wrt. r)\<close>
    shows "Q (while_option b c s)"
    using assms 
    by (smt (verit, ccfv_threshold) wf_rel_while_option_Some while_option_rule while_option_some_iff)
    
  text \<open>Asymmetric refinement: if the abstract program terminates, 
    then the concrete program terminates with a related result.
    
    If the abstract program does not terminate, there are no constraints 
    on the concrete program's behaviour.
    The idea is that we will prove that the abstract program terminates anyway.
  \<close>
  fun ref_option where
    "ref_option R _ None \<longleftrightarrow> True"      
  | "ref_option R (Some c) (Some a) \<longleftrightarrow> R c a"  
  | "ref_option R None (Some _) \<longleftrightarrow> False"  
    

  text \<open>Structural refinement rule for while, with the asymmetric rule\<close>  
  lemma while_option_refine_aux:
    assumes R0: "R si s" \<comment> \<open>The inital states are related\<close>
    assumes SIMB: "\<And>si s. R si s \<Longrightarrow> bi si = b s" \<comment> \<open>On related states, the conditions are equal\<close>
    assumes SIMC: "\<And>si s. R si s \<Longrightarrow> bi si \<Longrightarrow> b s \<Longrightarrow> R (ci si) (c s)" \<comment> \<open>On related states 
                                                                    for which the condition holds,
                                                                     the next states are related\<close>
    shows "ref_option R (while_option bi ci si) (while_option b c s)" 
      (is "ref_option R ?wi ?w")
  proof -
    {
      text \<open>We can assume that the abstract loop terminates. Call the result \<open>r\<close>.\<close>
      fix r
      assume A: "?w = Some r"

      text \<open>Let \<open>as\<close> be the sequence of abstract states, and \<open>cs\<close> be the sequence of concrete states\<close>
      define as where "as i \<equiv> (c^^i) s" for i
      define cs where "cs i \<equiv> (ci^^i) si" for i
      
      text \<open>Obtain \<open>n\<close> as number of iterations of the abstract loop\<close>
      from A obtain n where "r = as n" and ABS_CHAIN: "\<forall>i<n. b (as i)" "\<not>b (as n)" 
        unfolding as_def while_option_some_iff by blast
      
      text \<open>Now follow the iterations of the concrete loop, and show that they are related to 
        the corresponding abstract states\<close>
      have REL: "i\<le>n \<Longrightarrow> R (cs i) (as i)" for i 
      proof (induction i)
        case 0
        text \<open>The initial states are related by assumption @{thm [source] R0}}\<close>
        then show ?case by (simp add: cs_def as_def R0)
      next
        case (Suc i)
        text \<open>Induction hypothesis: the current states are related\<close>
        hence IH: "R (cs i) (as i)" by auto

        text \<open>To show: the next states are related\<close>
        show "R (cs (Suc i)) (as (Suc i))" 
        proof -

          text \<open>As there is a next state, the current state cannot be the last one, 
            and thus, the abstract condition holds on it\<close>        
          from Suc.prems ABS_CHAIN(1) have B: "b (as i)" by auto
          
          text \<open>As the current states are related, and the conditions are equal on related states,
            also the concrete condition holds on the abstract state\<close>
          with SIMB[OF IH] have BI: "bi (cs i)" by auto
  
          text \<open>Thus, also the results of applying one step are related\<close>
          from SIMC[OF IH BI B] have "R (ci (cs i)) (c (as i))" .
          moreover 
          text \<open>which are precisely the next states\<close>
          have "ci (cs i) = cs (Suc i)" "c (as i) = as (Suc i)"
            unfolding cs_def as_def by auto
          ultimately show "R (cs (Suc i)) (as (Suc i))" by simp
        qed
      qed

      text \<open>As all states are related, also all conditions are equal\<close>
      from REL have RELB: "i\<le>n \<Longrightarrow> bi (cs i) = b (as i)" for i using SIMB by blast
      
      text \<open>Thus, the concrete loop also terminates after \<open>n\<close> iterations\<close>
      from ABS_CHAIN RELB have "\<forall>i<n. bi (cs i)" "\<not>bi (cs n)" by auto 
      hence 1: "?wi = Some (cs n)"
        unfolding cs_def while_option_some_iff by blast
      moreover 
      text \<open>It only remains to unfold the name \<open>r\<close> for the result\<close>
      from REL \<open>r = as n\<close> have 2: "R (cs n) r" by simp
      ultimately have ?thesis using A by simp
    }
    thus ?thesis by (cases ?w; cases ?wi; simp)
  qed  
    
    
  text \<open>The simulation conditions are actually symmetric, 
    such that we easily get a symmetric relation via \<open>rel_option\<close>
  \<close>
  lemma while_option_refine:
    assumes R0: "R si s"
    assumes SIMB: "\<And>si s. R si s \<Longrightarrow> bi si = b s"
    assumes SIMC: "\<And>si s. R si s \<Longrightarrow> bi si \<Longrightarrow> b s \<Longrightarrow> R (ci si) (c s)"
    shows "rel_option R (while_option bi ci si) (while_option b c s)" (is "rel_option R ?wi ?w")
  proof -
    have "ref_option R ?wi ?w" by (simp add: while_option_refine_aux assms)
    moreover have "ref_option (\<lambda>a c. R c a) ?w ?wi" by (simp add: while_option_refine_aux assms)
    ultimately
    show ?thesis
      by (cases ?w; cases ?wi; simp)
  qed    



end
