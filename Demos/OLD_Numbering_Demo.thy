theory OLD_Numbering_Demo
imports 
  "HOL-Library.RBT"
  (* The next two activate dictionary order on strings, 
     such that we can use them as keys in RBTs
  *)
  "HOL-Library.Char_ord" 
  "HOL-Library.List_Lexorder"  
begin
  section \<open>Injective Numbering\<close>  
  
  (*
    Given a list of strings. Generate an injective map from the strings to
    numbers {0..<n}, where n is the number of different strings in the list.
    
    Motivation: many algorithms work better when things (states, etc) are numbered,
      such that properties for state i can be efficiently stored at an array index a[i].
      
    We could implement this numbering by a red-black tree. Note that the suffix "i"
    is for "implementation".  
  *)
  
  definition "addnumi x \<equiv> \<lambda>(n,s). if RBT.lookup s x \<noteq> None then (n,s) else (n+1,RBT.insert x n s)"

  definition "numberingi xs = (let 
    (_,s) = fold addnumi xs (0::nat,RBT.empty)
  in
    the o RBT.lookup s
  )"
  
  value "numberingi [''a'',''b'',''aa'',''bb''] ''a''"

  lemma "inj_on (numberingi xs) (set xs)"
    quickcheck oops
    
  lemma "\<lbrakk> a\<in>set xs; b\<in>set xs; numberingi xs a = n; numberingi xs b = n\<rbrakk> \<Longrightarrow> a=b"
    quickcheck oops
  
  lemma "\<exists>n. (numberingi xs)`set xs = {0..<n}"
    oops

  subsection \<open>Abstract Algorithm\<close>    
  (* Let's focus on the idea. Phrase the algorithm with maps ('a\<Rightarrow>'b option), 
    rather than their implementation. *)
    
  definition addnum :: "'b \<Rightarrow> nat \<times> ('b \<Rightarrow> nat option) \<Rightarrow> nat \<times> ('b \<Rightarrow> nat option)"
    where "addnum x \<equiv> \<lambda>(n,m). if m x \<noteq> None then (n,m) else (n+1,m(x\<mapsto>n))"

  definition "numbering xs = (let 
    (_,m) = fold addnum xs (0::nat,Map.empty)
  in
    the o m
  )"
      

  subsubsection \<open>Invariants\<close>
  (*
    The main algorithm folds over the list, using addnum on each element.
    This can be seen as a 'for'-loop over the elements of the list, 
    modifying a 'state' (n,m) in each iteration.
    
    Such loops are best reasoned about by providing an invariant: 
      A property that holds for the initial state (0,Map.empty),
      is maintained by each loop iteration, 
      and, upon completion of the iteration, implies the property we want to prove.
      
      
    The invariant usually describes the idea of the loop. 
    It requires a good understanding of how the algorithm works (and some practice) to
    come up with good invariants.
    
    In our case, we want to express that:
      the current map is an injective numbering of the strings seen so far,
      using the numbers {0..<n}
      
  *)
  
  (*
    Test lemma:
      the second property, using the numbers {0..<n}, is preserved by addnum
    
  *) 
  lemma "ran m = {0..<n} \<Longrightarrow> addnum x (n,m) = (n',m') \<Longrightarrow> ran m' = {0..<n'}"
    by (auto simp: addnum_def split: if_splits) 
  
  (*
    But we'll need more properties. 
    It makes sense to define a constant for the invariant. 
    Even more so, if the invariant corresponds to an abstract 'concept', 
    like being a dense numbering of the elements of some set.
    
    This gives a clear boundary between proofs over the algorithm structure 
      (e.g. induction over the fold), and reasoning about invariant preservation.
    
    Note: modularity is important for programming. 
      For proving programs correct, it's ESSENTIAL.  
      
    
  *)  
  definition is_numbering_on :: "nat \<times> ('b \<Rightarrow> nat option) \<Rightarrow> 'b set \<Rightarrow> bool"
  where
    "is_numbering_on \<equiv> \<lambda>(n,m) s. 
      ran m = {0..<n} \<comment> \<open>Exactly the numbers \<open>{0..<n}\<close> are used\<close>
    \<and> dom m = s       \<comment> \<open>To map the elements from \<open>s\<close>\<close>
    \<and> inj_on m s      \<comment> \<open>Injectively\<close>
    "  

  (* Invariant preservation: addnum preserves the invariant.  *)  
  lemma addnum_pres_numbering: "is_numbering_on nm s \<Longrightarrow> is_numbering_on (addnum x nm) (insert x s)"
    unfolding is_numbering_on_def addnum_def
    (* Note: Invariant and addnum unfolded here. Reasoning about the internals. *)
    apply (auto split: if_splits)
    subgoal by (metis domIff fun_upd_other inj_on_cong)
    subgoal by (simp add: domI inj_onD)
    subgoal by (metis atLeastLessThan_iff not_le order_refl ranI)
    done
    
  (* Thus, a fold preserves the invariant. *)  
  lemma is_numbering_fold: "is_numbering_on nm s \<Longrightarrow> is_numbering_on (fold addnum xs nm) (set xs \<union> s)"  
    (* Note: Not looking into invariant and addnum here. 
        Reasoning about the structure. *)
  proof (induction xs arbitrary: nm s)
    case Nil
    then show ?case by auto
  next
    case (Cons x xs)
    
    from addnum_pres_numbering[OF Cons.prems] 
    have "is_numbering_on (addnum x nm) (insert x s)" .
    
    from Cons.IH[OF this] show ?case by simp
  qed

  (* Invariant holds initially *)
  lemma is_numbering_init: "is_numbering_on (0,Map.empty) {}"
    by (auto simp: is_numbering_on_def)

  lemmas is_numbering_invar = is_numbering_fold[OF is_numbering_init, simplified]
      
  (* Now we can prove the properties that follow from the invariant, and use them to
    show that the main algorithm is correct. *)  
    
  (* Auxiliary lemma for property implied by invariant *)  
  lemma is_numbering_imp_inj: "is_numbering_on (n,m) s \<Longrightarrow> inj_on (the o m) s"  
    by (auto simp: is_numbering_on_def inj_on_def domI)

  lemma numbering_inj: "inj_on (numbering xs) (set xs)"
    apply (clarsimp simp: numbering_def split: prod.splits)
  proof -
    fix n m
    assume "fold addnum xs (0, Map.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_on (n,m) (set xs)" 
      by simp
    with is_numbering_imp_inj show "inj_on (the o m) (set xs)" by auto
  qed      

  (* Implication by invariant proved inside correctness proof. *)
  lemma numbering_dense: "\<exists>n. numbering xs`set xs = {0..<n}"
  proof (clarsimp simp: numbering_def split: prod.splits)
    fix n m
    assume "fold addnum xs (0, Map.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_on (n,m) (set xs)" 
      by simp
    then show "\<exists>n. (\<lambda>x. the (m x)) ` set xs = {0..<n}"
      (* TODO: find a nice proof! (homework?) *)
      apply (auto simp: is_numbering_on_def ran_def intro!: exI[where x=n])
      apply force
      by (smt (z3) Setcompr_eq_image all_nat_less_eq domI mem_Collect_eq option.sel) 
  qed      

  subsection \<open>Refinement to RBT\<close>
  (* Finally, we can link the abstract version on maps to the concrete version on RBTs *)
  
  (* Again, we proceed along the structure of the algorithm.
  
    First, we link addnum and addnum
  *)
  
  lemma addnumi_refine:
    assumes "addnumi x (n,mi) = (ni',mi')"
    assumes "addnum x (n,RBT.lookup mi) = (n',m')"
    shows "ni'=n'" "RBT.lookup mi' = m'"
    using assms
    apply (auto simp: addnumi_def addnum_def split: if_splits)
    done
  
  lemma fold_addnumi_refines:
    assumes "fold addnumi xs (n,mi) = (ni',mi')"
    assumes "fold addnum xs (n,RBT.lookup mi) = (n',m')"
    shows "ni'=n'" "RBT.lookup mi' = m'"
  proof -
    (* For technical reasons, we summarize the two separate goals for the induction *)
    have "ni'=n' \<and> RBT.lookup mi' = m'"
      using assms
    proof (induction xs arbitrary: n mi ni' mi' n' m')
      case Nil
      then show ?case by simp
    next
      case (Cons x xs)
      
      obtain nh mih where 1: "addnumi x (n, mi) = (nh,mih)" "addnum x (n,RBT.lookup mi) = (nh,RBT.lookup mih)"
        apply (cases "addnumi x (n, mi)"; cases "addnum x (n,RBT.lookup mi)")
        using addnumi_refine
        by blast
      
      show ?case
        using Cons.prems
        apply (simp add: 1)
        apply (rule Cons.IH)
        .
      
    qed
    thus "ni'=n'" "RBT.lookup mi' = m'" by auto
  qed
      
  
  lemma numberingi_refine: "numberingi xs = numbering xs"
    unfolding numbering_def numberingi_def
    by (auto split: prod.split dest: fold_addnumi_refines)

  (*   
    This proof, again, was divided into structural (refining the fold), 
    and more internal steps (refining addnum).
  *)  
    
  subsection \<open>Transitive Composition\<close>  
      
  (* Finally, we can compose the refinement and correctness lemma.
    We could use some forward proof trickery 
    (note the restriction to linearly ordered domains, 
    that we got by using RBTs)
  *)  
    
  lemmas numberingi_inj = numbering_inj[where 'a="_::linorder", folded numberingi_refine]
  lemmas numberingi_dense = numbering_dense[where 'a="_::linorder", folded numberingi_refine]
  
  (* It's more robust, though, to restate the lemmas. 
    Here, type inference takes care of the linearly ordered restriction
  *)  
  
  lemma numberingi_inj': "inj_on (numberingi xs) (set xs)"
    unfolding numberingi_refine by (rule numbering_inj)
  
  lemma numberingi_dense': "\<exists>n. numberingi xs`set xs = {0..<n}"
    unfolding numberingi_refine by (rule numbering_dense)
  
  
  
end
