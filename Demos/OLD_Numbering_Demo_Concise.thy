theory Numbering_Demo_Concise
imports 
  "HOL-Library.RBT"
  (* The next two activate dictionary order on strings, 
     such that we can use them as keys in RBTs
  *)
  "HOL-Library.Char_ord" 
  "HOL-Library.List_Lexorder"  
begin
  (*
    We do the numbering function again,
    however, in an even more systematic way, 
    focusing on general structure elements
  *)


  section \<open>Injective Numbering\<close>  
  
    
  definition addnum :: "'b \<Rightarrow> nat \<times> ('b \<Rightarrow> nat option) \<Rightarrow> nat \<times> ('b \<Rightarrow> nat option)"
    where "addnum x \<equiv> \<lambda>(n,m). if m x \<noteq> None then (n,m) else (n+1,m(x\<mapsto>n))"

  definition "numbering xs = (let 
    (_,m) = fold addnum xs (0::nat,Map.empty)
  in
    the o m
  )"
      

  subsubsection \<open>Invariants\<close>
  definition is_numbering_on :: "nat \<times> ('b \<Rightarrow> nat option) \<Rightarrow> 'b set \<Rightarrow> bool"
  where
    "is_numbering_on \<equiv> \<lambda>(n,m) s. 
      ran m = {0..<n} \<comment> \<open>Exactly the numbers \<open>{0..<n}\<close> are used\<close>
    \<and> dom m = s       \<comment> \<open>To map the elements from \<open>s\<close>\<close>
    \<and> inj_on m s      \<comment> \<open>Injectively\<close>
    "  

  (* Invariant holds initially *)
  lemma is_numbering_init: "is_numbering_on (0,Map.empty) {}"
    by (auto simp: is_numbering_on_def)
    
  (* Invariant preservation: addnum preserves the invariant.  *)  
  lemma addnum_pres_numbering: "is_numbering_on nm s \<Longrightarrow> is_numbering_on (addnum x nm) (insert x s)"
    unfolding is_numbering_on_def addnum_def
    (* Note: Invariant and addnum unfolded here. Reasoning about the internals. *)
    apply (auto split: if_splits)
    subgoal by (metis domIff fun_upd_other inj_on_cong)
    subgoal by (simp add: domI inj_onD)
    subgoal by (metis atLeastLessThan_iff not_le order_refl ranI)
    done

  (* 
    The proof that the fold preserves the invariant was quite verbose.
    We can do such a proof generically, once and for all, and then
    use it for all folds:

    The following assumes an iteration over the elements of a list,
    where the order and multiplicity of elements does not matter.
    
    The invariant is parameterised by a set of already processed elements.
  *)
  lemma fold_invariant_rule:
    assumes INIT: "P {} s" \<comment> \<open>Initially, no elements are processed\<close>
    assumes STEP: "\<And>x it s. \<lbrakk> it\<subseteq>set xs; x\<in>set xs; P it s \<rbrakk> \<Longrightarrow> P (insert x it) (f x s)"
      \<comment> \<open>Invariant is preserved by processing an element from the list\<close>
    assumes CONS: "P (set xs) (fold f xs s) \<Longrightarrow> Q"
      \<comment> \<open>Subgoal follows from invariant, if all elements of the list have been processed\<close>
    shows "Q"
  proof -
  
    have "P (it\<union>set xs') (fold f xs' s)" if "P it s" "it \<union> set xs' \<subseteq> set xs" for it xs'
      using that
    proof (induction xs' arbitrary: it s)
      case Nil
      then show ?case by simp
    next
      case (Cons x xs')
      
      note IH' = Cons.IH[of "insert x it", simplified]
      
      show ?case
        apply simp
        apply (rule IH')
        apply (rule STEP)
        using Cons.prems
        by auto
      
    qed
    from this[of "{}", OF INIT] CONS show ?thesis by simp
  qed

  (* The invariant proof is only a few lines now *)    
  lemma is_numbering_invar: "is_numbering_on (fold addnum xs (0,Map.empty)) (set xs)"  
    supply R = fold_invariant_rule[where P="\<lambda>x y. is_numbering_on y x"]
    thm R
    apply (rule R)
    (* Moreover, the first two subgoals hint at the low-level lemmas
      that have to be proved about the invariant *)
    apply (rule is_numbering_init)
    apply (rule addnum_pres_numbering, assumption)
    .
      
  lemma is_numbering_imp_inj: "is_numbering_on (n,m) s \<Longrightarrow> inj_on (the o m) s"  
    by (auto simp: is_numbering_on_def inj_on_def domI)

  lemma numbering_inj: "inj_on (numbering xs) (set xs)"
    apply (clarsimp simp: numbering_def split: prod.splits)
  proof -
    fix n m
    assume "fold addnum xs (0, Map.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_on (n,m) (set xs)" 
      by simp
    with is_numbering_imp_inj show "inj_on (the o m) (set xs)" by auto
  qed      

  lemma numbering_dense: "\<exists>n. numbering xs`set xs = {0..<n}"
  proof (clarsimp simp: numbering_def split: prod.splits)
    fix n m
    assume "fold addnum xs (0, Map.empty) = (n, m)"
    with is_numbering_invar[of xs] have "is_numbering_on (n,m) (set xs)" 
      by simp
    then show "\<exists>n. (\<lambda>x. the (m x)) ` set xs = {0..<n}"
      (* TODO: find a nice proof! (homework?) *)
      apply (auto simp: is_numbering_on_def ran_def intro!: exI[where x=n])
      apply force
      by (smt (z3) Setcompr_eq_image all_nat_less_eq domI mem_Collect_eq option.sel) 
  qed      

  subsection \<open>Refinement to RBT\<close>
  definition "addnumi x \<equiv> \<lambda>(n,s). if RBT.lookup s x \<noteq> None then (n,s) else (n+1,RBT.insert x n s)"

  definition "numberingi xs = (let 
    (_,s) = fold addnumi xs (0::nat,RBT.empty)
  in
    the o RBT.lookup s
  )"

  (* Refinement lemmas, in particular for tuples, are typically much easier to use
    if stated in relational style.
  *)  

  (* The relation between a concrete and abstract state: First elements of pair are equal, 
     second elements related wrt lookup *)
  definition "rel_state \<equiv> rel_prod (=) (\<lambda>ri r. r=RBT.lookup ri)"

  (* Refinement for addnumi in relational style. Much more concise! *)
  lemma addnumi_refine': "rel_state si s \<Longrightarrow> rel_state (addnumi x si) (addnum x s)"
    unfolding rel_state_def rel_prod_conv
    apply (auto simp: addnumi_def addnum_def split: if_splits)
    done

  (* Generic refinement lemma for fold, relational style: *)
  lemma fold_refine: 
    assumes INIT: "R si s"
    assumes STEP: "\<And>si s x. R si s \<Longrightarrow> R (fi x si) (f x s)"
    shows "R (fold fi xs si) (fold f xs s)"
    using INIT
    apply (induction xs arbitrary: si s)
    apply (auto simp: STEP)
    done
    
  lemma fold_addnumi_refine:
    assumes "rel_state si s"
    shows "rel_state (fold addnumi xs si) (fold addnum xs s)"
    (* Note: unlike the proof of fold_addnumi_refine, this proof is straightforward now! *)
    apply (rule fold_refine)
    apply fact
    by (rule addnumi_refine')
      
  lemma numberingi_refine: "numberingi xs = numbering xs"
    using fold_addnumi_refine[of "(0,RBT.empty)" "(0,Map.empty)" xs]
    unfolding numbering_def numberingi_def rel_state_def
    by (auto split: prod.split)
          
  subsection \<open>Transitive Composition\<close>  
    
  lemma numberingi_inj': "inj_on (numberingi xs) (set xs)"
    unfolding numberingi_refine by (rule numbering_inj)
  
  lemma numberingi_dense': "\<exists>n. numberingi xs`set xs = {0..<n}"
    unfolding numberingi_refine by (rule numbering_dense)
  
  (*
    Note that, without disciplined structuring of the proof, separating the different aspects
    of the reasoning, the proof of this rather simple program would likely have become very 
    complicated, and hard to understand.
  
    Even with the additional lemmas, getting the correct structuring 
    into this proof required some effort and knowing of what you are doing. 
    It is too easy to deviate from this structure accidentally, or due to pre-mature 
    proof-optimization attempts.
    
    In the last part of this lecture, we will introduce a more systematic 
    approach to this refinement proof style. This will pave the way to proving 
    correct more complex programs at still manageable effort.
  *)
    

  (*
    More homework ideas:

    LTS from list of triples, as RBT-map. Create from list of triples, succ operation on RBT-map.

    Map LTS with injective function. Language remains the same (do abstractly, maybe implement on triple list)
  *)
  
end
