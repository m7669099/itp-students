section "Arithmetic and Boolean Expressions"

theory tut05 imports Main "HOL-Library.RBT"
begin

(* Given definitions and lemmas *)


(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_set t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

(* This was added *)
definition "rbts_is_empty t \<equiv> RBT.is_empty t"

lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
lemma rbts_insert_correct:
  "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
lemma rbts_delete_correct:
  "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_set t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_set_def)
qed  
  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_set_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct


(*
  Once again we define a graph. However, this time we define it as a set of edges, rather
    than as a function. In this tutorial, we are going to focus on abstraction. In some cases
    you don't want Isabelle/HOL to automatically simplify basic operations, as basic lemmas
    you have proven on these operations are not applicable anymore.
*)

type_synonym 'v graph = "('v\<times>'v) set"

definition empty_graph :: "'v graph" where 
(*<*)
  "empty_graph \<equiv> undefined"
(*>*)

definition add_edge :: "('v\<times>'v) \<Rightarrow> 'v graph \<Rightarrow> 'v graph" where
(*<*)
  "add_edge e E \<equiv> undefined"  
(*>*)


text \<open>We now define a graph again, but this time we don't define it as a set, but as a list.\<close>

type_synonym 'v list_graph = "('v\<times>'v) list"

definition lg_\<alpha> :: "'v list_graph \<Rightarrow> 'v graph" where
(*<*)
  "lg_\<alpha> xs = undefined"
(*>*)

definition lg_empty :: "'v list_graph" where 
(*<*)
  "lg_empty \<equiv> undefined"
(*>*)

definition lg_add_edge :: "('v\<times>'v) \<Rightarrow> 'v list_graph \<Rightarrow> 'v list_graph" where 
(*<*)
  "lg_add_edge \<equiv> undefined"
(*>*)

(*<*)
(* AUXILIARY LEMMAS *)
(*<*)


(*  
  The list data structure allows us to efficiently iterate over the edges, but if we want to
    find the adjacent vertices more quickly we have to iterate through the entire set of edges.
    This is very inefficient. Alternatively, we can use a different data structure that maps
    each node it a list of adjacent nodes.
*)

type_synonym 'v rbt_graph = "('v,'v list) rbt"

definition rg_\<alpha> :: "'v::linorder rbt_graph \<Rightarrow> 'v graph"  where 
(*<*)
  "rg_\<alpha> t \<equiv> undefined"
(*>*)

definition rg_empty :: "'v::linorder rbt_graph" where 
(*<*)
  "rg_empty \<equiv> undefined"
(*<*)

definition rg_add_edge :: "('v::linorder\<times>'v) \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v rbt_graph" where
(*<*)
 "rg_add_edge \<equiv> undefined"  
(*>*)

(*<*)
(* AUXILIARY LEMMAS *)
(*>*)
  
  
(*

  Now we want to read a graph represented as list into a graph represented as red-black tree,
  and show that the abstraction of the graph is preserved by the read operation:

  lg - read \<rightarrow> rg
  |            |
 lg_\<alpha>         rg_\<alpha>
  |            |
  g     =      g
  
  
*)

definition read_lst :: \<open>'v::linorder list_graph \<Rightarrow> 'v rbt_graph\<close> where 
(*<*)
  "read_lst xs \<equiv> undefined"
(*>*)


lemma \<open>rg_\<alpha> (read_lst xs) = lg_\<alpha> xs\<close> 
(*<*)
  oops
(*>*)

  
(* Abstract concept of nodes in a graph *)
definition V :: "'v graph \<Rightarrow> 'v set" where 
(*<*)
  "V g = undefined"
(*>*)

(* Abstract concept of dead end *)
definition has_dead_end :: "'v graph \<Rightarrow> bool" where
(*<*)
  "has_dead_end g \<equiv> undefined"
(*>*)

(* Alternative abstract representation, that suggests implementation by set difference *)
lemma has_dead_end1: "has_dead_end g \<longleftrightarrow> snd`g - fst`g \<noteq> {}"
(*<*)
  oops
(*>*)

(* Concrete implementation (naive) *)
definition lg_has_dead_end :: "'v list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end xs \<equiv> undefined"
(*>*)

lemma lg_has_dead_end_correct: "lg_has_dead_end xs \<longleftrightarrow> has_dead_end (lg_\<alpha> xs)"
(*<*)
  oops
(*>*)

(*
  Operation list to rbt set
*)

(* Add operation *)  
  
(* Note: This is a generic operation. 
  It's not specific to RBTs, but the pattern works for all 
  set implementations that support insert and empty  
*)


(* More efficient implementation with two passes, and red-black-tree *)
definition lg_has_dead_end_rbt :: "'v::linorder list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end_rbt xs \<equiv> undefined"
(*>*)

lemma "lg_has_dead_end_rbt xs = has_dead_end (lg_\<alpha> xs)"
(*<*)
  oops
(*>*)

end
