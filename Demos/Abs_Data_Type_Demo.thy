theory Abs_Data_Type_Demo
imports "HOL-Library.RBT" 
begin

  section \<open>Set from Map Adapter\<close>

  (* We can get a set implementation from a map implementation, by using unit values *)
  type_synonym 'a rbts = "('a,unit) rbt"
  definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
  definition "rbts_set t \<equiv> { x. rbts_member x t }"
  definition "rbts_empty \<equiv> RBT.empty"
  definition "rbts_insert x t \<equiv> RBT.insert x () t"
  definition "rbts_delete x t \<equiv> RBT.delete x t"

  lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
    by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
  lemma rbts_member_correct: 
    "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
  lemma rbts_insert_correct:
    "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
  lemma rbts_delete_correct:
    "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)

  (* The correctness lemmas are obvious candidates for simp-lemmas: *)  
  lemmas rbts_set_correct[simp] = 
    rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct  

  section \<open>Efficient Remdup\<close>      

  (* Remove duplicates from list: *)  
  fun rd_aux where
    "rd_aux s [] = []"
  | "rd_aux s (x#xs) = (
      if rbts_member x s then rd_aux s xs
      else x # rd_aux (rbts_insert x s) xs
    )"  

  lemma rd_aux_set: "set (rd_aux s xs) = set xs - rbts_set s"
    apply (induction xs arbitrary: s)
    apply (auto)
    done
        
  lemma rd_aux_disjoint: "distinct (rd_aux s xs)"
    apply (induction xs arbitrary: s)
    apply (auto simp: rd_aux_set)
    done

  definition "rd xs \<equiv> rd_aux rbts_empty xs"  
    
  lemma rd_correct[simp]: "set (rd xs) = set xs" "distinct (rd xs)"
    unfolding rd_def
    by (auto simp: rd_aux_disjoint rd_aux_set)
    
  subsection \<open>Tail recursive version\<close>
  fun rd_aux_tr where
    "rd_aux_tr s r [] = r"  
  | "rd_aux_tr s r (x#xs) = (
      if rbts_member x s then rd_aux_tr s r xs
      else rd_aux_tr (rbts_insert x s) (x#r) xs
    )"  
    
  lemma rd_aux_tr_set: "set (rd_aux_tr s r xs) = set r \<union> (set xs - rbts_set s)"
    apply (induction xs arbitrary: s r)
    apply (auto)
    done
    
  lemma rd_aux_tr_disjoint: 
    "\<lbrakk>distinct r; rbts_set s = set r\<rbrakk> \<Longrightarrow> distinct (rd_aux_tr s r xs)"
    apply (induction xs arbitrary: s r)
    apply (auto)
    done
    
  definition "rd_tr xs \<equiv> rd_aux_tr rbts_empty [] xs"  
    
  lemma rd_tr_correct[simp]: "set (rd_tr xs) = set xs" "distinct (rd_tr xs)"
    unfolding rd_tr_def
    by (auto simp: rd_aux_tr_disjoint rd_aux_tr_set)

  value "rd_tr [1,5,6,1::nat]"
    
    
  (* Use Timing Panel! *)  
  value "length (rd ([1..<3000]@[1..<3000]))"  
  value "length (rd_tr ([1..<3000]@[1..<3000]))"  
  value "length (remdups ([1..<1000]@[1..<1000]))"  

  (* Possible (bonus) homework: nlogn remdups *)
  
  term "(2::nat) dvd n"
  
  
end
