chapter \<open>Homework 4\<close>
theory Homework4
imports Main "~~/src/HOL/Transcendental"
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #3
    RELEASED: Tue, Dec 07 2021
    DUE:      Tue, Dec 14, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)

section \<open>1: Directed Graph (25 Points)\<close>

(*
  Below you can find a definition of a directed graph which is a function in which a source node 
    and a destination node both of type 'v are mapped to a bool. The function evaluates to True 
    if there is an edge which starts in the source state and ends in the destination state. It 
    evaluates to False.
*)

type_synonym 'v dgraph = "'v \<Rightarrow> 'v \<Rightarrow> bool"

(*
  1a) Specify: In addition to the definition in the form of a function, it is also possible to use 
    an inductive definition. Now we define a path twice, once using a \<open>function\<close> and once using
    \<open>inductive\<close>. A path of directed graph E starts in some state s and ends in some state t and 
    visits all states in the list. E.g. path \<open>E s [t, u, v] v\<close> evaluates to True if \<open>E s t\<close> \<open>E t u\<close> 
    and \<open>E u v\<close> all evaluate to True.(5 pts.)
*)
fun path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u (x # xs) v \<longleftrightarrow> (E u x \<and> path E x xs v)"

inductive is_path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool"
(*<*)
(*>*)

(*
  1b) Show: both definitions are equal. (5 pts.)
  
  Hint: you may wat to prove both directions of the equivalence separately.
*)
lemma is_path_eq_path: "is_path E u xs v \<longleftrightarrow> path E u xs v"
(*<*)
  sorry
(*>*)


(*
  1c) Show: Two lists together form a path, iff each separate list forms a path on its own.
    Also, if there is a path with a loop, we retain a valid path if the loop is removed. (5 pts.)
*)

(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)

lemma is_path_append: "is_path E u (p1 @ p2) w \<longleftrightarrow> (\<exists>v. is_path E u p1 v \<and> is_path E v p2 w)"
(*<*)
  sorry
(*>*)


lemma is_path_elim_loop: "is_path E u (p1@w#p2@w#p3) v \<Longrightarrow> is_path E u (p1@w#p3) v"
(*<*)
  sorry
(*>*)

(*
  1d) Show: If there is a path between two states, then there exists also a distinct path. (10 pts.)
  HINT: Part of the proof has already been done. You need a new induction rule called length_induct
    Try finding out what is does. You will also need a built-in theorem called \<open>not_distinct_decomp\<close>
    which states that each list that is not distinct can be decomposed into three lists and an
    element which occurs twice. A distinct list is a list in which each element occurs only once.
    You can possibly use lemmas that you have already proved in this exercise.
*)


lemma path_distinct:
  assumes "is_path E u p v"
  shows "\<exists>p'. distinct p' \<and> is_path E u p' v"
  using assms
proof (induction p rule: length_induct)
  case step: (1 p)
  note IH = step.IH[rule_format] (* Note: rule_format converts a theorem 
    from \<open>\<forall>_. _ \<longrightarrow> _\<close> to \<open>\<And>_. _ \<Longrightarrow> _\<close> style, which makes it easier to apply using    
    single-step methods like rule. Useful in cases like this one, where the induction rule 
    is phrased in HOL (\<longrightarrow>) style.
  *)
  note prems = step.prems
  show ?case proof (cases "distinct p")
(*<*)
    case True show ?thesis sorry
  next
    case False show ?thesis sorry
  qed
qed
(*>*)


section \<open>2: Register Machine (15 points)\<close>

(*
  We give a set of operations similar to the arithmetic expressions we have seen before.
    Additionally, there is a \<open>conditional\<close> argument which evaluates the second argument if the
    first one is non-zero and it evaluates the third argument if the first one is zero. We have
    also given the evaluation function \<open>eval\<close>.
*)

type_synonym vname = string
type_synonym val = int
  
datatype exp = 
    Lit val 
  | Var vname 
  | Binop "val \<Rightarrow> val \<Rightarrow> val" exp exp \<comment> \<open>Any binary operator\<close>
  | Cond exp exp exp  \<comment> \<open>Ternary expression: Like \<open>c?a:b\<close> in Java\<close>


type_synonym state = "vname \<Rightarrow> val"
fun eval :: "exp \<Rightarrow> state \<Rightarrow> val" where
  "eval (Lit i) _ = i"
| "eval (Var x) s = s x"
| "eval (Binop f a b) s = f (eval a s) (eval b s)"  
| "eval (Cond c a b) s = (if eval c s \<noteq> 0 then eval a s else eval b s)"

(*
  Now we consider a register machine, with the following instructions
*)
type_synonym regname = nat
type_synonym regstate = "regname \<Rightarrow> val" 

datatype instr = 
  Loadi regname val   \<comment> \<open>Load immediate\<close>
| Loadv regname vname \<comment> \<open>Load variable\<close>
| Opr "val \<Rightarrow> val \<Rightarrow> val" regname regname \<comment> \<open>Binary op, result goes to first register\<close>
| CMov regname regname regname \<comment> \<open>Conditional move, r2:=r3 if r1\<noteq>0\<close>
  

(*
  The following is the semantics of a single instruction
*)
fun execi :: "state \<Rightarrow> instr \<Rightarrow> regstate \<Rightarrow> regstate" where
  "execi s (Loadi r i) \<rho> = \<rho>(r:=i)"
| "execi s (Loadv r x) \<rho> = \<rho>(r:=s x)"  
| "execi s (Opr f r1 r2) \<rho> = \<rho>(r1:=f (\<rho> r1) (\<rho> r2))"
| "execi s (CMov r1 r2 r3) \<rho> = (if \<rho> r1 \<noteq> 0 then \<rho>(r2:=\<rho> r3) else \<rho>)"
    
(* And of a list of instructions (basic block) *)
type_synonym basic_block = "instr list"

fun execb :: "state \<Rightarrow> basic_block \<Rightarrow> regstate \<Rightarrow> regstate" where
  "execb s [] sr = sr" 
| "execb s (i#is) sr = execb s is (execi s i sr)"
    
(* 
  2a) Show: Fold can be used to execute programs and a serial execution of to instruction lists 
    yields the same results as executing the second list on the result of executing the first one.
*)  
lemma "execb s xs sr = fold (execi s) xs sr"
(*<*)
  sorry
(*>*)

lemma execb_append[simp]: "execb s (xs1@xs2) sr = execb s xs2 (execb s xs1 sr)"
(*<*)
  sorry
(*>*)
 
(* 
  2b) Specify: We now want to compile an expression to a list of instructions. The \<open>regname\<close> 
    argument represents the register to which the output is eventually written. (5 pts.)
*)
fun compile_exp :: "exp \<Rightarrow> regname \<Rightarrow> basic_block" where
  "compile_exp _ _ = undefined"

(*
  2c) Show: the compilation of these expressions is correct. I.e. executing the compiled list of 
    instructions is the same as evaluating the original expression.
*)    
lemma "let \<rho>' = execb s (compile_exp e r) \<rho> in (\<forall>r'<r. \<rho>' r' = \<rho> r') \<and> \<rho>' r = eval e s"
(*<*)
  sorry
(*>*)


section \<open>Bonus: Irrationality of log (5 points)\<close>
text \<open>
  Show: The logarithm of 2 with base 3 is irrational.
  HINT: do the proof on paper first!
    Also take a look at the tutorial and the example proof at the following link:
    \<^url>\<open>https://en.wikipedia.org/wiki/Isabelle_(proof_assistant)\<close>
    A more general proof for all primes can be found under:
    \<^file>\<open>$ISABELLE_HOME/src/HOL/ex/Sqrt.thy\<close>
\<close>

(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)


lemma "(log 3 2) \<notin> \<rat>"
(*<*)
  sorry
(*>*)

end

