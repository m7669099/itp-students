section "Arithmetic and Boolean Expressions"

theory "tut04-1" imports Main "HOL.Transcendental" "HOL-Computational_Algebra.Primes"
begin

section \<open>Context-Free Grammar\<close>

text \<open>We define a grammar for palindromes consisting of the two symbols \<open>a\<close> and \<open>b\<close>:
  S \<longrightarrow> aSa
  S \<longrightarrow> bSb
  S \<longrightarrow> \<epsilon>

We can define this in Isabelle/HOL using an inductive definition.
\<close>

datatype ab = a | b

inductive S :: "ab list \<Rightarrow> bool" where
(*<*)
  S_nil: "S []" 
| S_a: "S xs \<Longrightarrow> S (a # xs @ [a])"
| S_b: "S xs \<Longrightarrow> S (b # xs @ [b])"
(*>*)

text \<open>
  Prove that if \<open>xux\<close> (where \<open>x\<close> is a symbol and \<open>u\<close> is a word) is a word in the language,
  then \<open>u\<close> is also a word in the language:
\<close>

thm S.cases

lemma palindromes_S:
  assumes "S (x # u @ [x])"
  shows "S u"
(*<*)
  using assms
proof(cases)
  case (S_a xs)
then show ?thesis by blast
next
  case (S_b xs)
  then show ?thesis by blast
qed
(*>*)


text \<open>Also prove that there are no words \<open>aub\<close> or \<open>bua\<close> in the language:\<close>

thm ccontr

lemma non_palindromes_not_S: "x \<noteq> y \<Longrightarrow> \<not> S (x # u @ [y])"
(*<*)
proof(rule ccontr; simp)
  assume "S (x # u @ [y])" and NEQ: "x \<noteq> y"
  then show False proof cases
    case (S_a xs)
    then show ?thesis using NEQ by force
  next
    case (S_b xs)
    then show ?thesis using NEQ by fast
  qed
qed
(*>*)


text \<open>Show that an extension of a word is only valid iff the extension on the end is the same
      symbol as the one at the beginning.\<close>

thm non_palindromes_not_S

lemma "S u \<Longrightarrow> S (x # u @ [y]) \<longleftrightarrow> x = y"
(*<*)
  apply auto
   apply(rule ccontr)
   apply(auto simp: non_palindromes_not_S)
  apply(cases y)
   apply(auto simp: S.intros)
  done

thm S.intros
(*>*)


section \<open>Paths vs. Transitive Closure\<close>


text \<open>We have defined a directed graph as a function which maps a source state and a destination
      state to a bool which evaluates to True if there is an edge starting in the source and 
      ending in the destination.\<close>

type_synonym 'v dgraph = "'v \<Rightarrow> 'v \<Rightarrow> bool"


text \<open>A path is a list of states that are connected through edges.\<close>

fun path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u (x # xs) v \<longleftrightarrow> (E u x \<and> path E x xs v)"
(*>*)


text \<open>The transitive closure contains an edge iff there is a path between the nodes of this edge\<close>

(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)

find_theorems name:induct "?E\<^sup>*\<^sup>*"

lemma rtranclp_to_path: "E\<^sup>*\<^sup>* u v \<Longrightarrow> \<exists>xs. path E u xs v"
proof(induction rule: converse_rtranclp_induct)
  case base
  have "path E v [] v" by simp
  then show ?case by blast
next
  case (step y z)
  then obtain xs where "path E z xs v" by blast
  hence "path E y (z # xs) v" using step by simp 
  then show ?case by fast
qed
  

lemma path_to_rtranclp: "path E u xs v \<Longrightarrow> E\<^sup>*\<^sup>* u v"
  apply(induction xs arbitrary: u) 
   apply auto
  by fastforce
  
  

lemma "E\<^sup>*\<^sup>* u v \<longleftrightarrow> (\<exists>xs. path E u xs v)"
(*<*)
  apply(auto simp: rtranclp_to_path path_to_rtranclp)
  done
(*>*)

find_theorems "\<rat>" "?a / ?b"
find_theorems name: power_mult

section \<open>Irrationality of sqrt 3\<close>

lemma sqrt_3_irrational: "sqrt 3 \<notin> \<rat>"
(*<*)
proof(rule ccontr; simp)
  assume "sqrt 3 \<in> \<rat>"
  then obtain a b::nat where "\<bar>sqrt 3\<bar> = a / b" and COPRIME: "coprime a b" by (rule Rats_abs_nat_div_natE)
  then have "\<bar>sqrt 3\<bar> * b = a" by force
  then have "\<bar>sqrt 3\<bar> ^ 2 * b ^ 2 = a ^ 2" using of_nat_power power_mult_distrib by metis
  hence eq: "3 * b ^ 2 = a ^ 2" using of_nat_eq_iff by fastforce
  hence "3 dvd a ^ 2" by presburger
  hence a_3: "3 dvd a" by(simp add: prime_dvd_power)
  moreover have "3 dvd b"
  proof -
    from a_3 obtain k::nat where "a = 3 * k" by fast
    with eq have "3 ^ 2 * k ^ 2 = 3 * b ^ 2" by force
    hence "3 * k ^ 2 = b ^ 2" by force
    hence "3 dvd b ^ 2" by presburger
    thus "3 dvd b" by(simp add: prime_dvd_power)
  qed    
  ultimately have "\<not> coprime a b" unfolding coprime_def
    using nat_dvd_1_iff_1 numeral_eq_one_iff by blast
  thus False using COPRIME by blast
qed
(*>*)

end
