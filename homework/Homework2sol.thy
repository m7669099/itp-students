chapter \<open>Homework 2\<close>
theory Homework2sol
imports Main
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #2
    RELEASED: Mon, Nov 22 2021
    DUE:      Tue, Nov 30, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)  


section \<open>1: Equality of functions (10 points)\<close>
(*
  First of all, we take a look at the two definitions for a count function.
  We have discussed this function in the first practical session. It counts 
  the number of occurrences of an element in a list. We also specified some
  lemmas which we did not prove yet, time to change that. (5 pts.)
*)

fun count :: "'a list \<Rightarrow> 'a \<Rightarrow> nat"
  where 
  "count [] _ = 0" (* \<leftarrow> add equation(s) here! *)
| "count (b#bl) a = count bl a + (if a=b then 1 else 0)"  



(*
  1a) Show: counting the occurrence in two appended list is equal to the sum
    of the occurrences in each separate list (5 pts.)
*)
lemma "count (xs @ ys) x = count xs x + count ys x"
  (*<*)
  apply (induction xs)
  apply auto
  done
  (*>*)

(*
  The following alternative definition of count using \<open>length\<close> and \<open>filter\<close> was
  also given in the practical
*)

definition "count' xs x \<equiv> length (filter (\<lambda>y. x=y) xs)"

(*
  1b) Show: These definitions for count are equal, at least, that is what you need to prove. 
    Good luck! (5 pts.)

  HINT: Isabelle/HOL can automatically look into functions. However, it 
  does not do this for definitions. So it does not look into count' unless
  you tell it to do so. This is why we need to do "unfolding count'_def".
*)

lemma "count xs x = count' xs x"
  unfolding count'_def
  (*<*)
  apply (induction xs) 
  apply auto
  done
  (*>*)


section \<open>2: Reverse (16 Points)\<close>
(*
  You defined the reverse function in the first practical session, now
  it's time to prove some basic properties of this function. Note that
  you cannot prove all lemmas in one step. You may need to break them
  down into smaller statements.
*)

fun snoc :: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "snoc [] n = [n]"
| "snoc (x#xs) n = x # (snoc xs n)"


fun reverse :: "'a list \<Rightarrow> 'a list" where
  "reverse [] = []"
| "reverse (x#xs) = snoc (reverse xs) x"


(*
  2a) Show: the reverse of a reversed list yields the original list (5 pts.)
  HINT: You need an auxiliary lemma to complete the proof.
*)   
  (*<*)
lemma rev_snoc_aux[simp]: "reverse (snoc xs x) = x#reverse (xs)"
  by (induction xs) auto
  (*>*)

lemma reverse_reverse_id: "reverse (reverse xs) = xs"
  (*<*)
  apply (induction xs)
   apply auto
  done
  (*>*)


(*
  2b) Show: You have shown lemma 2a using an auxiliary lemma. There are multiple ways to
    use an auxiliary lemma in your proofs. Show another way to do this different to the one 
    you have used in exercise 2a. You may use the same auxiliary lemma. (1 pt.)
*)
lemma "reverse (reverse xs) = xs"
  (*<*)
  apply (induction xs)
  apply simp
   apply(simp add: rev_snoc_aux)
  done
  (*>*)


(*
  2c) Show: the reverse of two appended lists is equal to the reverse of both lists
    appended in the opposite order. (5 pts.)
  HINT: auxiliary lemmas may be needed.
*)
  (*<*)
lemma [simp]: "snoc (xs@ys) x = xs @ (snoc ys x)"  
  apply (induction xs)
  apply auto
  done
  (*>*)
  
lemma "reverse (xs\<^sub>1 @ xs\<^sub>2) = reverse xs\<^sub>2 @ reverse xs\<^sub>1"
  (*<*)
  apply (induction xs\<^sub>1)
  apply auto
  done
  (*>*)

(*
  2d) Specify and show: Reversing a list should not change the length  of the list. 
    Using the definitions of \<open>length\<close> and \<open>reverse\<close>, specify a lemma that represents 
    this statement and prove it. (5 pts.)
  HINT: auxiliary lemmas may be needed, remember what you learned in 2b.
*)
  (*<*)
lemma [simp]: "length (snoc xs x) = length xs + 1"
  by (induction xs) auto
  (*>*)
  
lemma
  (*<*)
  "length (reverse xs) = length xs"  
  apply (induction xs)
  apply auto
  done
  (*>*)


section \<open>3: Summation of lists (14 points)\<close>

(* 
  3a) Specify: a function to sum up all elements in a list of integers. (4 pts.)
  HINT: The empty list has sum 0.
*)  
  
fun listsum :: "int list \<Rightarrow> int" where
  (*<*)  
  "listsum [] = 0"
| "listsum (x#xs) = x + listsum xs"  
  (*>*)

(* 
  3b) Specify and show: Filtering zeroes from a list does not affect its sum. 
    Specify a lemma that represents this statement using \<open>listsum\<close> and 
    \<open>filter\<close>. (5 pts.)
  HINT: auxiliary lemmas may be needed.
*)    
lemma  
  (*<*)
  "listsum (filter (\<lambda>x. x\<noteq>0) l) = listsum l"
  by (induction l) auto  
  (*>*)

(* 
  3c) Show: Reversing a list does not affect the sum. (5 pts.)
  HINT: auxiliary lemmas may be needed.
*)
  (*<*)
lemma [simp]: "listsum (snoc xs a) = listsum xs + a"
  apply(induction xs)
   apply auto
  done
  (*>*)

lemma "listsum (reverse xs) = listsum xs"    
  (*<*)
  apply (induction xs)
   apply auto
  done
  (*>*)  


section \<open>Bonus: Delta-Encoding (5 bonus points)\<close>      
(*
  We want to encode a list of integers as follows: 
    The first element is unchanged, and every next element 
    only indicates the difference to its predecessor.

  For example: (Hint: Use this as test cases for your spec!)
    denc [1,2,4,8] = [1,1,2,4]
    denc [3,4,5] = [3,1,1]
    denc [5] = [5]
    denc [] = []


  Background: This algorithm may be used in lossless data compression, 
    when the difference between two adjacent values is expected to be 
    small, e.g., audio data, image data, sensor data.

  It typically requires much less space to store the small deltas, than 
  the absolute values. 

  Disadvantage: If the stream gets corrupted, recovery is only possible 
    when the next absolute value is transmitted. For this reason, in 
    practice, one will submit the current absolute value from time to 
    time. (This is not modeled in this exercise!)
*)
      
  
(* 
  a) Specify: A function to encode a list with delta-encoding. 
    The first argument is used to represent the previous value, and can be initialized to 0. (1 pt.)
*)

fun denc :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  (*<*)
    "denc prv [] = []"
  | "denc prv (x#xs) = (x-prv)#denc x xs"  
  (*>*)
  
(* 
  b) Specify: Give a function representing the decoder. Again, the first argument represents 
    the previous decoded value, and can be initialized to 0. (1 pt.)
*)
fun ddec :: "int \<Rightarrow> int list \<Rightarrow> int list" where
  (*<*)
    "ddec prv [] = []"
  | "ddec prv (d#ds) = (prv+d) # ddec (prv+d) ds"
  (*>*)
  
(* 
  c) Show: encoding and then decoding yields the same list. 
  HINT: The lemma will need generalization. (2 pts.)
*)  
  (*<*)  
lemma dec_enc_id_aux: "ddec s (denc s l) = l"
    apply (induction l arbitrary: s) 
    apply auto
    done
  (*>*)
    
lemma dec_enc_id: "ddec 0 (denc 0 l) = l"
  (*<*)  
    by (simp add: dec_enc_id_aux)
  (*>*)

(*
  d) Show: Sometimes, you must use multiple auxiliary lemmata for a proof, complete the 
    following proof.
  HINT: It is possible to do this proof without induction, you may need to use lemmata
    from previous exercises on this sheet. (1 pt.)
*)

lemma "reverse (ddec 0 (denc 0 (reverse l))) = l"
  (*<*)
  by(simp add: reverse_reverse_id dec_enc_id)
  (*>*)

end