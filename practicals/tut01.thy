(*<*)
theory tut01
imports Main
begin
(*>*)



section \<open>Calculating with natural numbers\<close>

text \<open>
Use the \textbf{value} command to turn Isabelle into
a fancy calculator and evaluate the following natural
number expressions: $2+2$, $2*(5+3)$, $3*4 - 2*(7+1)$
\<close>

text \<open>
Can you explain the last result?
\<close>



section \<open>Natural number laws\<close>
text \<open>
Formulate and prove the well-known laws of commutativity and
associativity for addition of natural numbers.
\<close>

section \<open>Counting elements of a list\<close>

text \<open>
Define a function \<open>count\<close> which counts the number of occurrences of
a particular element in a list.
\<close>

definition "count \<equiv> undefined"

text \<open>
Test your definition of \<open>count\<close> on some examples.
\<close>

text \<open>
Prove the following inequality (and additional lemmas if
necessary) about the relation between @{term count} and
@{term length}, the function returning the length of a list.
\<close>

theorem "count xs x \<le> length xs" oops

section \<open>Adding elements to the end of a list\<close>

text \<open>
Recall the definition of lists from the lecture.  Define a
function \<open>snoc\<close> that appends an element at the right
end of a list.  Do not use the existing append operator
\<open>@\<close> for lists.
\<close>

text \<open>
Convince yourself on some test cases that your definition
of \<open>snoc\<close> behaves as expected, for example run:
\<close>

value "snoc [] c"

text \<open>
Also prove that your test cases are indeed correct,
for instance show:
\<close>
lemma "snoc [] c = [c]" oops

text \<open>
Next define a function \<open>reverse\<close> that reverses the
order of elements in a list.  (Do not use the existing
function @{term rev} from the library.)  Hint: Define
the reverse of \<open>x # xs\<close> using the @{term snoc}
function.
\<close>

text \<open>
Demonstrate that your definition is correct by running some
test cases, and proving that those test cases are correct.
For example:
\<close>

value "reverse [a, b, c]"
lemma "reverse [a, b, c] = [c, b, a]" oops

text \<open>
Prove the following theorem. Hint: You need to find an
additional lemma relating \<open>reverse\<close> and \<open>snoc\<close> to prove it.
\<close>
theorem "reverse (reverse xs) = xs" oops
