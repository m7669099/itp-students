theory DFS_Demo
imports Graph_Lib RBTS_Lib While_Lib
  (* Tell Isabelle to use efficient numbers (GMP) for evaluating nat and int with value *)
  "HOL-Library.Code_Target_Nat" "HOL-Library.Code_Target_Int"
begin

  section \<open>Abstract Algorithm\<close>
  (* Added operation rg_succs *)
  thm rg_succs_correct

  definition "dfs_aux gi v\<^sub>0 \<equiv> 
    while_option (\<lambda>(d,w). w\<noteq>[]) (\<lambda>(d,w). 
      let (v,w) = (hd w, tl w) in
      if v\<in>d then (d,w)
      else 
        let d = insert v d in
        let w = rg_succs gi v @ w in
        (d,w)
    
    ) ({},[v\<^sub>0]) 
  "

  definition "dfs gi v\<^sub>0 \<equiv> fst (the (dfs_aux gi v\<^sub>0))"

  
  section \<open>Correctness Proof\<close>
  
  text \<open>To show correctness, we implicitly fix an abstract and concrete graph, adn a start node. 
    This is just to make things more readable.
  \<close>
  context
    fixes gi and g :: "'v::linorder graph" and v\<^sub>0 :: 'v
    defines "g \<equiv> rg_\<alpha> gi"
  begin

    text \<open>Correctness can only be shown under the assumption that 
      only finitely many nodes are reachable from the start node!

      Fortunately, red-black trees can only hold finitely many nodes,
      and each successor list can only be finite. Thus, the graph is finite,
      and so is the set reachable from any node.
    \<close>
    lemma finite_reachable: "finite (g\<^sup>*``{v})"
    proof -
      have "g\<^sup>*``{v} \<subseteq> insert v (snd`g)"
        apply auto
        by (metis image_iff rtranclE snd_conv)
      also have "finite \<dots>" unfolding g_def using rg_finite_ran by simp
      finally (finite_subset) show ?thesis .
    qed        
      
    (*
      Invariant for DFS:
      
      \<^item> v\<^sub>0 is in d \<union> w
      
      \<^item> everything in d and w is reachable
      
      \<^item> making one step from d ends you in d \<union> w
    *)

    definition "dfs_invar \<equiv> \<lambda>(d,w). 
      v\<^sub>0\<in>d \<union> set w
    \<and> d \<union> set w \<subseteq> g\<^sup>*``{v\<^sub>0}
    \<and> g``d \<subseteq> d \<union> set w
    "

    (* Note: if set is closed by step, it's refl-transitively closed *)
    thm Image_closed_trancl
    
    lemma invar_init: "dfs_invar ({},[v\<^sub>0])"
      unfolding dfs_invar_def
      by (simp)
    
    lemma invar_step1: "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<in>d \<Longrightarrow> dfs_invar (d,tl w)"
      unfolding dfs_invar_def
      apply (cases w; simp)
      apply auto
      done
      
    lemma invar_step2: "dfs_invar (d,w) \<Longrightarrow> w\<noteq>[] \<Longrightarrow> hd w\<notin>d \<Longrightarrow> dfs_invar (insert (hd w) d, rg_succs gi (hd w) @ tl w)"
      unfolding dfs_invar_def
      
      thm rg_succs_correct (* Does not know about g, but uses rg_\<alpha> gi *)
      
      apply (cases w; simp)
      apply (auto simp flip: g_def)
      done
      
    lemma invar_final: "dfs_invar (d,[]) \<Longrightarrow> d = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_invar_def
      apply (auto simp flip: g_def)
      thm Image_closed_trancl
      by (metis Image_closed_trancl rev_ImageI)


    (*
      Termination: either the unfinished reachable nodes decrease, or
        they stay the same, and the length of the worklist decreases:
    *)  
    definition "dfs_wfrel \<equiv> (inv_image finite_psubset (\<lambda>d. g\<^sup>*``{v\<^sub>0} - d)) <*lex*> measure length"
      

    lemma wfrel_aux1: "\<lbrakk>dfs_invar (d, w); w \<noteq> []; hd w \<notin> d\<rbrakk>
           \<Longrightarrow> g\<^sup>* `` {v\<^sub>0} - insert (hd w) d \<subset> g\<^sup>* `` {v\<^sub>0} - d"
      unfolding dfs_invar_def
      apply clarsimp
      (* Consider a 'clean' Isar proof here instead! *)
      by (smt (verit, ccfv_threshold) Diff_iff Diff_insert Diff_subset in_mono list.set_sel(1) psubsetI singletonI)
    
    lemma dfs_aux_correct: "dfs_aux gi v\<^sub>0 = Some (g\<^sup>*``{v\<^sub>0},[])"
      unfolding dfs_aux_def
      apply (rule while_option_rule'[where P=dfs_invar and r=dfs_wfrel])
      apply (auto simp: invar_init invar_step1 invar_step2 dest: invar_final)
      unfolding dfs_wfrel_def
      apply auto []
      apply auto []
      using finite_reachable
      apply (clarsimp simp: wfrel_aux1)
      done

    lemma dfs_correct: "dfs gi v\<^sub>0 = g\<^sup>*``{v\<^sub>0}"  
      unfolding dfs_def by (simp add: dfs_aux_correct)
                
  end

  section \<open>Implementation\<close>
    
  definition "dfsi_aux gi v\<^sub>0 \<equiv> 
    while_option (\<lambda>(d,w). w\<noteq>[]) (\<lambda>(d,w). 
      let (v,w) = (hd w, tl w) in
      if rbts_member v d then (d,w)
      else 
        let d = rbts_insert v d in
        let w = rg_succs gi v @ w in
        (d,w)
    
    ) (rbts_empty,[v\<^sub>0]) 
  "

  section \<open>while-option refine\<close>
  thm while_option_refine (* \<leftarrow> look into proof! *)
  
  section \<open>Refinement Proof\<close>
  
  (* Straightforward! As expected. *)
  lemma dfsi_aux_refine: "rel_option (rel_prod rel_rbts (=)) (dfsi_aux gi v\<^sub>0) (dfs_aux gi v\<^sub>0)"
    unfolding dfsi_aux_def dfs_aux_def
    apply (rule while_option_refine)
    apply auto
    done

  definition "dfsi gi v \<equiv> fst (the (dfsi_aux gi v))"  

  (* Combine refinement and correctness lemma (transitivity) *)  
  lemma dfsi_correct: "rbts_set (dfsi gi v\<^sub>0) = (rg_\<alpha> gi)\<^sup>*``{v\<^sub>0}"
  proof -
    have "dfs_aux gi v\<^sub>0 \<noteq> None" using dfs_aux_correct[of gi v\<^sub>0] by simp
    hence "rbts_set (dfsi gi v\<^sub>0) = dfs gi v\<^sub>0"
      unfolding dfsi_def dfs_def
      using dfsi_aux_refine[of gi v\<^sub>0]
      by (cases "dfsi_aux gi v\<^sub>0"; cases "dfs_aux gi v\<^sub>0"; auto)
    also have "\<dots> = (rg_\<alpha> gi)\<^sup>*``{v\<^sub>0}" by (rule dfs_correct)
    finally show ?thesis .
  qed    
  
          
  section \<open>Benchmarks\<close>
  value "dfs (rg_from_lg [(0,1),(1,2),(2,1),(2,3),(4,5)]) (4::nat)"
    
  value "let n=10000 in dfs (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat) \<noteq> {}"
  value "let n=100000 in \<not>rbts_is_empty (dfsi (rg_from_lg (zip [0..<n] [1..<n+1])) (0::nat))"
  
  
  
    
  (*
    ctd here: 
      implement d-set as RBT
        \<rightarrow> we'll run into problem with refinement of while! \<rightarrow> use while_option!
      
      discussion: we used rbt-graph in abstract algorithm!
        using abstract graph is harder: successor nodes are unordered set,
          but have to be put in order to add them to worklist.
          The abstract algorithm would have to somehow fix this order, and it must be
          exactly the same order in which the concrete algorithm will see the successor elements,
          such that our simulation arguments work.
          However, the abstract algorithm has no means of guessing the right order, as it 
          knows nothing about the concrete implementation!
      
        \<rightarrow> this brings us to non-deterministic programs: 
          show that abstract algorithm is correct for any order!
          
      
      xmas-homework idea: 
      
        Be Creative! (80pt + bonus points for nice/elegant/difficult solutions)

        Develop your own piece of interesting theory. 
        Requirement: something must be proved, i.e., 
        just a functional program in Isabelle without correctness proof is not enough!
              
        Ideally, come up with your own idea. 
        Hint: keep it simple.
      
        You are welcome to send me your idea for discussion/feasibility check.
        
        DEADLINE: 18.01.2022 (you have two full (non-holiday) weeks). 
          But there will be another homework from 12.01, with deadline 18.01, too.
          (we trust in your time-management capabilities ;) )
        
        If you need inspiration what to do, here are a few suggestions:
      
        e.g. some graph/automata problem:
          add outer loop (fold) to DFS, to explore states from all roots
          topological sorting
          make: build recipe
          critical path in DAG
          shortest path from s to t (use BFS)
          find a cycle
          find a path from s to some node v with P v
      
  
  *)
  
  
      
end
