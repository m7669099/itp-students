theory Set_Monad_Demo
imports Main "HOL-Library.Monad_Syntax" "HOL-Library.Multiset"
begin

section \<open>Definition\<close>

(* Return x is {x}.*)

definition "return x \<equiv> {x}"

thm Set.bind_def

lemma "Set.bind m f = (\<Union>(f`m))" by auto

lemma "r\<in>Set.bind m f \<longleftrightarrow> (\<exists>x\<in>m. r\<in>f x)" by auto



(* Take any element x from s, then run f x *)
lemma 
  "do { x\<leftarrow>s; f x } = { r. \<exists>x. x\<in>s \<and> r\<in>f x }"
  by auto

(* Shorter *)  
lemma 
  "do { x\<leftarrow>s; f x } = \<Union>(f`s)"
  by auto

(* Intuition: possible results of nondeterministic computation. *)
  
section \<open>Monad Laws\<close>

(** Monad laws *)  
context
  notes [simp] = return_def
begin
  lemma "do { x \<leftarrow> return a; f x } = f a" by auto
  lemma "do { x \<leftarrow> m; return x } = m" by auto
  lemma "do { y \<leftarrow> do { x \<leftarrow> m; f x }; g y } = do { x\<leftarrow>m; y \<leftarrow> f x; g y }"
    for m :: "_ set"
    by (rule Set.bind_bind)

end  
  
(* Empty set: no possible results at all! *)  
lemma "do { x\<leftarrow>{}; f x } = {}" by simp
lemma "do { x\<leftarrow>m; {} } = {}" by auto
  

section \<open>Weakest Precondition\<close>

(* When is non-deterministic program correct? 
  All possible results satisfy specification!
*)
definition "wp s Q \<equiv> (\<forall>x\<in>s. Q x)"

(* called weakest precondition. 
  Naming: weakest thing that has to hold, for program to be correct.
  
  weak in sense of \<open>\<Longrightarrow>\<close>
*)

(*
  Rules for wp
*)

context
  notes [simp] = wp_def return_def
begin

lemma wp_return: "wp (return a) Q \<longleftrightarrow> Q a" by auto
lemma wp_bind: "wp (do { x\<leftarrow>m; f x}) Q = wp m (\<lambda>x. wp (f x) Q)" by auto

(* { x. P x } -- program that returns some x with P x *)
lemma wp_spec: "wp {x. P x} Q = (\<forall>x. P x \<longrightarrow> Q x)" by auto

lemmas wp_simps[simp] = wp_return wp_bind wp_spec
  
end


subsection \<open>Quicksort Spec\<close>

(* Example: partition a list. Any partition does the job. 
  Not caring about order or where elements equal to pivot go *)
definition partition_spec :: "'a::linorder \<Rightarrow> 'a list \<Rightarrow> 'a list \<times> 'a list \<Rightarrow> bool"
where "partition_spec p xs \<equiv> \<lambda>(ls,rs). 
  mset xs = mset ls + mset rs \<and> (\<forall>x\<in>set ls. x\<le>p) \<and> (\<forall>x\<in>set rs. x\<ge>p) 
"

(* Possible implementation: filter, pivots to the right *)
definition "partition_filter p xs \<equiv> return (filter (\<lambda>x. x<p) xs, filter (\<lambda>x. x\<ge>p) xs)"

lemma partition_filter_correct: "wp (partition_filter p xs) (partition_spec p xs)"
  unfolding partition_filter_def
  apply simp (* wp-simps *)
  unfolding partition_spec_def
  by (auto simp: not_le)

(* Another implementation: filter, pivots to the left *)
definition "partition_filter_left p xs \<equiv> return (filter (\<lambda>x. x\<le>p) xs, filter (\<lambda>x. x>p) xs)"

lemma partition_filter_left_correct: "wp (partition_filter_left p xs) (partition_spec p xs)"
  unfolding partition_filter_left_def
  apply simp (* wp-simps *)
  unfolding partition_spec_def
  by (auto simp: not_less) 


(* Sorting algorithm: we can use anything that satisfies spec *)

(* Tell function package that two binds are equal, already if second arguments 
  are equal only for possible first arguments.
  
  The termination prover uses it to create extra assumptions, that help with proving!
*)
lemma bind_cong[fundef_cong]: "s=s' \<Longrightarrow> (\<And>x. x\<in>s' \<Longrightarrow> f x = f' x) \<Longrightarrow> do { x\<leftarrow>s; f x } = do { x\<leftarrow>s'; f' x }" by auto
 

function (sequential) quicksort :: "'a::linorder list \<Rightarrow> 'a list set" where
  "quicksort [] = return []" 
| "quicksort (p#xs) = do {
    (ls,rs) \<leftarrow> Collect (partition_spec p xs);
    ls \<leftarrow> quicksort ls;
    rs \<leftarrow> quicksort rs;
    return (ls@p#rs) 
  }"  
  by pat_completeness auto
  
termination
  apply (relation \<open>measure length\<close>)
  apply (auto simp: partition_spec_def)
  apply (metis le_add1 le_imp_less_Suc size_mset size_union)
  by (metis less_add_Suc2 size_mset size_union)

  
(* 
  Note: correctness proof gets technical and complicated. 
  We'll skip it here, and come back later!
*)  
  
  
section \<open>Refinement\<close>

(* Monotonicity and Refinement *)  

(* Program satisfies specification: \<subseteq> *)
lemma wp_subset_conv: "wp s Q \<longleftrightarrow> s \<subseteq> {x. Q x}"
  by (auto simp: wp_def)

  
(* Refinement: s \<subseteq> s': all possible results of s, are also possible results of s'.

  In particular: if s' is correct, so is s
*)
  
(*
  Monotonicity: bind is monotone
*)    

lemma bind_mono:
  assumes "s\<subseteq>s'"  
  assumes "(\<And>x. x\<in>s \<Longrightarrow> f x \<subseteq> f' x)"
  shows "do { x\<leftarrow>s; f x } \<subseteq> do {x\<leftarrow>s'; f' x}"
  using assms by force

  
(*
  Now we can implement quicksort, using partition-filter
*)  
function (sequential) quicksort_impl :: "'a::linorder list \<Rightarrow> 'a list set" where
  "quicksort_impl [] = return []" 
| "quicksort_impl (p#xs) = do {
    (ls,rs) \<leftarrow> partition_filter_left p xs;
    ls \<leftarrow> quicksort_impl ls;
    rs \<leftarrow> quicksort_impl rs;
    return (ls@p#rs) 
  }"  
  by pat_completeness auto
  
termination
  apply (relation \<open>measure length\<close>)
  apply (auto simp: partition_filter_left_def return_def)
  subgoal by (meson le_imp_less_Suc length_filter_le)
  subgoal using length_filter_le less_Suc_eq_le by blast
  done

(*
  And show that it refines the original quicksort
*)
lemma "quicksort_impl xs \<subseteq> quicksort xs"  
  apply (induction xs rule: quicksort.induct)
  apply auto
  using partition_filter_left_correct[unfolded wp_subset_conv]
  by fast

  
  
lemma \<open>wp {} P\<close> by (simp add: wp_def)
  

(*
  Conclusions:

    nondeterminism: allow multiple possible solutions
    refinement: narrow solutions (e.g. by implementing spec)
  
    set-monad: monad for nondeterminism
    disadvantages:
      what if function does not always terminate? But only if precondition is met?
      even if function always terminate: computation induction gets ugly!
          
    Now: set-monad + option-monad = nondeterminism-error monad

*)

  
end
