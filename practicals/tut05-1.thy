
section "Arithmetic and Boolean Expressions"

theory "tut05-1" imports Main "HOL-Library.RBT"
begin

(* Given definitions and lemmas *)


(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_set t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

(* This was added *)
definition "rbts_is_empty t \<equiv> RBT.is_empty t"

lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
lemma rbts_insert_correct:
  "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
lemma rbts_delete_correct:
  "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_set t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_set_def)
qed  
  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_set_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct


(*
  Once again we define a graph. However, this time we define it as a set of edges, rather
    than as a function. In this tutorial, we are going to focus on abstraction. In some cases
    you don't want Isabelle/HOL to automatically simplify basic operations, as basic lemmas
    you have proven on these operations are not applicable anymore.
*)

type_synonym 'v graph = "('v\<times>'v) set"

definition empty_graph :: "'v graph" where 
(*<*)
  "empty_graph \<equiv> {}"
(*>*)

definition add_edge :: "('v\<times>'v) \<Rightarrow> 'v graph \<Rightarrow> 'v graph" where
(*<*)
  "add_edge e E \<equiv> E \<union> {e}" 
(*>*)


text \<open>We now define a graph again, but this time we don't define it as a set, but as a list.\<close>

type_synonym 'v list_graph = "('v\<times>'v) list"

definition lg_\<alpha> :: "'v list_graph \<Rightarrow> 'v graph" where
(*<*)
  "lg_\<alpha> xs = set xs"
(*>*)

definition lg_empty :: "'v list_graph" where 
(*<*)
  "lg_empty \<equiv> []"
(*>*)

definition lg_add_edge :: "('v\<times>'v) \<Rightarrow> 'v list_graph \<Rightarrow> 'v list_graph" where 
(*<*)
  "lg_add_edge \<equiv> (#)"
(*>*)

(*<*)
context
  notes [simp] = lg_\<alpha>_def lg_empty_def add_edge_def lg_add_edge_def
begin
lemma [simp]: "lg_\<alpha> lg_empty = {}"
  apply(auto)
  done

lemma [simp]: "lg_\<alpha> (lg_add_edge x xs) = add_edge x (lg_\<alpha> xs)"
  by auto
end
(*<*)


(*  
  The list data structure allows us to efficiently iterate over the edges, but if we want to
    find the adjacent vertices more quickly we have to iterate through the entire set of edges.
    This is very inefficient. Alternatively, we can use a different data structure that maps
    each node it a list of adjacent nodes.
*)

type_synonym 'v rbt_graph = "('v,'v list) rbt"

definition rg_\<alpha> :: "'v::linorder rbt_graph \<Rightarrow> 'v graph"  where 
(*<*)
  "rg_\<alpha> t \<equiv> {(u,v). \<exists>succs. RBT.lookup t u = Some succs \<and> v \<in> set succs}"
(*>*)

definition rg_empty :: "'v::linorder rbt_graph" where 
(*<*)
  "rg_empty \<equiv> RBT.empty"
(*<*)

definition rg_add_edge :: "('v::linorder\<times>'v) \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v rbt_graph" where
(*<*)
 "rg_add_edge \<equiv> (\<lambda> (u,v) t. case RBT.lookup t u of 
                  None \<Rightarrow> RBT.insert u [v] t |
                  Some succs \<Rightarrow> RBT.insert u (v # succs) t)"
(*>*)

(*<*)
context
  notes [simp] = rg_\<alpha>_def rg_empty_def add_edge_def rg_add_edge_def
begin
lemma [simp]: "rg_\<alpha> rg_empty = {}"
  apply(auto)
  done

thm prod.split

lemma [simp]: "rg_\<alpha> (rg_add_edge e t) = add_edge e (rg_\<alpha> t)"
  apply(auto split: prod.split option.split if_splits)
  done
end
(*<*)
  
(*

  Now we want to read a graph represented as list into a graph represented as red-black tree,
  and show that the abstraction of the graph is preserved by the read operation:

  lg - read \<rightarrow> rg
  |            |
 lg_\<alpha>         rg_\<alpha>
  |            |
  g     =      g
  
  
*)

definition read_lst :: \<open>'v::linorder list_graph \<Rightarrow> 'v rbt_graph\<close> where 
(*<*)
  "read_lst xs \<equiv> fold rg_add_edge xs rg_empty"
(*>*)


lemma lg_\<alpha>_alt: "lg_\<alpha> xs = fold add_edge xs (rg_\<alpha> rg_empty)"
proof -
  have "fold add_edge xs E = E \<union> lg_\<alpha> xs" for E
    apply(induction xs arbitrary: E)
     apply(auto simp: lg_\<alpha>_def add_edge_def)
    done
  from this[where E="{}"] show ?thesis by simp
qed

lemma \<open>rg_\<alpha> (read_lst xs) = lg_\<alpha> xs\<close> 
(*<*)
proof -
  have "rg_\<alpha> (fold rg_add_edge xs t) = fold add_edge xs (rg_\<alpha> t)" for t
    apply(induction xs arbitrary: t)
     apply auto
    done
  from this[where t="rg_empty"] show ?thesis unfolding read_lst_def lg_\<alpha>_alt .
qed
(*>*)

  
(* Abstract concept of nodes in a graph *)
definition V :: "'v graph \<Rightarrow> 'v set" where 
(*<*)
  "V E = fst ` E \<union> snd ` E"
(*>*)
term fst
term "(`)"
(* Abstract concept of dead end *)
definition has_dead_end :: "'v graph \<Rightarrow> bool" where
(*<*)
  "has_dead_end g \<equiv> \<exists>u \<in> V g. \<nexists>v. v \<in> V g \<and> (u, v) \<in> g"
(*>*)

(* Alternative abstract representation, that suggests implementation by set difference *)
lemma has_dead_end1: "has_dead_end g \<longleftrightarrow> snd`g - fst`g \<noteq> {}"
(*<*)
  unfolding has_dead_end_def V_def
  apply auto
  by force+
(*>*)

(* Concrete implementation (naive) *)
definition lg_has_dead_end :: "'v list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end xs \<equiv> set (map snd xs) - set (map fst xs) \<noteq> {}"
(*>*)

lemma lg_has_dead_end_correct: "lg_has_dead_end xs \<longleftrightarrow> has_dead_end (lg_\<alpha> xs)"
(*<*)
  unfolding lg_has_dead_end_def has_dead_end1 lg_\<alpha>_def
  by simp
(*>*)

(*
  Operation list to rbt set
*)

(* Add operation *)  
  
(* Note: This is a generic operation. 
  It's not specific to RBTs, but the pattern works for all 
  set implementations that support insert and empty  
*)

definition "rbts_from_list xs = fold rbts_insert xs rbts_empty"

lemma [simp]: "rbts_set (rbts_from_list xs) = set xs"
proof -
  have "rbts_set (fold rbts_insert xs t) = rbts_set t \<union> set xs" for t
    apply(induction xs arbitrary: t) by auto
  from this[where t="rbts_empty"] show ?thesis unfolding rbts_from_list_def by simp
qed


definition "rbts_diff_list s xs = fold rbts_delete xs s"

lemma [simp]: "rbts_set (rbts_diff_list s xs) = rbts_set s - set xs"
  unfolding rbts_diff_list_def
  apply(induction xs arbitrary: s)
   apply auto
  done

(* More efficient implementation with two passes, and red-black-tree *)
definition lg_has_dead_end_rbt :: "'v::linorder list_graph \<Rightarrow> bool" where 
(*<*)
  "lg_has_dead_end_rbt xs \<equiv> \<not>rbts_is_empty (rbts_diff_list (rbts_from_list (map snd xs)) (map fst xs))"
(*>*)

lemma "lg_has_dead_end_rbt xs = has_dead_end (lg_\<alpha> xs)"
(*<*)
  (* The correctness proof is done by stepwise refinement: 
  
    we show that lg_has_dead_end_rbt implements lg_has_dead_end, 
      which we have already shown to implement the specification.
      
      
    Note that this is similar to the transitivity proof we did for lg_has_dead_end_correct,
    but that the intermediate implementation was defined as its own constant, rather than
    just appearing on the RHS of a lemma.
  *)
proof -
  have "lg_has_dead_end_rbt xs = lg_has_dead_end xs"
    unfolding lg_has_dead_end_rbt_def lg_has_dead_end_def by force
  also have "... = has_dead_end (lg_\<alpha> xs)" unfolding lg_has_dead_end_correct by simp
  finally show ?thesis by blast
qed
(*>*)

end
