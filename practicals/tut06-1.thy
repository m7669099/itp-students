section "Arithmetic and Boolean Expressions"

theory "tut06-1"
imports "../Demos/Graph_Lib" "../Demos/RBTS_Lib" "../Demos/While_Lib" (* link these to the respective demo files on your machine *)
begin


section \<open>Specification\<close>

(* Loop reachable from v\<^sub>0: v\<^sub>0 \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v *)
definition "has_loop g v\<^sub>0 \<equiv> \<exists>v. (v\<^sub>0, v) \<in> g\<^sup>* \<and> (v, v) \<in> g\<^sup>+"

(*
  If there is a loop reachable from v, there is also a successor of v from which we can reach the loop.
  
  Intuition (\<Longrightarrow>):
    Either loop not yet reached: v\<^sub>0 \<rightarrow> v' \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v. Obviously, loop also reachable from successor v'.
    Or v\<^sub>0 is already part of loop: v\<^sub>0\<rightarrow>\<^sup>+v\<^sub>0. Just 'rotate' loop by one, to start and end at successor of v\<^sub>0.

  (\<Longleftarrow>): obvious
*)
lemma has_loop_iff_succ: "has_loop g u \<longleftrightarrow> (\<exists>v. (u,v)\<in>g \<and> has_loop g v)"
  apply (auto simp: has_loop_def)
  subgoal by (meson rtrancl_trancl_trancl tranclD)
  subgoal by (meson converse_rtrancl_into_rtrancl)
  done

(* Dest-version of this lemma, as it is not a good simp lemma! *)  
lemmas has_loop_succD = has_loop_iff_succ[THEN iffD1]



section \<open>Algorithm Definition\<close>

definition "dfs_find_cycle_aux gi v\<^sub>0 \<equiv> 
    while_option (\<lambda>(d,w,c). w\<noteq>[] \<and> \<not>c) (\<lambda>(d,w,c). 
      let stack = snd ` set w in
      let ((p,v),w) = (hd w, tl w) in
      case p of
        [] \<Rightarrow> (insert v d, w, False)
      | u # p \<Rightarrow> (
          if u \<in> d then (d, (p,v) # w, False)
          else if u \<in> stack then (d, w, True)
          else (d, (rg_succs gi u ,u) # (p, v) # w, False)
      )
    ) ({},[(rg_succs gi v\<^sub>0,v\<^sub>0)],False) 
"

definition "dfs_find_cycle gi v\<^sub>0 \<equiv> let (_,_,c) = the (dfs_find_cycle_aux gi v\<^sub>0) in c"


section \<open>Invariant\<close>

text \<open>
  We present the invariant here.
  Note that, in practice, one will typically start with a too weak invariant,
  and only discover what is needed while trying to prove its preservation.

  So the proof will cycle between correctness proof and adjusting the invariant multiple times.
  
  When developing this tutorial, we started with an invariant, 
  that we knew would only prove soundness and termination. We had to modify this about 3 times.
  
  Only then, we added the invariant for completeness. This also was changed 3 times before it worked.
  
  Note that our final invariants are not perfect! We could prove them, and they prove the algorithm correct.
  But there are certainly more concise formulations!
  
  Moreover, during the proof we discovered a bug in our initial algorithm, that, of course, 
  needed fixing before we could complete the proof.
\<close>

context
  fixes gi and g :: "'v::linorder graph" and v\<^sub>0 :: 'v
  defines "g \<equiv> rg_\<alpha> gi"
begin

(*
  Building the invariant:
    We split the invariant into different, mostly independent parts.
    This is to keep the subgoals readable. 
*)

(*
  The first part we do separately is the fact that the nodes on the stack form a path.
  This is obvious, as we only ever push a successor of the current top node.

  We phrase a slightly weaker property here: 
    From each node on the stack, we can reach any node above that node.
    
*)
definition "stack_reachable w = (\<forall>ws p v ws'. w = ws @ (p,v) # ws' \<longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v})"

(*
  Next, we summarize all the invariants that hold when no cycle is found yet.
  
  We define the invariants required for soundness, and for completeness separately:
*)
(*
  Soundness: 
  These are required to maintain the property that the stack forms a path, 
    and is reachable. 
  Thus, if we find a back-edge, we know we close a reachable cycle.
  
  Also, knowing that we only work on reachable nodes (including the done ones), 
  is required for the termination argument.
*)
definition "loop_invar_sound d w \<equiv> 
  d \<union> (snd ` set w) \<subseteq> g\<^sup>*``{v\<^sub>0}
\<and> (\<forall>(p,v) \<in> set w. set p \<subseteq> g``{v})
\<and> stack_reachable w
"

  value "{(1,2), (2,3), (2,4), (3::nat,5::nat)}``{2::nat}"

(*
  Completeness:
  Required to maintain the property that no cycles are reachable from done nodes, and v\<^sub>0 is such a node at the end.

  The main idea to maintain this invariant is, that all successors of a node on the stack are one of
    1) done (no loop)
    2) still pending
    3) are the next node on the stack
  
  accordingly, a loop from the top node of the stack must go through a pending node,
  and a loop from any node in the middle of the stack must go through a pending node, 
  or through the next node on the stack.
  
*)        
definition "loop_invar_complete d w \<equiv>   
  v\<^sub>0 \<in> d \<union> snd ` set w
\<and> (\<forall>v\<in>d. \<not> has_loop g v\<^sub>0)
\<and> (\<forall>p v w\<^sub>2. w=(p,v)#w\<^sub>2 \<and> has_loop g v \<longrightarrow> (\<exists>v' \<in> set p. has_loop g v'))
\<and> (\<forall>w\<^sub>1 p\<^sub>1 v\<^sub>1 p\<^sub>2 v\<^sub>2 w\<^sub>2. w=w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2 \<and> has_loop g v\<^sub>2 \<longrightarrow> (\<exists>v'\<in>insert v\<^sub>1 (set p\<^sub>2). has_loop g v'))" 

(*
  Finally, we assemble the main invariant:
    if we indicate that we have found a cycle, there is one.
    otherwise, the soundness and completeness invariants still hold.
   
  This type of invariant, i.e., not requiring anything but the cycle on breaking the loop,
  is typical for invariants that involve breaking the loop with a flag.  
*)
definition "loop_invar \<equiv> \<lambda>(d, w, c). (if c then has_loop g v\<^sub>0 else loop_invar_sound d w \<and> loop_invar_complete d w)"


subsection \<open>Invariant auxiliary lemmas\<close>

(*
  In order to simplify later reasoning, we show a few
  auxiliary lemmas, that can be used to manually instantiate the 
  \<open>ws @ (p,v) # ws'\<close> and \<open>w=w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2\<close> patterns used in the invariants.
  Note that auto and friends will typically struggle with such instantiations, as
  the simplifier will change the associativity, e.g.

*)

notepad begin
  fix xs zs :: "'a list" and x y
  let "?xs@?y#?zs" = "xs@y#zs"  (* Matches *)
  
  (* let "?xs@?y#?zs" = "x#xs@y#zs"  (* Does not match! *) *)
  let "?xs@?y#?zs" = "(x#xs)@y#zs"  (* This matches *)
  
  (* But, simplifier will rewrite  (x#xs)@y#zs \<rightarrow> x#xs@y#zs, 
    before any such pattern will be matched!
  *)
  
end

lemma stack_reachableD: assumes "stack_reachable w" "w = ws @ (p,v) # ws'" shows "snd ` (set ws) \<subseteq> g\<^sup>+``{v}"
  using assms
  unfolding stack_reachable_def
  by blast

lemma stack_reachableI: assumes "\<And>ws p v ws'. w = ws @ (p,v) # ws' \<Longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v}" shows "stack_reachable w"
  using assms
  unfolding stack_reachable_def
  by blast

lemma stack_reachableI':
  assumes "\<And>w\<^sub>1 w\<^sub>2 w\<^sub>3 p\<^sub>1 p\<^sub>2 u\<^sub>1 u\<^sub>2. w = w\<^sub>1@(p\<^sub>1,u\<^sub>1)#w\<^sub>2@(p\<^sub>2,u\<^sub>2)#w\<^sub>3 \<Longrightarrow> (u\<^sub>2,u\<^sub>1)\<in>g\<^sup>+"
  shows "stack_reachable w"
  using assms unfolding stack_reachable_def
  by (auto simp: in_set_conv_decomp)
    
lemma loop_invar_complete_middleD:
  assumes "loop_invar_complete d (w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2)"
  assumes "has_loop g v\<^sub>2"
  shows "\<exists>v'\<in>insert v\<^sub>1 (set p\<^sub>2). has_loop g v'"
  using assms unfolding loop_invar_complete_def
  by blast
  



  
section \<open>Invariant Preservation\<close>  

(*
  At this point in the proof, one will start with the main correctness lemma.
  Having defined the invariant as a constant, 
  after applying while-rule and some goal massaging (clarsimp, auto, etc),
  one should see the different branches of the algorithm, and what invariant preservation lemmas are required.
  These are then proved SEPARATELY. This, again, is a method to keep subgoals readable, and
  to allow for more flexible changes: after a change, things will break locally, rather than somewhere in a 
  big and messy apply auto try0+sledgehammer proof.
*)

subsection \<open>Init\<close>
(* Invariant holds initially. Proof is simple enough for one-liner *)
lemma invar_init: "loop_invar ({},[(rg_succs gi v\<^sub>0, v\<^sub>0)],False)"
  unfolding loop_invar_def loop_invar_sound_def stack_reachable_def loop_invar_complete_def
  by (auto simp: Cons_eq_append_conv dest: has_loop_succD simp flip: g_def)

  
subsection \<open>Pending list empty, pop node\<close>
  
(*
  The remaining lemmas have been, subsequently, split by the defined parts of the invariant.
*)  
lemma stack_reachable_tail: assumes "local.stack_reachable ((px, vx) # w)" shows "local.stack_reachable w"
  unfolding stack_reachable_def
proof clarsimp
  fix ws p v ws' p' v'
  assume "(p', v') \<in> set ws" and [simp]: "w = ws @ (p, v) # ws'"
  then obtain ws1 ws2 where "ws = ws1 @ (p', v') # ws2" using in_set_conv_decomp by metis
  hence 1: "(px, vx) # w = ((px, vx) # ws1 @ (p', v') # ws2) @ (p, v) # ws'" by simp
  from stack_reachableD[OF assms this] show "(v, v') \<in> g\<^sup>+" by simp
qed


subsection \<open>Next pending node already done\<close>  
  
lemma stack_reachable_change_pending: assumes "stack_reachable ((p', v) # w) " shows "stack_reachable ((p, v) # w)"
proof (rule stack_reachableI)
  fix ws pa va ws'
  assume DECOMP: "(p, v) # w = ws @ (pa, va) # ws'"
  show "snd ` set ws \<subseteq> g\<^sup>+ `` {va}"
  proof(cases ws)
    case Nil
    then show ?thesis by simp
  next
    case (Cons a list)
    then show ?thesis using DECOMP stack_reachableD[OF assms, of "(p', v) # list"] by auto
  qed
qed
  

subsection \<open>Next pending node on stack. Loop found!\<close>  
  
(* Our invariants require us to show two cases, one for a loop with at least two edges,
  and one for a self-loop over the top of the stack: *)
  
subsection \<open>Pushing node on stack\<close>

  
section \<open>Main Proof\<close>
(*
  Note, this proof will be started first, and then
  all the auxiliary lemmas in the previous sections are incrementally added!
*)


subsection \<open>Termination\<close>

(*
          We find a cycle 
  <*lex*> nodes not yet on the stack nor done decrease
  <*lex*> stack decreases
  <*lex*> length of pending nodes on top of stack decreases

  The inv_image maps our state to the things that decrease.
*)
definition "dfs_wfrel \<equiv> (inv_image ({(True, False)} <*lex*> finite_psubset <*lex*> less_than <*lex*> less_than) 
  (\<lambda>(d,w,c). (
    c,
    g\<^sup>*``{v\<^sub>0} - (d \<union> snd ` set w),
    length w,
    length (fst (hd w))
)))"

lemma finite_reachable[simp, intro!]: "finite (g\<^sup>*``{v})"
proof -
  have "g\<^sup>*``{v} \<subseteq> insert v (fst`g \<union> snd`g)"
    apply auto
    by (metis image_iff rtranclE snd_conv)
  also have "finite \<dots>" unfolding g_def using rg_finite_dom rg_finite_ran by simp
  finally (finite_subset) show ?thesis .
qed       


lemma compl_subsetI:
  assumes "b\<subseteq>a" "c\<subseteq>a" "c\<subset>b"
  shows "a-b \<subset> a-c"
  using assms by blast

(* Auxiliary lemma to show that invar-complete implies no loops at the end.
  The remaining \<dots>-final properties are shown in-line.
*)  
lemma loop_invar_complete_final: 
  assumes LIC: "loop_invar_complete d []"
  shows "\<not>has_loop g v\<^sub>0"
proof -
  from LIC have \<open>v\<^sub>0\<in>d\<close> unfolding loop_invar_complete_def by auto
  with LIC show ?thesis unfolding loop_invar_complete_def by auto
qed  


lemma "\<And>x b ba ys. \<lbrakk>local.loop_invar (x, ([], ba) # ys, False)\<rbrakk> \<Longrightarrow> local.loop_invar (insert ba x, ys, False)"
(*
 2. \<And>x b ba ys x22. \<lbrakk>local.loop_invar (x, (ba # x22, ba) # ys, False); s_ = (x, (ba # x22, ba) # ys, False); \<not> b; ba \<in> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, (x22, ba) # ys, False)
 3. \<And>x b ba ys x22. \<lbrakk>local.loop_invar (x, (ba # x22, ba) # ys, False); s_ = (x, (ba # x22, ba) # ys, False); \<not> b; ba \<notin> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, ys, True)
 4. \<And>x b ba ys x22 a bb. \<lbrakk>local.loop_invar (x, (bb # x22, ba) # ys, False); s_ = (x, (bb # x22, ba) # ys, False); \<not> b; (a, bb) \<in> set ys; bb \<in> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, (x22, ba) # ys, False) 
 5. \<And>x b ba ys x22 a bb. \<lbrakk>local.loop_invar (x, (bb # x22, ba) # ys, False); s_ = (x, (bb # x22, ba) # ys, False); \<not> b; (a, bb) \<in> set ys; bb \<notin> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, ys, True)
 6. \<And>x b ba ys x21 x22. \<lbrakk>local.loop_invar (x, (x21 # x22, ba) # ys, False); s_ = (x, (x21 # x22, ba) # ys, False); \<not> b; x21 \<noteq> ba; x21 \<notin> snd ` set ys; x21 \<in> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, (x22, ba) # ys, False)
 7. \<And>x b ba ys x21 x22.
       \<lbrakk>local.loop_invar (x, (x21 # x22, ba) # ys, False); s_ = (x, (x21 # x22, ba) # ys, False); \<not> b; x21 \<noteq> ba; x21 \<notin> snd ` set ys; x21 \<notin> x\<rbrakk> \<Longrightarrow> local.loop_invar (x, (rg_succs gi x21, x21) # (x22, ba) # ys, False
*)


lemma dfs_find_cycle_aux_correct: "\<exists>d w c. dfs_find_cycle_aux gi v\<^sub>0 = Some (d, w, c) \<and> (c = has_loop g v\<^sub>0)"
  unfolding dfs_find_cycle_aux_def
  apply (rule while_option_rule'[where P=loop_invar and r=dfs_wfrel])
  subgoal by(auto simp: invar_init)
  subgoal apply(auto simp: neq_Nil_conv split: list.splits)
      apply (auto simp: invar_init invar_step1 invar_step2 dest: invar_final)
  unfolding dfs_wfrel_def
      apply auto []
      apply auto []
  using finite_reachable
      apply (clarsimp simp: wfrel_aux1)
  done
  oops


end
