chapter \<open>Homework 3\<close>
theory Homework3sol
imports Main "~~/src/HOL/Library/Multiset"
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #3
    RELEASED: Mon, Nov 29 2021
    DUE:      Tue, Dec 07, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)

section \<open>General definitions\<close>
(*
  The following definitions have already been used in the lecture. It is now time to
  put them to use.
*)

text \<open>Start over with the syntax tree\<close>  
type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> val"

  

section \<open>1: Arithmetic expressions with exceptions (20 points)\<close>
(*
  We define a set of arithmetic expressions that supports addition as well as division.
  The out put is an integer value. However, remember that dividing by 0 does not yield
  an integer. In this case the output should be an exception.
*)

datatype aexp = N int | V vname | Plus aexp aexp | Div aexp aexp 

(*
  1a) Specify: The evaluation function for an expression that outputs an exception (in 
    the form of value \<open>None\<close>) if we divide by 0. (5 pts.)
*)


fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val option" where
(*<*)
  "aval (N i) s = Some i" 
| "aval (V x) s = Some (s x)"
| "aval (Plus a1 a2) s = (case (aval a1 s, aval a2 s) of 
      (Some v1, Some v2) \<Rightarrow> Some (v1+v2) 
    | (_, _) \<Rightarrow> None)" 
| "aval (Div a1 a2) s = (case (aval a1 s, aval a2 s) of
      (Some v1, Some v2) \<Rightarrow> (if v2 = 0 then None else Some (v1 div v2)) 
    | (_, _) \<Rightarrow> None)"
(*>*)


(*
  1b) Show: If everything is alright, an occurrence of 0 on the right hand side in any form
    should cause an exception to be thrown. Prove the following lemmas. (5 pts.)
    Hint: no induction needed ;)
*)

lemma "s x = 0 \<Longrightarrow> aval (Div a (V x)) s = None"
(*<*)  
  apply(auto split: option.split)
  done
(*>*)


lemma "aval (Div a (Plus (N (0-x)) (N x))) s = None"
(*<*)
  apply(auto split: option.split)
  done
(*>*)

(*
  1c) Specify: A function that correctly propagates all constants in an expression.
    In other words: if an operation (Div or Plus) is performed on a constant (N x)
    we evaluate the expression. (5 pts.)
    HINT: (Plus (N 3) (N 2)) becomes (N 5). Division by 0 is not evaluated.
*)

fun cprop :: "aexp \<Rightarrow> aexp" where
(*<*)
  (* @Bram: students might use smart constructors here, as I have discussed that in the lecture *)

  "cprop (N v) = N v"
| "cprop (V x) = V x"
| "cprop (Plus a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> N (va + vb)
    | (a,b) \<Rightarrow> Plus a b  
    )"
| "cprop (Div a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> (if vb = 0 then Div (N va) (N vb) else N (va div vb))
    | (a,b) \<Rightarrow> Div a b  
    )"
(*>*)


(*
  1d) Show: An expression with propagated constants evaluates to the same value as 
    the original expression. (5 pts.)
    
    Hint: you might need splitting. If you are left with 
      strange subgoals that auto cannot solve, or the invocation of auto takes a long time,
      you may try to split sequentially, i.e., first split on one, than on the other datatype.
      You might also try to use split!: instead of split: for a different strategy for 
      combining splitting and simplification.
    
*)

lemma cprop_correct: "aval (cprop a) s = aval a s" 
  apply (induction a)
  apply (auto split!: aexp.splits option.splits)
  done
  
lemma "aval (cprop a) s = aval a s" 
  apply (induction a)
  apply (auto split: aexp.splits option.splits) (* Takes long, but terminates *)
  done
  
lemma "aval (cprop a) s = aval a s" 
  apply (induction a)
  apply (auto split: aexp.splits)
  apply (auto split: option.splits) (* Produces trivial goals, but outside of auto's capabilities *)
  try0
  apply metis+
  done
  
lemma "aval (cprop a) s = aval a s" 
  apply (induction a) (* Sequential splitting can also be fast (Bram's proof) *)
  apply (auto split: option.split)
  apply (auto split: aexp.split)
  done


section \<open>2: Words on a Labelled Transition System(LTS) ( points)\<close>
(*
  We look at a simple definition of an LTS. Here we define it as a function that maps
  a source state of type \<open>'q\<close>, a symbol of type \<open>'a\<close> and a destination state of type
  \<open>'q\<close> to a boolean value. It evaluates to true if there is an edge between the source
  and destination state with the specified symbol.
  e.g. for an LTS L, L q1 a q2 evaluates to true if we can make a transition from state
  q1 to q2 by reading symbol a.
*)

type_synonym ('q,'a) lts = "'q \<Rightarrow> 'a \<Rightarrow> 'q \<Rightarrow> bool"

(*
  2a) Specify: We can make words by following a path on the LTS. Unlike a Finite-State
    Automaton, an accepting path on an LTS can start and end any state. Define the
    function \<open>word\<close> in such a way that \<open>word L p xs q\<close> evaluates to True if and only if
    there is a path starting in p that follows the symbols in xs (we call this the word)
    and ends in q. For a word \<open>x # xs\<close> this means that there exists a 
    destination state r such that there is an edge between p and r such that we can 
    complete the word xs from there. (5 pts.)
*)
fun word :: "('q,'a) lts \<Rightarrow> 'q \<Rightarrow> 'a list \<Rightarrow> 'q \<Rightarrow> bool" where
  "word L p [] q \<longleftrightarrow> p = q"
| "word L p (a#as) r \<longleftrightarrow> (\<exists>q. word L q as r \<and> L p a q)" 


(*
  2b) Specify and Show: If we have a word \<open>as\<close> starting in p and ending in q as well as
    a word \<open>bs\<close> starting in q and ending in r, then we also have a word where \<open>bs\<close> is 
    appended to \<open>as\<close> that starts in p and ends in r. (5 pts.)
*)
lemma word_append: "word L p as q \<Longrightarrow> word L q bs r \<Longrightarrow> word L p (as@bs) r"
  apply (induction as arbitrary: p)
   apply auto
  done


(*
  2c) Show: We have given a function duplicate which takes a list xs as well as a natural
    number n and it returns a new list where xs is repeated n times. If there is a word \<open>as\<close> 
    that starts and ends in the same state, then there also is a word \<open>duplicate as n\<close> that
    starts and ends in that exact same state. Prove the following lemma.
    
    NOTE: no sledgehammer. We won't accept proofs generated by sledgehammer. 
      do not use metis, meson, smt, or using thms by auto/force/... idioms, that are usually 
      generated by sledgehammer! Auxiliary lemmas 
      
*)
(* Corrections: -2 points if sledgehammmer has obviously been used *)

fun duplicate :: "'a list \<Rightarrow> nat \<Rightarrow> 'a list" where
  "duplicate xs 0 = []"
| "duplicate xs (Suc n) = xs @ duplicate xs n"


(*<*)
lemma duplicate_loop: "word L p as p \<Longrightarrow> word L p (duplicate as n) p"
  apply(induction n)
   apply auto
  apply(auto simp: word_append)
  done
(*>*)


lemma pump: "word L p as q \<Longrightarrow> word L q bs q \<Longrightarrow> word L q cs q \<Longrightarrow> word L p (as @ (duplicate bs n) @ cs) q"
(*<*)
  apply(rule word_append)
   apply auto
  apply(rule word_append)
   apply auto
  apply(rule duplicate_loop)
   apply auto
  done
(*>*)


(* 2d) Show: Prove the following lemmas (you might have proven them as auxiliary lemmas already) 
    and then use them to construct forward proofs
*)

lemma dup_loop: "word L p as p \<Longrightarrow> word L p (duplicate as n) p" (*<*)by (simp add: duplicate_loop)(*>*)
lemma word_empty: "word L p [] p" (*<*)by simp(*>*)
lemma word_cons: "L p a q \<Longrightarrow> word L q as r \<Longrightarrow> word L p (a#as) r" (*<*)by auto(*>*)

(*
  Using any of dup_loop, word_empty, word_cons, and word_append,construct the 
  following statements via forward proof [OF ...]. 
*)

(* For example: *)
lemmas stmt_ex = word_append[OF _ word_cons[OF _ word_empty]]
lemma "\<lbrakk>word L p as q; L q a r\<rbrakk> \<Longrightarrow> word L p (as @ [a]) r"
  by (rule stmt_ex)

(* Solve these yourself! *)
lemmas stmt1 = word_append[OF dup_loop dup_loop]
lemma \<open>\<lbrakk>word L p as p; word L p bs p\<rbrakk> \<Longrightarrow> word L p (duplicate as n\<^sub>1 @ duplicate bs n\<^sub>2) p\<close>
  by (rule stmt1)

lemmas stmt2 = word_append[OF _ dup_loop]
lemma \<open>\<lbrakk>word L p as q; word L q bs q\<rbrakk> \<Longrightarrow> word L p (as @ duplicate bs n) q\<close>
  by (rule stmt2)

lemmas stmt3 = word_cons[OF _ word_cons[OF _ word_empty]]
lemma \<open>\<lbrakk>L p a q; L q b r\<rbrakk> \<Longrightarrow> word L p [a, b] r\<close>
  by (rule stmt3)





section \<open>Bonus: Quicksort (5 bonus points)\<close> 
(*
  In the tutorial we have already taken a look at mergesort. We are now going to define quicksort
  and show that it is implemented correctly.
*)


(*
  a) Specify: The quicksort algorithm has previously been introduced in the lecture, now we define
    it in a slightly different way using the \<open>partition\<close> function. We only award points if the 
    partition function is used. (1 pt.)
  HINT: You can look at the definition of the partition function by typing \<open>term partition\<close>. By pressing
    control and clicking it you can see the definition.
*)

fun quicksort :: "nat list \<Rightarrow> nat list" where
(*<*)
  "quicksort [] = []"                    
| "quicksort (x#xs) = (let (xs1, xs2) = partition (\<lambda>x'. x' < x) xs in quicksort xs1 @ [x] @ quicksort xs2)"
(*>*)


(*
  b) Show: We have already seen the multiset in the tutorial. One requirement for a sorting algorithm is that
   a sorted list must have the same multiset as the original list. (1 pt.)
*)

lemma quicksort_preserves_mset: "mset (quicksort xs) = mset xs"
(*<*)  
  apply (induction xs rule: quicksort.induct)
  apply auto
  done
(*>*)


(*
  c) Show: Another requirement for sorting algorithms is that it should actually sort the list.
  HINT: Auxiliary lemmas may be needed, also auxiliary lemmas for your auxiliary lemmas may be needed.
    You also may have to unfold constants like function composition (o_def). Also have a look at
    thm sorted_append, before you end up re-proving it as an aux-lemma ;)
*)

(*<*)
lemma quicksort_preserves_set: "set (quicksort xs) = set xs"
  by (rule mset_eq_setD[OF quicksort_preserves_mset])
(*>*)

lemma quicksort_sorts: "sorted (quicksort xs)"
(*<*)
  apply (induction xs rule: quicksort.induct)
  by (auto simp: o_def sorted_append quicksort_preserves_set)
(*>*)
  
end
