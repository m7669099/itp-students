theory Code_Export
imports Refine
begin

fun nres_of_option where
  "nres_of_option None = FAIL"
| "nres_of_option (Some x) = RETURN x"

declare nres_of_option.simps[code del]

code_datatype nres_of_option

definition "bind1 m f \<equiv> case m of None \<Rightarrow> FAIL | Some x \<Rightarrow> f x"

lemma [code]:
  "FAIL = nres_of_option None"
  "RETURN x = nres_of_option (Some x)"
  "BIND (nres_of_option m) f = bind1 m f"
  apply (auto split: Option.bind_split option.split simp: bind1_def)
  done

declare WHILE_unfold[code]  
  
(* Refinement rules for code export *)

named_theorems code_refine

lemma code_refine_basic[code_refine]:
  "refines (=) (RETURN x) (RETURN x)"
  "refines (=) m m' \<Longrightarrow> refines (=) m (do {assert P; m'})"
  "refines (=) m m' \<Longrightarrow> (\<And>x. refines (=) (f x) (f' x)) \<Longrightarrow> refines (=) (do {x\<leftarrow>m; f x}) (do {x\<leftarrow>m'; f' x})"
  
  "refines (=) m\<^sub>1 m\<^sub>1' \<Longrightarrow> refines (=) m\<^sub>2 m\<^sub>2' \<Longrightarrow> refines (=) (if b then m\<^sub>1 else m\<^sub>2) (if b then m\<^sub>1' else m\<^sub>2')"
  "\<And>f f'. (\<And>x. refines (=) (f x) (f' x)) \<Longrightarrow> refines (=) (let x=v in f x) (let x=v in f' x)"
  "\<And>f f'. (\<And>x y. refines (=) (f x y) (f' x y)) \<Longrightarrow> refines (=) (case p of (x,y) \<Rightarrow> f x y) (case p of (x,y) \<Rightarrow> f' x y)"
    
  unfolding Let_def
  apply (pw; (auto split: prod.splits)?; blast)+
  done
  
lemma code_refine_WHILE[code_refine]: 
  "(\<And>s. refines (=) (b s) (b' s)) \<Longrightarrow> (\<And>s. refines (=) (f s) (f' s)) \<Longrightarrow> refines (=) (WHILE b f s) (WHILE b' f' s)"
  apply wp_step+
  apply (simp_all add: rel_def)
  done
  
lemma code_refine_WHILEI[code_refine]: 
  "(\<And>s. refines (=) (b s) (b' s)) \<Longrightarrow> (\<And>s. refines (=) (f s) (f' s)) \<Longrightarrow> refines (=) (WHILE b f s) (WHILEI V I b' f' s)"
  unfolding WHILEI_def
  apply wp_step+
  apply (simp_all add: rel_def)
  .
  




end
