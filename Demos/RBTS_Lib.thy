theory RBTS_Lib
imports Main "HOL-Library.RBT"
begin

(* From Abs_Data_Type_Demo: *)
(* We can get a set implementation from a map implementation, by using unit values *)
type_synonym 'a rbts = "('a,unit) rbt"
definition "rbts_member x t \<equiv> RBT.lookup t x = Some ()"
definition "rbts_set t \<equiv> { x. rbts_member x t }"
definition "rbts_empty \<equiv> RBT.empty"
definition "rbts_insert x t \<equiv> RBT.insert x () t"
definition "rbts_delete x t \<equiv> RBT.delete x t"

definition "rbts_is_empty t \<equiv> RBT.is_empty t"

abbreviation "rel_rbts t s \<equiv> s = rbts_set t"


lemma rbts_empty_correct: "rbts_set rbts_empty = {}"
  by (auto simp: rbts_member_def rbts_set_def rbts_empty_def)
lemma rbts_member_correct: 
  "rbts_member x t \<longleftrightarrow> x\<in>rbts_set t" by (auto simp: rbts_member_def rbts_set_def)
lemma rbts_insert_correct:
  "rbts_set (rbts_insert x t) =  insert x (rbts_set t)" by (auto simp: rbts_member_def rbts_insert_def rbts_set_def)
lemma rbts_delete_correct:
  "rbts_set (rbts_delete x t) = rbts_set t - {x}"  by (auto simp: rbts_member_def rbts_delete_def rbts_set_def)

lemma rbts_is_empty_correct: "rbts_is_empty t \<longleftrightarrow> rbts_set t = {}" for t :: "'a::linorder rbts"
proof -
  have [simp]: "(\<forall>x. m x \<noteq> Some ()) \<longleftrightarrow> m=Map.empty" for m :: "'a \<rightharpoonup> unit"
    using option.discI by fastforce
  show ?thesis
    by (auto simp: rbts_member_def rbts_is_empty_def rbts_set_def)
qed  
  
(* The correctness lemmas are obvious candidates for simp-lemmas: *)  
lemmas rbts_set_correct[simp] = 
  rbts_empty_correct rbts_member_correct rbts_insert_correct rbts_delete_correct rbts_is_empty_correct


(* Add operation *)  
  
(* Note: This is a generic operation. 
  It's not specific to RBTs, but the pattern works for all 
  set implementations that support insert and empty  
*)
definition "rbts_from_list xs \<equiv> fold rbts_insert xs rbts_empty"

lemma [simp]: "rbts_set (rbts_from_list xs) = set xs"  
proof -
  have "rbts_set (fold rbts_insert xs s) = rbts_set s \<union> set xs" for s
    apply (induction xs arbitrary: s)
    by auto
  thus ?thesis unfolding rbts_from_list_def by auto
qed  
  
(* Generic operation to subtract elements from list *)  

definition "rbts_diff_list s xs \<equiv> fold rbts_delete xs s"

lemma [simp]: "rbts_set (rbts_diff_list s xs) = rbts_set s - set xs"
  unfolding rbts_diff_list_def
  apply (induction xs arbitrary: s)
  by auto

end
