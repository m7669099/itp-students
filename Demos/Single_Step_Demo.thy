theory Single_Step_Demo
imports Main
begin

text\<open>"thm" is a command that displays one or more named theorems:\<close>
thm conjI impI iffI notI

text\<open>Instantiation of theorems: "of"\<close>

text\<open>Positional:\<close>
thm conjI[of "A" "B"]
thm conjI[of "A"]
thm conjI[of _ "B"]

text\<open>By name:\<close>
thm conjI[where ?Q = "B"]
thm conjI[where Q = "B"]   (* May omit ? *)
  
(* If variable name ends with number, ? and ".0" must be written! (Idiosyncrasy of Isabelle) *)
lemma my_rev_append: "rev (a1@a2) = rev a2 @ rev a1" by (rule rev_append)
thm my_rev_append    
thm my_rev_append[where ?a1.0 = "[]"]
    
  

text\<open>Composition of theorems: OF\<close>

thm refl[of "a"]
thm conjI[OF refl[of "a"] refl[of "b"]]
thm conjI[OF refl[of "a"]]
thm conjI[OF _ refl[of "b"]]

notepad
begin

  assume thm1: "A \<Longrightarrow> B \<Longrightarrow> C"
  assume thm2: "X\<and>Y \<Longrightarrow> Z \<Longrightarrow> B"

  thm thm1[OF _ thm2]
  

end




text\<open>A simple backward proof:\<close>
lemma "\<lbrakk> A; B \<rbrakk> \<Longrightarrow> A \<and> B"
apply(rule conjI)
apply assumption
apply assumption
done

end
