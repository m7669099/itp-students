theory Option_Monad_Demo
imports Main "HOL-Library.Monad_Syntax"
begin

section \<open>Bind and Return\<close>

(* return is just Some *)

thm Option.bind.simps

(* Note the \<bind> syntax for bind *)
(* Always use bind with type-annotations, otherwise it cannot be disambiguated *)
term "bind (m::_ option) f" 

lemma 
  "do { x\<leftarrow>None; f x } = None"
  "do { x\<leftarrow>Some a; f x } = f a"
  by auto

find_theorems Option.bind  

subsection \<open>Monad Laws\<close>


lemma "do { x \<leftarrow> Some a; f x } = f a" by simp
lemma "do { x \<leftarrow> m; Some x } = m" by simp
lemma "do { y \<leftarrow> do { x \<leftarrow> m; f x }; g y } = do { x\<leftarrow>m; y \<leftarrow> f x; g y }"
  for m :: "_ option"
  by simp

thm Option.bind.bind_lunit
thm Option.bind_runit
thm Option.bind_assoc

(** Errors are propagated *)
lemma "do { x \<leftarrow> None; f x } = None" by simp
lemma "do { x \<leftarrow> m; None} = None" by simp

thm Option.bind.bind_lzero
thm Option.bind_rzero


section \<open>Assertions\<close> 
  
(* Assert. Return () if condition is true, None otherwise. *)
  
definition "assert P \<equiv> if P then Some () else None"

lemma assert_simps[simp]:
  "assert False = None"
  "assert True = Some ()"
  by (auto simp: assert_def)

  
section \<open>Example: expressions\<close>  
  
  
type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> val"
  
datatype aexp = N int | V vname | Plus aexp aexp | Div aexp aexp 

fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val option" where
  "aval (N i) s = Some i"
| "aval (V x) s = Some (s x)"  
| "aval (Plus a b) s = do {
    ra \<leftarrow> aval a s;
    rb \<leftarrow> aval b s;
    Some (ra+rb)
}"
| "aval (Div a b) s = do {
    ra \<leftarrow> aval a s;
    rb \<leftarrow> aval b s;
    assert (rb \<noteq> 0);
    Some (ra div rb)
}"


fun cprop :: "aexp \<Rightarrow> aexp" where
  "cprop (N v) = N v"
| "cprop (V x) = V x"
| "cprop (Plus a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> N (va + vb)
    | (a,b) \<Rightarrow> Plus a b  
    )"
| "cprop (Div a b) = (
    case (cprop a, cprop b) of 
      (N va, N vb) \<Rightarrow> (if vb = 0 then Div (N va) (N vb) else N (va div vb))
    | (a,b) \<Rightarrow> Div a b  
    )"

lemma "aval (cprop e) s = aval e s"
  apply (induction e)
  apply (auto split!: aexp.splits Option.bind_split)
  done
    
end
