section "Arithmetic and Boolean Expressions"

theory tut02 imports Main begin


section \<open>Folding a list\<close>

fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "itrev [] ys = ys"
| "itrev (x # xs) ys = itrev xs (x # ys)"

text \<open>We already proved the following lemma connecting @{term itrev} and @{term rev}:\<close>
lemma itrev_rev:
  "itrev xs ys = rev xs @ ys"
  apply (induction xs arbitrary: ys) 
   apply auto
  done

text \<open>A list can be folded into a single value by applying an operation
      to each element and the accumulator and store it into that accumulator.
      This procedure is then repeated until all elements of the list are processed.\<close>
fun fold_left :: "('a \<Rightarrow> 'b \<Rightarrow> 'b) \<Rightarrow> 'a list \<Rightarrow> 'b \<Rightarrow> 'b" where
  "fold_left _ _ = undefined"

text \<open>Find and prove an appropriate theorem that connects \<open>itrev\<close> and \<open>fold_left\<close>,
      and then use it to prove \<open>fold_left_rev\<close>.\<close>

lemma itrev_fold_left':
  "fold_left (#) xs ys = itrev xs ys"
  sorry

lemma itrev_fold_left:
  "fold_left (#) xs [] = itrev xs []"
  sorry

lemma fold_left_rev:
  "fold_left (#) xs [] = rev xs"
  sorry

section \<open>Deduplicate\<close>

text \<open>Define a function \<open>deduplicate\<close> that removes duplicate occurrences of subsequent 
      elements.\<close>
fun deduplicate :: "'a list \<Rightarrow> 'a list" where
  "deduplicate _ = undefined"


text \<open>Prove that a deduplicated list has at most the length of the original list:\<close>
lemma
  "length (deduplicate xs) \<le> length xs"
  sorry


section \<open>Binary tree\<close>

datatype 'a tree = Node "'a tree" 'a "'a tree" | Leaf

text \<open>In this exercise, we want to write a function that updates a sub-tree inside a 
      larger tree. For that, we first have to define what a ``location'' inside a tree 
      means. In Isabelle, we can use the @{command type_synonym} to define shorthands 
      for types.\<close>

type_synonym loc = "bool list"

text \<open>A location is a list of @{typ bool}s that are either @{const True} (go left) 
      or @{const False} (go right). Define a \<open>lookup\<close> function that takes a tree and 
      a location and returns the sub-tree at that position. If the location is too long, 
      just return @{const Leaf}. Here are some examples:\<close>

fun lookup :: "'a tree \<Rightarrow> loc \<Rightarrow> 'a tree" where
  "lookup _ _ = undefined"


text \<open>Now, define a function \<open>contained\<close> that returns @{const True} or @{const False} 
      depending on whether the location exists in the tree.\<close>

fun contained :: "'a tree \<Rightarrow> loc \<Rightarrow> bool" where
  "contained _ _ = undefined"


text \<open>Finally, a function \<open>update\<close> that replaces the sub-tree at a given location by a new
      sub-tree. If the location does not exist, return the original tree unchanged.\<close>

fun update :: "'a tree \<Rightarrow> loc \<Rightarrow> 'a tree \<Rightarrow> 'a tree" where
  "update _ _ = undefined"

text \<open>Prove the following lemmas. Hints:
      \<^item> Use computation induction.
      \<^item> You might need a lemma about @{const lookup}.\<close>

lemma "\<not> contained t loc \<Longrightarrow> update t loc t' = t"
  sorry

lemma "contained t loc \<Longrightarrow> lookup (update t loc t') loc = t'"
  sorry

end
