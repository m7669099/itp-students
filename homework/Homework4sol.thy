chapter \<open>Homework 4\<close>
theory Homework4sol
imports Main "~~/src/HOL/Transcendental"
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #3
    RELEASED: Mon, Dec 06 2021
    DUE:      Tue, Dec 14, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)

section \<open>1: Directed Graph (25 Points)\<close>

(*
  Below you can find a definition of a directed graph which is a function in which a source node 
    and a destination node both of type 'v are mapped to a bool. The function evaluates to True 
    if there is an edge which starts in the source state and ends in the destination state. It 
    evaluates to False.
*)

type_synonym 'v dgraph = "'v \<Rightarrow> 'v \<Rightarrow> bool"

(*
  1a) Specify: In addition to the definition in the form of a function, it is also possible to use 
    an inductive definition. Now we define a path twice, once using a \<open>function\<close> and once using
    \<open>inductive\<close>. A path of directed graph E starts in some state s and ends in some state t.
    A list \<open>x # xs\<close> is a path if there is an edge between s and x (\<open>E s x\<close> is True) and if there
    is a path xs between x and t. The empty set is a path if s and t are equal. E.g. path 
    \<open>E s [t, u, v] v\<close> evaluates to True if \<open>E s t\<close> \<open>E t u\<close> and \<open>E u v\<close> all evaluate to True.(5 pts.)
*)
fun path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u (x # xs) v \<longleftrightarrow> (E u x \<and> path E x xs v)"
(*>*)

inductive is_path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool"
(*<*)
for E where
  NilI: "is_path E u [] u"
| ConsI: "\<lbrakk> E u v; is_path E v l w \<rbrakk> \<Longrightarrow> is_path E u (v#l) w"
(*>*)

(*
  1b) Show: both definitions are equal. (5 pts.)
*)
lemma is_path_eq_path: "is_path E u xs v \<longleftrightarrow> path E u xs v"
(*<*)
  apply rule
  subgoal apply (induction rule: is_path.induct) by auto
  subgoal apply (induction xs arbitrary: u) by (auto intro: is_path.intros)
  done
(*>*)


(*
  1c) Show: If two lists together form a path, then each separate list forms a path on its own.
    Also, if there is a path with a loop, we retain a valid path if the loop is removed.
    A path has a loop if it visits some state multiple times. (5 pts.)
*)

(*<*)
lemma path_append: "path E u (xs@ys) v \<longleftrightarrow> (\<exists>w. path E u xs w \<and> path E w ys v)"  
  by (induction xs arbitrary: u) auto

lemma ConsI_eq_path: "is_path E u (x # xs) v \<longleftrightarrow> (E u x \<and> is_path E x xs v)"
  by(auto simp: is_path_eq_path)
(*>*)

lemma is_path_append: "is_path E u (p1 @ p2) w \<longleftrightarrow> (\<exists>v. is_path E u p1 v \<and> is_path E v p2 w)"
(*<*)
  by(auto simp: path_append is_path_eq_path)
(*>*)


lemma is_path_elim_loop: "is_path E u (p1@w#p2@w#p3) v \<Longrightarrow> is_path E u (p1@w#p3) v"
(*<*)
  by (auto simp: is_path_append ConsI_eq_path)
(*>*)


(*
  1d) Show: If there is a path between two states, then there exists also a distinct path. (10 pts.)
  HINT: Part of the proof has already been done. You need a new induction rule called length_induct
    Try finding out what is does. You will also need a built-in theorem called \<open>not_distinct_decomp\<close>
    which states that each list that is not distinct can be decomposed into three lists and an
    element which occurs twice. A distinct list is a list in which each element occurs only once.
    You can possibly use lemmas that you have already proven in this exercise.
*)

lemma path_distinct:
  assumes "is_path E u p v"
  shows "\<exists>p'. distinct p' \<and> is_path E u p' v"
  using assms
proof (induction p rule: length_induct)
  case step: (1 p)
  note IH = step.IH[rule_format] (* Note: rule_format converts a theorem 
    from \<open>\<forall>_. _ \<longrightarrow> _\<close> to \<open>\<And>_. _ \<Longrightarrow> _\<close> style, which makes it easier to apply using    
    single-step methods like rule. Useful in cases like this one, where the induction rule 
    is phrased in HOL (\<longrightarrow>) style.
  *)
  note prems = step.prems
  show ?case proof (cases "distinct p")
(*<*)
    case True with prems show ?thesis by blast
  next
    case False 
    from not_distinct_decomp[OF False] obtain p1 x p2 p3 where
      [simp]: "p=p1@x#p2@x#p3" by auto
    from prems is_path_elim_loop have "is_path E u (p1@x#p3) v" by auto

    with IH[where ys="p1@x#p3"] show ?thesis
      by auto
  qed
qed
(*>*)


section \<open>2: Register Machine (15 points)\<close>

(*
  We give a set of operations similar to the arithmetic expressions we have seen before.
    Additionally, there is a \<open>conditional\<close> argument which evaluates the second argument if the
    first one is non-zero and it evaluates the third argument if the first one is zero. We have
    also given the evaluation function \<open>eval\<close>.
*)

type_synonym vname = string
type_synonym val = int
  
datatype exp = 
    Lit val 
  | Var vname 
  | Binop "val \<Rightarrow> val \<Rightarrow> val" exp exp \<comment> \<open>Any binary operator\<close>
  | Cond exp exp exp  \<comment> \<open>Ternary expression: Like \<open>c?a:b\<close> in Java\<close>


type_synonym state = "vname \<Rightarrow> val"
fun eval :: "exp \<Rightarrow> state \<Rightarrow> val" where
  "eval (Lit i) _ = i"
| "eval (Var x) s = s x"
| "eval (Binop f a b) s = f (eval a s) (eval b s)"  
| "eval (Cond c a b) s = (if eval c s \<noteq> 0 then eval a s else eval b s)"

(*
  Now we consider a register machine, with the following instructions
*)
type_synonym regname = nat
type_synonym regstate = "regname \<Rightarrow> val" 

datatype instr = 
  Loadi regname val   \<comment> \<open>Load immediate\<close>
| Loadv regname vname \<comment> \<open>Load variable\<close>
| Opr "val \<Rightarrow> val \<Rightarrow> val" regname regname \<comment> \<open>Binary op, result goes to first register\<close>
| CMov regname regname regname \<comment> \<open>Conditional move, r2:=r3 if r1\<noteq>0\<close>
  

(*
  The following is the semantics of a single instruction
*)
fun execi :: "state \<Rightarrow> instr \<Rightarrow> regstate \<Rightarrow> regstate" where
  "execi s (Loadi r i) \<rho> = \<rho>(r:=i)"
| "execi s (Loadv r x) \<rho> = \<rho>(r:=s x)"  
| "execi s (Opr f r1 r2) \<rho> = \<rho>(r1:=f (\<rho> r1) (\<rho> r2))"
| "execi s (CMov r1 r2 r3) \<rho> = (if \<rho> r1 \<noteq> 0 then \<rho>(r2:=\<rho> r3) else \<rho>)"
    
(* And of a list of instructions (basic block) *)
type_synonym basic_block = "instr list"

fun execb :: "state \<Rightarrow> basic_block \<Rightarrow> regstate \<Rightarrow> regstate" where
  "execb s [] sr = sr" 
| "execb s (i#is) sr = execb s is (execi s i sr)"
    
(* 
  2a) Show: Fold can be used to execute programs and a serial execution of to instruction lists 
    yields the same results as executing the second list on the result of executing the first one.
*)  
lemma "execb s xs sr = fold (execi s) xs sr"
(*<*)
  apply (induction xs arbitrary: sr) by auto
(*>*)

lemma execb_append[simp]: "execb s (xs1@xs2) sr = execb s xs2 (execb s xs1 sr)"
(*<*)
  apply (induction xs1 arbitrary: sr)
  apply auto
  done
(*>*)
 
(* 
  2b) Specify: We now want to compile an expression to a list of instructions. The \<open>regname\<close> 
    argument represents the register to which the output is eventually written. (5 pts.)
*)
fun compile_exp :: "exp \<Rightarrow> regname \<Rightarrow> basic_block" where
  "compile_exp (Lit i) r = [Loadi r i]"
| "compile_exp (Var x) r = [Loadv r x]"
| "compile_exp (Binop f a b) r = compile_exp a r @ compile_exp b (r+1) @ [Opr f r (r+1)]"  
| "compile_exp (Cond c a b) r = compile_exp b r @ compile_exp a (r+1) @ compile_exp c (r+2) @ [CMov (r+2) r (r+1)]"  

(*
  2c) Show: the compilation of these expressions is correct. I.e. executing the compiled list of 
    instructions is the same as evaluating the original expression.
*)    
lemma "let \<rho>' = execb s (compile_exp e r) \<rho> in (\<forall>r'<r. \<rho>' r' = \<rho> r') \<and> \<rho>' r = eval e s"
(*<*)
  apply (induction e arbitrary: r \<rho>)
  apply auto []
  apply auto []
  apply (auto split: prod.splits simp: Let_def) 
  done
(*>*)

section \<open>Bonus: Irrationality of log (5 points)\<close>

(*
  Show: The logarithm of 2 with base 3 is irrational.
  HINT: do the proof on paper first!
    Also take a look at the tutorial and the example proof at the following link:
    \<open>https://en.wikipedia.org/wiki/Isabelle_(proof_assistant)\<close>
*)

(*<*)
lemma even_neq_odd: "odd a \<Longrightarrow> even b \<Longrightarrow> a \<noteq> b"
  by blast


lemma two_pow_neq_three_pow: "b > 0 \<Longrightarrow> (3::nat) ^ a \<noteq> (2::nat) ^ b"
  apply(rule even_neq_odd)
  apply(auto)
  done


lemma two_powr_neq_three_powr: "b > 0 \<Longrightarrow> (3::nat) powr (a::nat) \<noteq> (2::nat) powr (b::nat)"
  apply(simp add: powr_realpow)
  using two_pow_neq_three_pow[of b a]
  by (metis real_of_nat_eq_numeral_power_cancel_iff)
(*>*)


lemma "(log 3 2) \<notin> \<rat>"
(*<*)
proof(rule ccontr; simp)
  assume "(log 3 2) \<in> \<rat>"
  then obtain a b :: nat where "\<bar>(log 3 2)\<bar> = a / b" and NON_ZERO: "b > 0"
    apply(rule Rats_abs_nat_div_natE) by auto
  moreover have "\<bar>(log 3 2)\<bar> = (log 3 2)" by simp
  ultimately have "3 powr (log 3 2) = 3 powr (a / b)" by presburger
  hence "2 = 3 powr (a / b)" by fastforce
  also have "... = 3 powr (a * (1 / b))" by simp
  finally have "2 powr b = (3 powr (a * (1 / b))) powr b" by presburger
  also have "... = 3 powr (a * (1 / b) * b)" by (simp add: powr_powr)
  also have "... = 3 powr a" by(auto simp: NON_ZERO)
  finally have "2 powr b = 3 powr a" by fast
  moreover have "2 powr b \<noteq> 3 powr a" using two_powr_neq_three_powr[OF NON_ZERO] by force
  ultimately show False by blast 
qed
(*>*)

end

