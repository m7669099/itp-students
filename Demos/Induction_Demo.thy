theory Induction_Demo
imports Main
begin

subsection \<open> Generalization \<close>
  
fun itrev :: "'a list \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"itrev [] ys = ys" |
"itrev (x#xs) ys = itrev xs (x#ys)"

value "itrev [1,2,3::int] [6,7,8]"

lemma itrev_aux[simp]: "itrev xs ys = rev xs @ ys"
  (*apply (induction xs ys rule: itrev.induct)
  apply auto
  done
  *)


  apply (induction xs arbitrary: ys)
  subgoal by auto
  subgoal premises prems for a xs ys'
    apply auto
    using prems
    apply (subst prems)
    by (rule refl)
  done
    (* or just induction, auto *)  

lemma "itrev xs [] = rev xs" 
  apply auto
  (* apply (auto simp: itrev_aux)*)
  done


subsection\<open> Computation Induction \<close>

fun sep :: "'a \<Rightarrow> 'a list \<Rightarrow> 'a list" where
"sep a [] = []" |
"sep a [x] = [x]" |
"sep a (x#y#zs) = x # a # sep a (y#zs)"

lemma "map f (sep a xs) = sep (f a) (map f xs)"
  apply (induction a xs rule: sep.induct)
  apply auto
  done

subsection \<open> Auxiliary Lemmas \<close>
  
hide_const rev  (* Get predefined rev of standard lib out of way! *)  
fun rev where 
  "rev [] = []"  
| "rev (x#xs) = rev xs @ [x]"  
  
(*lemma aux: "rev (xs@[a]) = a # rev xs"
  apply (induction xs) apply auto done
*)

lemma aux2: "rev (xs' @ xs) = rev xs @ rev xs'" 
  apply (induction xs') apply auto
   done



lemma "rev (rev xs) = xs"  
  apply (induction xs)
   apply (auto simp: )
   subgoal premises prem for a xs
     apply (subst aux2)
     apply (subst prem)
     apply (subst rev.simps)
     apply (subst rev.simps)
     apply (subst append.simps)
     apply (subst append.simps)
     apply (subst append.simps)
     apply (rule refl)
     done
   done
   
   using aux2
   done  
   (* We get stuck here! Auxiliary lemma needed! *)
  
  
end
