section "Arithmetic and Boolean Expressions"

theory tut04_sol imports Main "HOL.Transcendental"
  "HOL-Computational_Algebra.Primes"
begin

section \<open>Context-Free Grammar\<close>

text \<open>We define a grammar for palindromes consisting of the two symbols \<open>a\<close> and \<open>b\<close>:
  S \<longrightarrow> aSa
  S \<longrightarrow> bSb
  S \<longrightarrow> \<epsilon>

We can define this in Isabelle/HOL using an inductive definition.
\<close>

datatype ab = a | b

inductive S :: "ab list \<Rightarrow> bool" where
(*<*)
  a: "S w \<Longrightarrow> S (a # w @ [a])"
| b: "S w \<Longrightarrow> S (b # w @ [b])"
| nil: "S []"
(*>*)

text \<open>
  Prove that if \<open>xux\<close> (where \<open>x\<close> is a symbol and \<open>u\<close> is a word) is a word in the language,
  then \<open>u\<close> is also a word in the language:
\<close>

lemma palindromes_S:
  assumes "S (x # u @ [x])"
  shows "S u"
proof -
  (*<*)
  from assms show ?thesis
  proof cases
    case (a w)
    then show ?thesis by simp
  next
    case (b w)
    then show ?thesis by simp
  qed
(*>*)
qed


text \<open>Also prove that there are no words \<open>aub\<close> or \<open>bua\<close> in the language:\<close>

lemma non_palindromes_not_S: "x \<noteq> y \<Longrightarrow> \<not> S (x # u @ [y])"
(*<*)
proof
  assume "S (x # u @ [y])" and NEQ: "x \<noteq> y"
  then show False
  proof cases
    case (a w)
    then show ?thesis using NEQ by blast
  next
    case (b w)
    then show ?thesis using NEQ by blast
  qed
qed
(*>*)


text \<open>Show that an extension of a word is only valid iff the extension on the end is the same
      symbol as the one at the beginning.\<close>

lemma "S u \<Longrightarrow> S (x # u @ [y]) \<longleftrightarrow> x = y" (*try sledgehammer here*)
(*<*)
  apply auto
   apply(rule ccontr)
   apply(auto simp: non_palindromes_not_S)
  apply(cases y)
   apply(auto simp: S.intros)
  done
(*>*)


section \<open>Paths vs. Transitive Closure\<close>


text \<open>We have defined a directed graph as a function which maps a source state and a destination
      state to a bool which evaluates to True if there is an edge starting in the source and 
      ending in the destination.\<close>

type_synonym 'v dgraph = "'v \<Rightarrow> 'v \<Rightarrow> bool"


text \<open>A path is a list of states that are connected through edges.\<close>

fun path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u (x # xs) v \<longleftrightarrow> (E u x \<and> path E x xs v)"
(*>*)


text \<open>The transitive closure contains an edge iff there is a path between the nodes of this edge\<close>

(*<*)
lemma path_to_rtranclp: "path E u xs v \<Longrightarrow> E\<^sup>*\<^sup>* u v"
  apply(induction xs arbitrary: u)
   apply(auto simp: converse_rtranclp_into_rtranclp)
  done


lemma rtranclp_to_path: "E\<^sup>*\<^sup>* u v \<Longrightarrow> \<exists>xs. path E u xs v"
proof(induction rule: converse_rtranclp_induct)
  case base
  have "path E v [] v" by simp
  then show ?case by blast
next
  case (step y z)
  then obtain xs where "path E z xs v" by blast
  hence "path E y (z # xs) v" using step.hyps(1) by auto
  then show ?case by blast
qed
(*>*)

lemma "E\<^sup>*\<^sup>* u v \<longleftrightarrow> (\<exists>xs. path E u xs v)"
(*<*)
  apply(auto simp: rtranclp_to_path path_to_rtranclp)
  done
(*>*)


section \<open>Irrationality of sqrt 3\<close>

lemma sqrt_3_irrational: "sqrt 3 \<notin> \<rat>"
(*<*)
proof
  assume "sqrt 3 \<in> \<rat>"
  then obtain m n :: nat where
    sqrt_rat: "\<bar>sqrt 3\<bar> = m / n" and lowest_terms: "coprime m n"
    by (rule Rats_abs_nat_div_natE)
  hence "m^2 = (sqrt 3)^2 * n^2" by (auto simp add: power2_eq_square)
  hence eq: "m^2 = 3 * n^2" using of_nat_eq_iff power2_eq_square by fastforce
  hence "3 dvd m^2" by simp
  hence "3 dvd m" by (simp add: prime_dvd_power)
  moreover have "3 dvd n" proof -
    from \<open>3 dvd m\<close> obtain k where "m = 3 * k" ..
    with eq have "3 * n^2 = 3^2 * k^2" by simp
    hence "3 dvd n^2" by simp
    thus "3 dvd n" by (simp add: prime_dvd_power)
  qed
  ultimately have "\<not> coprime m n" unfolding coprime_def 
    using nat_dvd_1_iff_1 numeral_eq_one_iff by blast
  thus False using lowest_terms by simp
qed
(*>*)

end
