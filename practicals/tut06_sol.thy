section "Arithmetic and Boolean Expressions"

theory "tut06_sol"
imports "../Demos/Graph_Lib" "../Demos/RBTS_Lib" "../Demos/While_Lib" (* link these to the respective demo files on your machine *)
begin


section \<open>Specification\<close>

(* Loop reachable from v\<^sub>0: v\<^sub>0 \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v *)
definition "has_loop g v\<^sub>0 \<equiv> \<exists>v. (v\<^sub>0, v) \<in> g\<^sup>* \<and> (v, v) \<in> g\<^sup>+"

(*
  If there is a loop reachable from v, there is also a successor of v from which we can reach the loop.
  
  Intuition (\<Longrightarrow>):
    Either loop not yet reached: v\<^sub>0 \<rightarrow> v' \<rightarrow>\<^sup>* v \<rightarrow>\<^sup>+ v. Obviously, loop also reachable from successor v'.
    Or v\<^sub>0 is already part of loop: v\<^sub>0\<rightarrow>\<^sup>+v\<^sub>0. Just 'rotate' loop by one, to start and end at successor of v\<^sub>0.

  (\<Longleftarrow>): obvious
*)
lemma has_loop_iff_succ: "has_loop g u \<longleftrightarrow> (\<exists>v. (u,v)\<in>g \<and> has_loop g v)"
  apply (auto simp: has_loop_def)
  subgoal by (meson rtrancl_trancl_trancl tranclD)
  subgoal by (meson converse_rtrancl_into_rtrancl)
  done

(* Dest-version of this lemma, as it is not a good simp lemma! *)  
lemmas has_loop_succD = has_loop_iff_succ[THEN iffD1]



section \<open>Algorithm Definition\<close>

definition "dfs_find_cycle_aux gi v\<^sub>0 \<equiv> 
  while_option (\<lambda>(d,w,c). w\<noteq>[] \<and> \<not>c) (\<lambda>(d,w,c). 
    let stack = snd`set w in
    let ((p,v),w) = (hd w, tl w) in
    case p of 
      [] \<Rightarrow> (insert v d, w, False)
    | u # p \<Rightarrow> (
        if u \<in> d then (d, (p,v) # w, False) 
        else if u\<in>stack then (d, w, True)
        else (d, (rg_succs gi u, u) # (p,v) # w, False))
  ) ({},[(rg_succs gi v\<^sub>0, v\<^sub>0)],False)
"

definition "dfs_find_cycle gi v\<^sub>0 \<equiv> let (_,_,c) = the (dfs_find_cycle_aux gi v\<^sub>0) in c"


section \<open>Invariant\<close>

text \<open>
  We present the invariant here.
  Note that, in practice, one will typically start with a too weak invariant,
  and only discover what is needed while trying to prove its preservation.

  So the proof will cycle between correctness proof and adjusting the invariant multiple times.
  
  When developing this tutorial, we started with an invariant, 
  that we knew would only prove soundness and termination. We had to modify this about 3 times.
  
  Only then, we added the invariant for completeness. This also was changed 3 times before it worked.
  
  Note that our final invariants are not perfect! We could prove them, and they prove the algorithm correct.
  But there are certainly more concise formulations!
  
  Moreover, during the proof we discovered a bug in our initial algorithm, that, of course, 
  needed fixing before we could complete the proof.
\<close>

context
  fixes gi and g :: "'v::linorder graph" and v\<^sub>0 :: 'v
  defines "g \<equiv> rg_\<alpha> gi"
begin

(*
  Building the invariant:
    We split the invariant into different, mostly independent parts.
    This is to keep the subgoals readable. 
*)

(*
  The first part we do separately is the fact that the nodes on the stack form a path.
  This is obvious, as we only ever push a successor of the current top node.

  We phrase a slightly weaker property here: 
    From each node on the stack, we can reach any node above that node.
    
*)
definition "stack_reachable w = (\<forall>ws p v ws'. w = ws @ (p,v) # ws' \<longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v})"

(*
  Next, we summarize all the invariants that hold when no cycle is found yet.
  
  We define the invariants required for soundness, and for completeness separately:
*)
(*
  Soundness: 
  These are required to maintain the property that the stack forms a path, 
    and is reachable. 
  Thus, if we find a back-edge, we know we close a reachable cycle.
  
  Also, knowing that we only work on reachable nodes (including the done ones), 
  is required for the termination argument.
*)
definition "loop_invar_sound d w \<equiv> 
      d \<union> (snd ` set w) \<subseteq> g\<^sup>*``{v\<^sub>0} \<comment> \<open>We only work on reachable nodes\<close>
    \<and> (\<forall>(p,v) \<in> set w. set p \<subseteq> g``{v}) \<comment> \<open>The pending set contains only successors of the node on the stack\<close>
    \<and> stack_reachable w \<comment> \<open>The nodes on the stack form a path\<close>
    "
   

(*
  Completeness:
  Required to maintain the property that no cycles are reachable from done nodes, and v\<^sub>0 is such a node at the end.

  The main idea to maintain this invariant is, that all successors of a node on the stack are one of
    1) done (no loop)
    2) still pending
    3) are the next node on the stack
  
  accordingly, a loop from the top node of the stack must go through a pending node,
  and a loop from any node in the middle of the stack must go through a pending node, 
  or through the next node on the stack.
  
*)        
definition "loop_invar_complete d w \<equiv>
  v\<^sub>0 \<in> d \<union> (snd ` set w) \<comment> \<open>We do not forget about \<open>v\<^sub>0\<close>\<close>
\<and> (\<forall>v\<in>d. \<not>has_loop g v) \<comment> \<open>There are no loops reachable from done nodes\<close>
\<and> (\<forall>p v w\<^sub>2. w=(p,v)#w\<^sub>2 \<and> has_loop g v \<longrightarrow> (\<exists>v'\<in>set p. has_loop g v')) \<comment> \<open>Loops over top of stack\<close>
\<and> (\<forall>w\<^sub>1 p\<^sub>1 v\<^sub>1 p\<^sub>2 v\<^sub>2 w\<^sub>2. w=w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2 \<and> has_loop g v\<^sub>2 \<longrightarrow> (\<exists>v'\<in>insert v\<^sub>1 (set p\<^sub>2). has_loop g v')) \<comment> \<open>Loops over middle of stack\<close>
" 

(*
  Finally, we assemble the main invariant:
    if we indicate that we have found a cycle, there is one.
    otherwise, the soundness and completeness invariants still hold.
   
  This type of invariant, i.e., not requiring anything but the cycle on breaking the loop,
  is typical for invariants that involve breaking the loop with a flag.  
*)
definition "loop_invar \<equiv> \<lambda>(d, w, c). (if c then has_loop g v\<^sub>0 else loop_invar_sound d w \<and> loop_invar_complete d w)"


subsection \<open>Invariant auxiliary lemmas\<close>

(*
  In order to simplify later reasoning, we show a few
  auxiliary lemmas, that can be used to manually instantiate the 
  \<open>ws @ (p,v) # ws'\<close> and \<open>w=w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2\<close> patterns used in the invariants.
  Note that auto and friends will typically struggle with such instantiations, as
  the simplifier will change the associativity, e.g.

*)

notepad begin
  fix xs zs :: "'a list" and x y
  let "?xs@?y#?zs" = "xs@y#zs"  (* Matches *)
  
  (* let "?xs@?y#?zs" = "x#xs@y#zs"  (* Does not match! *) *)
  let "?xs@?y#?zs" = "(x#xs)@y#zs"  (* This matches *)
  
  (* But, simplifier will rewrite  (x#xs)@y#zs \<rightarrow> x#xs@y#zs, 
    before any such pattern will be matched!
  *)
  
end

lemma stack_reachableD: assumes "stack_reachable w" "w = ws @ (p,v) # ws'" shows "snd ` (set ws) \<subseteq> g\<^sup>+``{v}"
  using assms
  unfolding stack_reachable_def
  by blast

lemma stack_reachableI: assumes "\<And>ws p v ws'. w = ws @ (p,v) # ws' \<Longrightarrow> snd ` (set ws) \<subseteq> g\<^sup>+``{v}" shows "stack_reachable w"
  using assms
  unfolding stack_reachable_def
  by blast

lemma stack_reachableI':
  assumes "\<And>w\<^sub>1 w\<^sub>2 w\<^sub>3 p\<^sub>1 p\<^sub>2 u\<^sub>1 u\<^sub>2. w = w\<^sub>1@(p\<^sub>1,u\<^sub>1)#w\<^sub>2@(p\<^sub>2,u\<^sub>2)#w\<^sub>3 \<Longrightarrow> (u\<^sub>2,u\<^sub>1)\<in>g\<^sup>+"
  shows "stack_reachable w"
  using assms unfolding stack_reachable_def
  by (auto simp: in_set_conv_decomp)
    
lemma loop_invar_complete_middleD:
  assumes "loop_invar_complete d (w\<^sub>1@(p\<^sub>1,v\<^sub>1)#(p\<^sub>2,v\<^sub>2)#w\<^sub>2)"
  assumes "has_loop g v\<^sub>2"
  shows "\<exists>v'\<in>insert v\<^sub>1 (set p\<^sub>2). has_loop g v'"
  using assms unfolding loop_invar_complete_def
  by blast



  
section \<open>Invariant Preservation\<close>  

(*
  At this point in the proof, one will start with the main correctness lemma.
  Having defined the invariant as a constant, 
  after applying while-rule and some goal massaging (clarsimp, auto, etc),
  one should see the different branches of the algorithm, and what invariant preservation lemmas are required.
  These are then proved SEPARATELY. This, again, is a method to keep subgoals readable, and
  to allow for more flexible changes: after a change, things will break locally, rather than somewhere in a 
  big and messy apply auto try0+sledgehammer proof.
*)

subsection \<open>Init\<close>
(* Invariant holds initially. Proof is simple enough for one-liner *)
lemma invar_init: "loop_invar ({},[(rg_succs gi v\<^sub>0, v\<^sub>0)],False)"
  unfolding loop_invar_def loop_invar_sound_def stack_reachable_def loop_invar_complete_def
  by (auto simp: Cons_eq_append_conv dest: has_loop_succD simp flip: g_def)

  
subsection \<open>Pending list empty, pop node\<close>
  
(*
  The remaining lemmas have been, subsequently, split by the defined parts of the invariant.
*)  
lemma stack_reachable_tail: assumes "local.stack_reachable ((px, vx) # w)" shows "local.stack_reachable w"
  unfolding stack_reachable_def
proof clarsimp
  fix ws p v ws' p' v'
  assume "(p', v') \<in> set ws" and [simp]: "w = ws @ (p, v) # ws'"
  then obtain ws1 ws2 where "ws = ws1 @ (p', v') # ws2" using in_set_conv_decomp by metis
  hence 1: "(px, vx) # w = ((px, vx) # ws1 @ (p', v') # ws2) @ (p, v) # ws'" by simp
  from stack_reachableD[OF assms this] show "(v, v') \<in> g\<^sup>+" by simp
qed


lemma invar_complete_step1: 
  assumes LIC: "loop_invar_complete d (([], vv) # w)" 
  shows "loop_invar_complete (insert vv d) w"
  (*
    Let's try naively
  *)
  using assms
  unfolding loop_invar_complete_def
  apply auto
  apply blast
  apply (metis append_Cons)
  apply blast
  apply (metis append_Cons)
  apply blast
  apply blast
  apply (metis append_Cons)
  apply (metis append_Cons)
  apply (metis append_Cons)
  apply (metis append_Cons)
  
  (** OK, with a lot of try0 and sledgehammer, we have solved it.
    BUT: the subgoals are unreadable, and if something goes wrong,
      you have no idea why. In our initial attempts, something went wrong, so
      we decided to split it a bit more intelligently:
  *)
  
  oops

lemma invar_complete_step1: 
  assumes LIC: "loop_invar_complete d (([], vv) # w)" 
  shows "loop_invar_complete (insert vv d) w"
  (*
    Next attempt
  *)

  (* First, we divide the invariant into its 3 parts *)  
  unfolding loop_invar_complete_def
  apply (intro conjI allI impI ballI; (elim conjE)?)
  (* Then we solve each part separately. Thus, if something goes wrong, we at least know 
    which part of the invariant we could not prove.
    
  ALTERNATIVE: separate definitions for each of the parts. 
    This would be cleaner, but more boilerplate \<longrightarrow> proof engineering trade off  
  *)
  subgoal using assms by (auto simp: loop_invar_complete_def)
  subgoal using assms by (auto simp: loop_invar_complete_def)
  subgoal using assms apply (auto simp: loop_invar_complete_def) (*try0*) by blast+
  subgoal using assms apply (auto simp: loop_invar_complete_def Cons_eq_append_conv) (*try0*) by blast+
  done  


lemma invar_step1: "local.loop_invar (d, ([], v) # w, False) \<Longrightarrow> local.loop_invar (insert v d, w, False)"
  unfolding loop_invar_def
  apply simp
  apply (subst loop_invar_sound_def)
  apply (intro conjI allI impI ballI)
  subgoal by(auto simp: loop_invar_sound_def)
  subgoal by(auto simp: loop_invar_sound_def)
  subgoal by(auto simp: stack_reachable_tail loop_invar_sound_def)
  subgoal by (simp add: invar_complete_step1)
  done

subsection \<open>Next pending node already done\<close>  
  
lemma stack_reachable_change_pending: assumes "stack_reachable ((p', v) # w) " shows "stack_reachable ((p, v) # w)"
proof (rule stack_reachableI)
  fix ws pa va ws'
  assume DECOMP: "(p, v) # w = ws @ (pa, va) # ws'"
  show "snd ` set ws \<subseteq> g\<^sup>+ `` {va}"
  proof(cases ws)
    case Nil
    then show ?thesis by simp
  next
    case (Cons a list)
    then show ?thesis using DECOMP stack_reachableD[OF assms, of "(p', v) # list"] by auto
  qed
qed
  
lemma invar_complete_step2: "\<lbrakk>loop_invar_complete d ((u # p, v) # w); u \<in> d\<rbrakk>
    \<Longrightarrow> loop_invar_complete d ((p, v) # w)"
  unfolding loop_invar_complete_def
  apply (clarsimp simp: Cons_eq_append_conv)
  by blast
  

lemma invar_step2: "local.loop_invar (d, (u # p, v) # w, False) \<Longrightarrow> u \<in> d \<Longrightarrow> local.loop_invar (d, (p, v) # w, False)"
  unfolding loop_invar_def
  apply simp
  apply (subst loop_invar_sound_def)
  apply (intro conjI allI impI ballI)
  subgoal by(auto simp: loop_invar_sound_def)
  subgoal by(auto simp: loop_invar_sound_def)
  subgoal by(auto simp: stack_reachable_change_pending loop_invar_sound_def) 
  subgoal by (auto simp: invar_complete_step2)
  done

subsection \<open>Next pending node on stack. Loop found!\<close>  
  
(* Our invariants require us to show two cases, one for a loop with at least two edges,
  and one for a self-loop over the top of the stack: *)
  
(* At least two edges *)  
lemma invar_step3: 
  assumes "local.loop_invar (d, (u # p, v) # w, False)" 
      "(p', u) \<in>set w" 
      "u \<notin> d"  
  shows "loop_invar (d, w, True)"
  unfolding loop_invar_def
proof (simp)
  from assms have LI: "loop_invar_sound d ((u # p, v) # w)"
    unfolding loop_invar_def by simp

  from \<open>(p', u) \<in> set w\<close> obtain w\<^sub>1 w\<^sub>2 where [simp]: "w = w\<^sub>1@(p',u)#w\<^sub>2" by (auto simp: in_set_conv_decomp)

  from LI have "(v\<^sub>0,u)\<in>g\<^sup>*" unfolding loop_invar_sound_def by auto
  moreover {
    from LI have "stack_reachable ((u # p, v) # w\<^sub>1@(p',u)#w\<^sub>2)" unfolding loop_invar_sound_def by auto
    from stack_reachableD[OF this, where ws="(u # p, v) # w\<^sub>1"] have "(u,v)\<in>g\<^sup>+" by auto
    also from LI have "(v,u)\<in>g" unfolding loop_invar_sound_def by auto
    finally have "(u,u)\<in>g\<^sup>+" .
  }
  ultimately show "has_loop g v\<^sub>0" by (auto simp: has_loop_def)
qed

(* Self loop *)  
lemma invar_step3': 
  assumes "local.loop_invar (d, (v # p, v) # w, False)" 
  shows "loop_invar (d, w, True)"
  unfolding loop_invar_def
proof (simp)
  from assms have LI: "loop_invar_sound d ((v # p, v) # w)"
    unfolding loop_invar_def by simp

  from LI have "(v\<^sub>0,v)\<in>g\<^sup>*" unfolding loop_invar_sound_def by auto
  moreover
  from LI have "(v,v)\<in>g\<^sup>+" unfolding loop_invar_sound_def by auto
  ultimately show "has_loop g v\<^sub>0" by (auto simp: has_loop_def)
qed

subsection \<open>Pushing node on stack\<close>

lemma aux3: assumes "loop_invar_sound d ((u # p, v) # w)" shows "stack_reachable ((p', u) # (p, v) # w)"
proof(intro stack_reachableI')
  fix w\<^sub>1 w\<^sub>2 w\<^sub>3 p\<^sub>1 p\<^sub>2 u\<^sub>1 u\<^sub>2

  from assms have SR: "stack_reachable ((u # p, v) # w)" unfolding loop_invar_sound_def by blast
  have V_U: "(v,u) \<in> g" using assms unfolding loop_invar_sound_def by auto

  (* We manually consider all 4 cases, how these patterns may be aligned *)
  assume "(p', u) # (p, v) # w = w\<^sub>1 @ (p\<^sub>1, u\<^sub>1) # w\<^sub>2 @ (p\<^sub>2, u\<^sub>2) # w\<^sub>3"
  then consider
      "w\<^sub>1 = []" "p' = p\<^sub>1" "u = u\<^sub>1" "w\<^sub>2 = []" "p = p\<^sub>2" "v = u\<^sub>2" "w = w\<^sub>3"
    | w\<^sub>2' where  " w\<^sub>1 = []" "p' = p\<^sub>1" "u = u\<^sub>1" "w = w\<^sub>2' @ (p\<^sub>2, u\<^sub>2) # w\<^sub>3" "w\<^sub>2 = (p, v) # w\<^sub>2'"
    | "w\<^sub>1 = [(p', u)]" "p = p\<^sub>1" "v = u\<^sub>1" "w = w\<^sub>2 @ (p\<^sub>2, u\<^sub>2) # w\<^sub>3"
    | w\<^sub>1' where "w\<^sub>1 = (p', u) # (p, v) # w\<^sub>1'" "w = w\<^sub>1' @ (p\<^sub>1, u\<^sub>1) # w\<^sub>2 @ (p\<^sub>2, u\<^sub>2) # w\<^sub>3"
    by (auto simp: Cons_eq_append_conv)
  thus "(u\<^sub>2,u\<^sub>1)\<in>g\<^sup>+" proof cases
    case [simp]: 1
    show ?thesis using V_U by simp
  next
    case [simp]: 2

    (* And for the cases, we manually instantiate the pattern to apply the assumption SR.
       Here, it is enough to instantiate the prefix ws 
     *)
    from SR[simplified] have "(u\<^sub>2,v)\<in>g\<^sup>+"
      using stack_reachableD[where ws="(u\<^sub>1 # p, v) # w\<^sub>2'"]
      by auto
    also note V_U[simplified]  
    finally show ?thesis .
  next
    case [simp]: 3
    show ?thesis 
      using SR[simplified]
      using stack_reachableD[where ws="(u # p\<^sub>1, u\<^sub>1) # w\<^sub>2"]
      by simp

  next 
    case [simp]: 4
    show ?thesis 
      using SR[simplified]
      using stack_reachableD[where ws="(u # p, v) # w\<^sub>1' @ (p\<^sub>1, u\<^sub>1) # w\<^sub>2"]
      by simp
  qed
qed  

lemma loop_invar_complete_step4: 
  assumes A: "loop_invar_complete d ((u # p, v) # w)"
  shows "loop_invar_complete d ((rg_succs gi u, u) # (p, v) # w)"
  unfolding loop_invar_complete_def
  (* Again, first split into parts, then prove each part separately *)
  apply (clarsimp simp flip: g_def)
  apply safe
  subgoal using A unfolding loop_invar_complete_def by simp
  subgoal for v 
    using A unfolding loop_invar_complete_def by simp
  subgoal
    by (auto dest: has_loop_succD)
  subgoal for w\<^sub>1 p\<^sub>1 v\<^sub>1 p\<^sub>2 v\<^sub>2 w\<^sub>2
    using A
    (* And split again, though a bit more ughly, as we use auto for splitting, 
      rather than a more controlled method like into *)
    apply (auto simp: Cons_eq_append_conv)
    subgoal unfolding loop_invar_complete_def by auto
    subgoal unfolding loop_invar_complete_def (*try0*) by fast
    subgoal for ww
      using A loop_invar_complete_middleD[where w\<^sub>1="(u # p, v) # ww"] (* 
        Instantiate just as much as required to match the pattern. The prefix to the start of pattern is enough!
      *)
      by fastforce
    done  
  done      
        

lemma invar_step4: "local.loop_invar (d, (u # p, v) # w, False) \<Longrightarrow> u \<notin> snd ` set w \<Longrightarrow> u \<notin> d 
  \<Longrightarrow> local.loop_invar (d, (rg_succs gi u, u) # ((p,v)) # w, False)"
  unfolding loop_invar_def
  apply simp
  apply (subst loop_invar_sound_def)
  apply (intro conjI)
  subgoal by(auto simp: loop_invar_sound_def)
  subgoal by (auto simp flip: g_def simp: loop_invar_sound_def)
  subgoal
    using aux3
    by (clarsimp simp: loop_invar_sound_def)
  subgoal by (auto simp: loop_invar_complete_step4)
  done
  
section \<open>Main Proof\<close>
(*
  Note, this proof will be started first, and then
  all the auxiliary lemmas in the previous sections are incrementally added!
*)

subsection \<open>Termination\<close>

(*
          We find a cycle 
  <*lex*> nodes not yet on the stack nor done decrease
  <*lex*> stack decreases
  <*lex*> length of pending nodes on top of stack decreases

  The inv_image maps our state to the things that decrease.
*)
definition "dfs_wfrel \<equiv> (inv_image 
  ({(True,False)} <*lex*> finite_psubset <*lex*> less_than <*lex*> less_than) 
  (\<lambda>(d,w,c). (
    c,                        \<comment> \<open>Cycle found\<close>
    g\<^sup>*``{v\<^sub>0} - (d\<union>snd`set w), \<comment> \<open>nodes not yet on stack or in d\<close>
    length w,                 \<comment> \<open>length of stack\<close>
    length (fst (hd w))       \<comment> \<open>number of pending nodes on top of stack\<close>
  )))"

lemma finite_reachable[simp, intro!]: "finite (g\<^sup>*``{v})"
proof -
  have "g\<^sup>*``{v} \<subseteq> insert v (fst`g \<union> snd`g)"
    apply auto
    by (metis image_iff rtranclE snd_conv)
  also have "finite \<dots>" unfolding g_def using rg_finite_dom rg_finite_ran by simp
  finally (finite_subset) show ?thesis .
qed       


lemma compl_subsetI:
  assumes "b\<subseteq>a" "c\<subseteq>a" "c\<subset>b"
  shows "a-b \<subset> a-c"
  using assms by blast

(* Auxiliary lemma to show that invar-complete implies no loops at the end.
  The remaining \<dots>-final properties are shown in-line.
*)  
lemma loop_invar_complete_final: 
  assumes LIC: "loop_invar_complete d []"
  shows "\<not>has_loop g v\<^sub>0"
proof -
  from LIC have \<open>v\<^sub>0\<in>d\<close> unfolding loop_invar_complete_def by auto
  with LIC show ?thesis unfolding loop_invar_complete_def by auto
qed  
  

lemma dfs_find_cycle_aux_correct: 
  "\<exists>d w c. dfs_find_cycle_aux gi v\<^sub>0 = Some (d,w,c) \<and> (c = has_loop g v\<^sub>0)"
  unfolding dfs_find_cycle_aux_def
  apply (rule while_option_rule'[where P=loop_invar and r=dfs_wfrel])
  subgoal (* Invariant holds initially *)
    apply(auto simp: invar_init) done 
  subgoal (* Invariant is preserved. SHOW THAT only AFTER the invar-implies-final subgoal *)
    (* We start with some splitting, but not unfolding the invariant yet*)
    apply (auto simp: neq_Nil_conv split!: list.split) 
    (* From here, we identify the auxiliary invar_stepXX lemmas. Note that some subgoals are redundant!*)
    (* Good approach: first sorry your invar_stepXX lemmas, and check if you can prove this goal.
      Then go and prove the invar_stepXX lemmas.
    
      IMPORTANT: Try to intuitively understand in which case/step of the algorithm you are,
        and why the invariant should be preserved here. This will also help you to identify redundant subgoals,
        and thus prove more general invar_stepXXX lemmas, that solve more than one subgoal!
    *)
    apply (auto simp: invar_step1 invar_step2 invar_step3 invar_step3' invar_step4) 
    done
  subgoal (* Invariant implies desired property. SHOW THAT FIRST! Otherwise, you might waste time on proving a too weak invariant.  *)
    by (auto simp: loop_invar_def loop_invar_complete_final split: if_splits)
  subgoal (* Our well-founded relation is well-founded. By construction! *)
    by (auto simp: dfs_wfrel_def)
  subgoal (* State decreases. We expect the same cases as for the invariant preservation lemma.
    Usually, they are simpler to prove, though. 
  
    We have realized that, after unfolding dfs_wfrel, auto could prove all but the last subgoal, 
    however, it looped on the last subgoal. Thus we explored it in more depth:
  *)
    apply (auto simp: neq_Nil_conv split: list.splits)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal by (auto simp: dfs_wfrel_def)
    subgoal for d c v w u p
      apply (simp add: dfs_wfrel_def)
      apply clarsimp
      apply (rule compl_subsetI)
        unfolding loop_invar_def loop_invar_sound_def
        by auto
      done
  done
  
subsection \<open>Final Correctness Theorem\<close> 
  
theorem dfs_find_cycle_correct: "dfs_find_cycle gi v\<^sub>0 \<longleftrightarrow> has_loop g v\<^sub>0"
  using dfs_find_cycle_aux_correct unfolding dfs_find_cycle_def by auto

end 

section \<open>Refinement\<close>

(*
  While it is easy to implement the done set by a red-black tree,
  maintaining an additional red-black-tree for the nodes on the stack is 
  more difficult: When we pop a node from the stack, we must know that it 
  is not elsewhere on the stack. Otherwise, it would be illegal to remove from the RBT for the stack.
  Actually, the stack is always distinct, and we can easily prove that by augmenting the invariant.
  Unfortunately, when refining the loop, we do not have access to the invariant we proved earlier!
  
  Thus, currently, the only possibility would be adding the set of nodes on the stack 
  already to the abstract loop, such that we can just refine set operations as usual.
  Next year, we'll see a mechanism how to pass knowledge from the correctness proof down to 
  the refinement proof.
  
  Now, we only show refinement of the d set. Amending the abstract algorithm by a set for the stack, 
  and refining it to use an RBT may be a good (and not too hard) ORIGINAL PROJECT!

*)

definition "dfs_find_cycle_aux_impl gi v\<^sub>0 \<equiv> 
  while_option (\<lambda>(d,w,c). w\<noteq>[] \<and> \<not>c) (\<lambda>(d,w,c). 
    let stack = snd`set w in
    let ((p,v),w) = (hd w, tl w) in
    case p of 
      [] \<Rightarrow> (rbts_insert v d, w, False)
    | u # p \<Rightarrow> (
        if rbts_member u d then (d, (p,v) # w, False) 
        else if u\<in>stack then (d, w, True)
        else (d, (rg_succs gi u, u) # (p,v) # w, False))
  ) (rbts_empty,[(rg_succs gi v\<^sub>0, v\<^sub>0)],False)
"

lemma dfs_find_cycle_aux_impl_refine: "rel_option (rel_prod rel_rbts (=)) (dfs_find_cycle_aux_impl gi v\<^sub>0) (dfs_find_cycle_aux gi v\<^sub>0)"
  unfolding dfs_find_cycle_aux_impl_def dfs_find_cycle_aux_def
  apply (rule while_option_refine)
  apply (auto split: prod.splits list.splits)
  done

definition "dfs_find_cycle_impl gi v\<^sub>0 \<equiv> let (_,_,c) = the (dfs_find_cycle_aux_impl gi v\<^sub>0) in c"
  
lemma "dfs_find_cycle_impl gi v\<^sub>0 = has_loop (rg_\<alpha> gi) v\<^sub>0" 
  using dfs_find_cycle_aux_correct[of gi v\<^sub>0]  
  unfolding dfs_find_cycle_impl_def dfs_find_cycle_def
  using dfs_find_cycle_aux_impl_refine[of gi v\<^sub>0]
  apply (cases "dfs_find_cycle_aux_impl gi v\<^sub>0")
  apply auto 
  done


end
