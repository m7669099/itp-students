section \<open>Iteration over Finite Set\<close>
theory Foreach
imports Refine
begin

  definition "mforeach S f s \<equiv> do {
    REC (\<lambda>mforeach (S,s). do {
      assert (finite S);
      if S={} then return s
      else do {
        x \<leftarrow> SPEC (\<lambda>x. x\<in>S);
        let S = S - {x};
        s \<leftarrow> f x s;
        mforeach (S, s)
      }
    }) (S,s)
  }"  
    
  lemma mforeach_unfold: "mforeach S f s = do {
    assert (finite S);
    if S={} then return s
    else do {
      x \<leftarrow> SPEC (\<lambda>x. x\<in>S);
      let S = S - {x};
      s \<leftarrow> f x s;
      mforeach S f s
    }
  }"
    apply (unfold mforeach_def)
    apply (subst REC_unfold)
    apply (intro mono)
    by simp
    
  lemma wp_mforeach: 
    assumes F: "finite S"
    assumes I0: "I {} S s"
    assumes IS: "\<And>x s S\<^sub>1 S\<^sub>2. \<lbrakk> I S\<^sub>1 S\<^sub>2 s; S=S\<^sub>1\<union>S\<^sub>2; S\<^sub>1\<inter>S\<^sub>2={}; x\<notin>S\<^sub>1; x\<in>S\<^sub>2 \<rbrakk> \<Longrightarrow> wp (f x s) (I (insert x S\<^sub>1) (S\<^sub>2-{x}))"
    assumes IF: "\<And>s. I S {} s \<Longrightarrow> Q s"
    shows "wp (mforeach S f s) Q"
    unfolding mforeach_def
    supply [wp_rule] = wp_REC[where 
      V="measure (\<lambda>(S,_). card S)" and 
      P="\<lambda>(S\<^sub>2,s). S\<^sub>2\<subseteq>S \<and> I (S-S\<^sub>2) (S\<^sub>2) s" and
      Q="\<lambda>_ s. Q s"
    ]
    apply wp_step+
    apply (clarsimp; erule IS[THEN wp_cons])
    apply wp_step+
    
    apply (simp_all add: IF I0)

    subgoal by (meson F card_Diff1_less rev_finite_subset)
    apply blast
    apply blast
    subgoal by (smt (z3) Diff_insert2 Set.set_insert insert_Diff insert_Diff_if subsetD subset_insert_iff)
    subgoal using F infinite_super by blast
    done

    
    
  lemma mono_mforeach[mono]:
    assumes "\<And>D x s. nres_mono' (\<lambda>D. F D x s)"
    shows "nres_mono' (\<lambda>D. mforeach S (\<lambda>x s. F D x s) s)"
    using assms unfolding mforeach_def
    apply (intro mono)
    .
    
    
  definition mforeachI 
    :: "('a set \<Rightarrow> 'a set \<Rightarrow> 'b \<Rightarrow> bool) \<Rightarrow> 'a set \<Rightarrow> ('a \<Rightarrow> 'b \<Rightarrow> 'b nres) \<Rightarrow> 'b \<Rightarrow> 'b nres" 
    where "mforeachI I \<equiv> mforeach"

  lemma mforeachI_unfold: "mforeachI I S f s = do {
      assert (finite S);
      if S={} then return s
      else do {
        x \<leftarrow> SPEC (\<lambda>x. x\<in>S);
        let S = S - {x};
        s \<leftarrow> f x s;
        mforeachI I S f s
      }
    }"
    unfolding mforeachI_def
  by (rule mforeach_unfold)
    
  lemma wp_mforeachI[wp_rule]: 
    assumes F: "finite S"
    assumes I0: "I {} S s"
    assumes IS: "\<And>x s S\<^sub>1 S\<^sub>2. \<lbrakk> I S\<^sub>1 S\<^sub>2 s; S=S\<^sub>1\<union>S\<^sub>2; S\<^sub>1\<inter>S\<^sub>2={}; x\<notin>S\<^sub>1; x\<in>S\<^sub>2 \<rbrakk> \<Longrightarrow> wp (f x s) (I (insert x S\<^sub>1) (S\<^sub>2-{x}))"
    assumes IF: "\<And>s. I S {} s \<Longrightarrow> Q s"
    shows "wp (mforeachI I S f s) Q"
    unfolding mforeachI_def
    using wp_mforeach[OF assms] by fast
              
  lemma mono_mforeachI[mono]:
    assumes "\<And>D x s. nres_mono' (\<lambda>D. F D x s)"
    shows "nres_mono' (\<lambda>D. mforeachI I S (\<lambda>x s. F D x s) s)"
    unfolding mforeachI_def
    apply (intro mono assms)
    .



end
