theory Graph_Lib
imports Main "HOL-Library.RBT" 
begin

  subsection \<open>Abstract Graphs\<close>
  type_synonym 'v graph = "('v\<times>'v) set"
  
  definition empty_graph :: "'v graph" where 
    "empty_graph \<equiv> {}"
  
  definition add_edge :: "('v\<times>'v) \<Rightarrow> 'v graph \<Rightarrow> 'v graph" where
    "add_edge e E \<equiv> E\<union>{e}"  

  subsection \<open>As RBTs\<close>  
  type_synonym 'v rbt_graph = "('v,'v list) rbt"
  
  definition rg_\<alpha> :: "'v::linorder rbt_graph \<Rightarrow> 'v graph"  where 
    "rg_\<alpha> t \<equiv> {(u,v). \<exists>succs. RBT.lookup t u = Some succs \<and> v\<in>set succs  }"
  
  definition rg_empty :: "'v::linorder rbt_graph" where 
    "rg_empty \<equiv> RBT.empty"
  
  definition rg_add_edge :: "('v::linorder\<times>'v) \<Rightarrow> 'v rbt_graph \<Rightarrow> 'v rbt_graph" where
   "rg_add_edge \<equiv> \<lambda>(u,v) t. case RBT.lookup t u of
        None \<Rightarrow> RBT.insert u [v] t
      | Some succs \<Rightarrow> RBT.insert u (v#succs) t"  
  
  definition rg_succs :: "'v::linorder rbt_graph \<Rightarrow> 'v \<Rightarrow> 'v list" where
    "rg_succs g v \<equiv> case RBT.lookup g v of None \<Rightarrow> [] | Some succs \<Rightarrow> succs"
      
      
  context
    notes [simp] = rg_\<alpha>_def rg_empty_def rg_add_edge_def
       empty_graph_def add_edge_def rg_succs_def
  begin    
      
  lemma rg_empty_correct[simp]: "rg_\<alpha> rg_empty = empty_graph" by auto
        
  lemma rg_add_edge_correct[simp]: "rg_\<alpha> (rg_add_edge e t) = add_edge e (rg_\<alpha> t)"
    apply (auto split: prod.splits option.split if_splits)
    done

  lemma rg_succs_correct[simp]: "set (rg_succs g v) = rg_\<alpha> g `` {v}"
    by force 
          
  end  

  text \<open>Graphs represented by red-black-trees are finite\<close>
  lemma rg_finite_dom[simp, intro!]: "finite (fst`rg_\<alpha> g)" 
  proof -
    have "fst`rg_\<alpha> g \<subseteq> dom (RBT.lookup g)"
      by (auto simp: rg_\<alpha>_def)
    also have "finite \<dots>" by simp
    finally (finite_subset) show ?thesis .
  qed
  
  
  term \<open>(\<union>)\<close>
  term \<open>\<Union>\<close>
  
  lemma rg_finite_ran[simp, intro!]: "finite (snd`rg_\<alpha> g)"
  proof -
    have "snd`rg_\<alpha> g \<subseteq> \<Union> (set ` ran (RBT.lookup g))"
      by (auto simp: rg_\<alpha>_def ran_def)
    also have "finite \<dots>" 
      apply (rule finite_Union)
      by (auto simp: finite_ran)
    finally (finite_subset) show ?thesis .
  qed
  
  

  subsection \<open>As lists\<close>
  
  type_synonym 'v list_graph = "('v\<times>'v) list"
  
  definition lg_\<alpha> :: "'v list_graph \<Rightarrow> 'v graph" where
    \<open>lg_\<alpha> xs = set xs\<close>
  
  definition lg_empty :: "'v list_graph" where 
    "lg_empty \<equiv> []"
  
  definition lg_add_edge :: "('v\<times>'v) \<Rightarrow> 'v list_graph \<Rightarrow> 'v list_graph" where 
    "lg_add_edge \<equiv> (#)"
  
  context 
    notes [simp] = lg_\<alpha>_def lg_empty_def lg_add_edge_def
  begin  
    
  lemma [simp]: "lg_\<alpha> lg_empty = {}" by simp
  lemma [simp]: "lg_\<alpha> (lg_add_edge e xs) = {e} \<union> lg_\<alpha> xs" by simp
  
  end
  
  
  definition rg_from_lg :: \<open>'v::linorder list_graph \<Rightarrow> 'v rbt_graph\<close> where 
    "rg_from_lg xs \<equiv> fold rg_add_edge xs rg_empty"
  
  lemma lg_\<alpha>_alt: "lg_\<alpha> xs = fold add_edge xs empty_graph"
  proof -
    have "fold add_edge xs E = lg_\<alpha> xs \<union> E" for E
      unfolding add_edge_def lg_\<alpha>_def
      apply (induction xs arbitrary: E)
      apply (auto simp: empty_graph_def)
      done
    from this[where E="{}"] show ?thesis unfolding empty_graph_def by simp
  qed
  
  lemma [simp]: \<open>rg_\<alpha> (rg_from_lg xs) = lg_\<alpha> xs\<close> 
  proof -
    have "rg_\<alpha> (fold rg_add_edge xs t) = fold add_edge xs (rg_\<alpha> t)" for t 
      apply (induction xs arbitrary: t)
      apply auto
      done
    from this[where t=rg_empty] show ?thesis
      unfolding lg_\<alpha>_alt rg_from_lg_def by simp
  qed
  
end

