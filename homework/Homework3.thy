chapter \<open>Homework 3\<close>
theory Homework3
imports Main "~~/src/HOL/Library/Multiset"
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #3
    RELEASED: Mon, Nov 29 2021
    DUE:      Tue, Dec 07, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)
  

section \<open>General Hints\<close>  
(*
  The best way to work on this homework is to fill in the missing gaps in this file.

  Try to go for simple function definitions, as simple definitions mean simple proofs!
  In particular, do not go for a more complicated definition only b/c it may be more efficient!


  The proofs should work with induction (structural/computations), 
  and some generalizations, followed by auto to solve the subgoals.
  You may have to add some simp-lemmas to auto, which we will hint at.
 
  We indicate where auxiliary lemmas are likely to be needed.
  However, as proofs depend on your definitions, we cannot predict every corner 
  case that you may run into. Changing the definition to something simpler might 
  help to get a simpler proof.


*)

section \<open>General definitions\<close>
(*
  The following definitions have already been used in the lecture. It is now time to
  put them to use.
*)

text \<open>Start over with the syntax tree\<close>  
type_synonym vname = string
type_synonym val = int
type_synonym state = "vname \<Rightarrow> val"

  

section \<open>1: Arithmetic expressions with exceptions (20 points)\<close>
(*
  We define a set of arithmetic expressions that supports addition as well as division.
  The out put is an integer value. However, remember that dividing by 0 does not yield
  an integer. In this case the output should be an exception.
*)

datatype aexp = N int | V vname | Plus aexp aexp | Div aexp aexp 

(*
  1a) Specify: The evaluation function for an expression that outputs an exception (in 
    the form of value \<open>None\<close>) if we divide by 0. (5 pts.)
*)
fun aval :: "aexp \<Rightarrow> state \<Rightarrow> val option" where
(*<*)
  "aval _ _ = undefined"
(*>*)


(*
  1b) Show: If everything is alright, an occurrence of 0 on the right hand side in any form
    should cause an exception to be thrown. Prove the following lemmas. (5 pts.)
    Hint: no induction needed ;)
*)
lemma "s x = 0 \<Longrightarrow> aval (Div a (V x)) s = None"
(*<*)  
  sorry
(*>*)


lemma "aval (Div a (Plus (N (0-x)) (N x))) s = None"
(*<*)
  sorry
(*>*)

(*
  1c) Specify: A function that correctly propagates all constants in an expression.
    In other words: if an operation (Div or Plus) is performed on a constant (N x)
    we evaluate the expression. (5 pts.)
  HINT: Plus (N 3) (N 2) becomes (N 5) and Div (N 6) (Plus (N 1) (N 1)) becomes N 2. 
    Division by 0 is not evaluated.
*)

fun cprop :: "aexp \<Rightarrow> aexp" where
(*<*)
  "cprop _ = undefined"
(*>*)


(*
  1d) Show: An expression with propagated constants evaluates to the same value as 
    the original expression. (5 pts.)
    
  HINT: you might need splitting. If you are left with 
    strange subgoals that auto cannot solve, or the invocation of auto takes a long time,
    you may try to split sequentially, i.e., first split on one, than on the other datatype.
    You might also try to use split!: instead of split: for a different strategy for 
    combining splitting and simplification.
*)
lemma "aval (cprop a) s = aval a s" 
(*<*)
  sorry
(*>*)


section \<open>2: Words on a Labelled Transition System(LTS) (20 points)\<close>
(*
  We look at a simple definition of an LTS. Here we define it as a function that maps
  a source state of type \<open>'q\<close>, a symbol of type \<open>'a\<close> and a destination state of type
  \<open>'q\<close> to a boolean value. It evaluates to true if there is an edge between the source
  and destination state with the specified symbol.
  e.g. for an LTS L, L q1 a q2 evaluates to true if we can make a transition from state
  q1 to q2 by reading symbol a.
*)

type_synonym ('q,'a) lts = "'q \<Rightarrow> 'a \<Rightarrow> 'q \<Rightarrow> bool"

(*
  2a) Specify: We can make words by following a path on the LTS. Unlike a Finite-State
    Automaton, an accepting path on an LTS can start and end in any state. Define the
    function \<open>word\<close> in such a way that \<open>word L p xs q\<close> evaluates to True if and only if
    there is a path starting in p that follows the symbols in xs (we call this the word)
    and ends in q. For a word \<open>x # xs\<close> this means that there exists a 
    destination state r such that there is an edge between p and r such that we can 
    complete the word xs from there. (5 pts.)
*)
fun word :: "('q,'a) lts \<Rightarrow> 'q \<Rightarrow> 'a list \<Rightarrow> 'q \<Rightarrow> bool" where
  "word _ _ _ _ = undefined"


(*
  2b) Show: If we have a word \<open>as\<close> starting in p and ending in q as well as
    a word \<open>bs\<close> starting in q and ending in r, then we also have a word where \<open>bs\<close> is 
    appended to \<open>as\<close> that starts in p and ends in r. (3 pts.)
*)
lemma word_append: "word L p as q \<Longrightarrow> word L q bs r \<Longrightarrow> word L p (as@bs) r"
(*<*)
  sorry
(*>*)


(*
  2c) Show: We have given a function duplicate which takes a list xs as well as a natural
    number n and it returns a new list where xs is repeated n times. If there is a word \<open>as\<close> 
    that starts and ends in the same state, then there also is a word \<open>duplicate as n\<close> that
    starts and ends in that exact same state. Prove the following lemma.
    
  NOTE: no sledgehammer. We won't accept proofs generated by sledgehammer. 
    do not use metis, meson, smt, or using thms by auto/force/... idioms, that are usually 
    generated by sledgehammer! You may need auxiliary lemmas.
      
*)
fun duplicate :: "'a list \<Rightarrow> nat \<Rightarrow> 'a list" where
  "duplicate xs 0 = []"
| "duplicate xs (Suc n) = xs @ duplicate xs n"


(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)


lemma pump: "word L p as q \<Longrightarrow> word L q bs q \<Longrightarrow> word L q cs q \<Longrightarrow> word L p (as @ (duplicate bs n) @ cs) q"
(*<*)
  sorry
(*>*)


(* 2d) Show: Prove the following lemmas (you might have proven them as auxiliary lemmas already) 
    and then use them to construct forward proofs. (7 pts.)
*)
lemma dup_loop: "word L p as p \<Longrightarrow> word L p (duplicate as n) p"(*<*)sorry(*>*)
lemma word_empty: "word L p [] p" (*<*)sorry(*>*)
lemma word_cons: "L p a q \<Longrightarrow> word L q as r \<Longrightarrow> word L p (a#as) r" (*<*)sorry(*>*)

(*
  Using any of dup_loop, word_empty, word_cons, and word_append,construct the 
  following statements via forward proof [OF ...]. 
*)

(* For example: *)
lemmas stmt_ex = word_append[OF _ word_cons[OF _ word_empty]]
lemma "\<lbrakk>word L p as q; L q a r\<rbrakk> \<Longrightarrow> word L p (as @ [a]) r"
  by(rule stmt_ex)

(* Solve these yourself! *)
lemma \<open>\<lbrakk>word L p as p; word L p bs p\<rbrakk> \<Longrightarrow> word L p (duplicate as n\<^sub>1 @ duplicate bs n\<^sub>2) p\<close>
(*<*)
  sorry
(*>*)


lemma \<open>\<lbrakk>word L p as q; word L q bs q\<rbrakk> \<Longrightarrow> word L p (as @ duplicate bs n) q\<close>
(*<*)
  sorry
(*>*)


lemma \<open>\<lbrakk>L p a q; L q b r\<rbrakk> \<Longrightarrow> word L p [a, b] r\<close>
(*<*)
  sorry
(*>*)





section \<open>Bonus: Quicksort (5 bonus points)\<close> 
(*
  In the tutorial we have already taken a look at mergesort. We are now going to define quicksort
  and show that it is implemented correctly.
*)


(*
  a) Specify: The quicksort algorithm has previously been introduced in the lecture, now we define
    it in a slightly different way using the \<open>partition\<close> function. We only award points if the 
    partition function is used. (1 pt.)
  HINT: You can look at the definition of the partition function by typing \<open>term partition\<close>. By pressing
    control and clicking it you can see the definition.
*)

fun quicksort :: "nat list \<Rightarrow> nat list" where
(*<*)
  "quicksort _ = undefined"
(*>*)


(*
  b) Show: We have already seen the multiset in the tutorial. One requirement for a sorting algorithm is that
   a sorted list must have the same multiset as the original list. (1 pt.)
*)

lemma quicksort_preserves_mset: "mset (quicksort xs) = mset xs"
(*<*)  
  sorry
(*>*)


(*
  c) Show: Another requirement for sorting algorithms is that it should actually sort the list.
  HINT: Auxiliary lemmas may be needed, also auxiliary lemmas for your auxiliary lemmas may be needed.
    You also may have to unfold constants like function composition (o_def). Also have a look at
    thm sorted_append, before you end up re-proving it as an aux-lemma ;)
*)

(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)

lemma quicksort_sorts: "sorted (quicksort xs)"
(*<*)
  sorry
(*>*)
  
end
