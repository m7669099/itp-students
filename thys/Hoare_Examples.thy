theory Hoare_Examples
imports Hoare_Logic Foreach "HOL-Library.Multiset" "List-Index.List_Index"
begin

subsection \<open>Euclid's Algorithm\<close>
  text \<open>Euclids algorithm to compute the greatest common divisor (gcd). 
    The invariant is that the gcd of \<open>a\<close> and \<open>b\<close> remains the same, 
    and the variant is that \<open>a+b\<close> decreases in every iteration.
    
    Additionally, the invariant includes that \<open>a\<close> and \<open>b\<close> are positive.
    \<close>

  definition "euclid a\<^sub>0 b\<^sub>0 \<equiv> do {
    (a,b) \<leftarrow> WHILEI (measure (\<lambda>(a,b). a+b)) (\<lambda>(a,b). a>0 \<and> b>0 \<and> gcd a b = gcd a\<^sub>0 b\<^sub>0) 
      (\<lambda>(a,b). return a\<noteq>b) 
      (\<lambda>(a,b). if a<b then return (a,b-a) else return (a-b,b))
      (a\<^sub>0,b\<^sub>0);
    return a
  }"

  text \<open>Correctness: for positive arguments, \<open>euclid\<close> terminates with the gcd\<close>
  lemma wp_euclid[THEN wp_cons, wp_rule]: "a>0 \<Longrightarrow> b>0 \<Longrightarrow> wp (euclid a b) (\<lambda>r. r=gcd a b)"
    unfolding euclid_def
    apply wp
    apply auto
    subgoal by (metis gcd_diff1_nat leI)
    subgoal by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
    done

(*    oops    apply (intro wp_rule)
    apply auto
    apply (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
    by (metis gcd_diff1_nat leI)
*)    
    
  (* EXERCISE: implement and prove correct the variant with div/mod! *)  
    
    
subsection \<open>Binary Search\<close>    
  text \<open>
    Generalized binary search: given a monotonic predicate over natural numbers, 
    find the least index in the range \<^term>\<open>{l..<h}\<close> for that \<open>P\<close> holds. 
    If there is no such index, return \<open>h\<close>.
    
      0 1 2 3 4 5 6 7
    P f f f f t t t t 
          l     h
    
  \<close>

  definition "approx_upper l\<^sub>0 h\<^sub>0 P \<equiv> do {
    (l,h) \<leftarrow> WHILEI (measure (\<lambda>(l,h). h-l)) 
      (\<lambda>(l,h). l\<^sub>0\<le>l \<and> l\<le>h \<and> h\<le>h\<^sub>0 \<and> (\<forall>i\<in>{l\<^sub>0..<l}. \<not>P i) \<and> (\<forall>i\<in>{h..<h\<^sub>0}. P i))
      (\<lambda>(l,h). return (l<h))
      (\<lambda>(l,h). do {
        let m = (l+h) div 2;
        assert m < h;
        if P m then return (l,m) else return (m+1,h)
      }) (l\<^sub>0,h\<^sub>0);
  
    return h  
  }"

  lemma approx_upper_correct[THEN wp_cons,wp_rule]:   
    assumes MONO: "mono P"
    assumes LH: "l\<le>h"
    shows "wp (approx_upper l h P) (\<lambda>r. r\<in>{l..h} \<and> (\<forall>i\<in>{l..<r}. \<not>P i) \<and> (\<forall>i\<in>{r..<h}. P i))"
    unfolding approx_upper_def
    apply wp
    apply (all \<open>(simp add: LH)?\<close>)
    subgoal
      apply auto
      by (metis MONO le_bool_def less_Suc_eq_le monoD)
    subgoal
      apply auto
      apply (metis MONO le_bool_def monoE)
      done
    done

    
    
  text \<open>We can use the same algorithm to find the greatest element for that a predicate 
    does not hold\<close>

  definition "approx_lower l h P \<equiv> do {
    assert (\<not>P l \<and> P h);
    r \<leftarrow> approx_upper l h P;
    assert (r>l);
    return (r-1)
  }"
    
  lemma approx_lower_correct[THEN wp_cons,wp_rule]:
    assumes "mono P" "\<not>P l" "P h"
    shows "wp (approx_lower l h P) (\<lambda>r. r\<in>{l..<h} \<and> \<not>P r \<and> P (r+1))"
    unfolding approx_lower_def
    apply wp
    using assms 
    apply simp_all
    subgoal by (smt (z3) Suc_less_eq Suc_pred atLeastLessThan_iff leI le_less le_zero_eq less_Suc_eq_le)
    subgoal by (metis atLeastLessThan_iff nat_less_le)
    subgoal by (metis le_bool_def monoD nat_le_linear)
    done
        
subsubsection \<open>Integer Square Root\<close>
    
  definition sqrt where "sqrt x = approx_lower 0 (x+1) (\<lambda>r. r^2 > x)"
  
  lemma sqrt_correct[THEN wp_cons,wp_rule]: "wp (sqrt x) (\<lambda>r. r^2\<le>x \<and> (r+1)^2>x)"
    unfolding sqrt_def
    apply wp_step
    apply simp_all
    subgoal apply rule by (meson le_bool_def less_le_trans power2_nat_le_eq_le)
    subgoal by (simp add: power2_eq_square) 
    done
  
subsection \<open>Quicksort\<close>      

  text \<open>We specify what sorting a list means: reshuffling the list such that it is sorted: \<close>      
  definition "sort_spec xs xs' \<equiv> mset xs' = mset xs \<and> sorted xs'"
  text \<open>
    The first part of the specification, \<^term>\<open>mset xs' = mset xs\<close>, 
    states that the result contains the same elements as the input list. 
    Here, \<^const>\<open>mset\<close> is the multiset of the elements in a list.  
  \<close>
  
  text \<open>Obviously, a list with less than two elements is sorted, thus the sorting algorithm
    can return the same list: \<close>
  lemma sort_spec_01: "length xs < 2 \<Longrightarrow> sort_spec xs xs"
    by (auto simp: sort_spec_def sorted01)

  text \<open>The main operation of quicksort is partitioning: split a list into
    a pivot element, a left partition, and a right partition, such that 
    all elements of the left partition are \<open>\<le>\<close> the pivot, and all elements 
    of the right partition are \<open>\<ge>\<close> the pivot:
  \<close>  
  definition partition_spec :: "'a::linorder list \<Rightarrow> 'a list \<times> 'a \<times> 'a list \<Rightarrow> bool" where 
    "partition_spec xs \<equiv> \<lambda>(xs\<^sub>1,p,xs\<^sub>2). 
    mset xs = mset (xs\<^sub>1@p#xs\<^sub>2) \<and> (\<forall>x\<in>set xs\<^sub>1. x\<le>p) \<and> (\<forall>x\<in>set xs\<^sub>2. x\<ge>p)"

    
  text \<open>
    The quicksort algorithm sorts a list by partitioning it around some pivot element,
    and then recursively sorting the two partitions:
  \<close>            
  definition "quicksort xs \<equiv> RECI (measure length) (\<lambda>_. True) (\<lambda>quicksort xs. 
    if length xs < 2 then return xs 
    else do {
      (xs\<^sub>1,p,xs\<^sub>2) \<leftarrow> SPEC (partition_spec xs);
      xs\<^sub>1 \<leftarrow> quicksort xs\<^sub>1;
      xs\<^sub>2 \<leftarrow> quicksort xs\<^sub>2;
      return xs\<^sub>1@p#xs\<^sub>2
    }
  ) xs"

    
        
  text \<open>The main argument of quicksort: partitioning and then sorting the partitions sorts the list: \<close>      
  lemma sort_spec_part: "\<lbrakk>
      partition_spec xs (xs\<^sub>1,p, xs\<^sub>2); 
      sort_spec xs\<^sub>1 xs\<^sub>1'; 
      sort_spec xs\<^sub>2 xs\<^sub>2'
    \<rbrakk> \<Longrightarrow> sort_spec xs (xs\<^sub>1' @ p # xs\<^sub>2')"
    by (force 
      simp: partition_spec_def sort_spec_def sorted_append 
      simp flip: set_mset_mset)

  text \<open>Main termination argument: the partitions are smaller than the original list:\<close>  
  lemma partition_decr_len: "partition_spec xs (xs\<^sub>1, p, xs\<^sub>2) \<Longrightarrow> length xs\<^sub>1 < length xs \<and> length xs\<^sub>2 < length xs"
    unfolding partition_spec_def
    apply (simp)
    by (metis less_add_Suc1 less_add_Suc2 size_add_mset size_mset size_union)

  text \<open>With these two lemmas, we easily prove the quicksort algorithm correct:\<close>
  lemma quicksort_correct: "wp (quicksort xs) (sort_spec xs)"
    unfolding quicksort_def
    (*
    apply wp_step
    apply simp
    apply wp_step
    apply simp
    apply wp_step
    apply wp_step
    defer
    apply wp_step
    apply wp_step
    apply wp_step
    apply wp_step
    apply wp_step
    apply (rule wp_cons)
    apply rprems    
    defer
    defer
    apply wp_step
    apply (rule wp_cons)
    apply rprems    
    defer
    defer
    apply wp_step
    defer
    
    
    apply simp_all
    apply (simp add: sort_spec_01)
    apply (simp add: partition_decr_len)
    apply (simp add: partition_decr_len)
    apply (simp add: sort_spec_part)
    
    *)
    apply wp
    apply (simp_all add: sort_spec_01 sort_spec_part partition_decr_len)
    done
    
subsection \<open>Depth First Search\<close>

  text \<open>Depth first search over a graph with finitely many reachable nodes.
  
    The graph is modeled as its edge relation, and a start node.
  \<close>        
  locale fin_graph =
    fixes E :: "'a rel" and s\<^sub>0 :: "'a"
    assumes fin: "finite (E\<^sup>*``{s\<^sub>0})"  \<comment> \<open>Finitely many nodes reachable from the start\<close>
  begin  

    (* TODO: generalize to allow or disallow duplicates in workset! *)
      
    definition "dfs_invar V W \<equiv> W\<union>V \<subseteq> E\<^sup>*``{s\<^sub>0} \<and> E``V \<subseteq> V\<union> W \<and> s\<^sub>0\<in> W\<union>V"
    definition "dfs_var \<equiv> measure (\<lambda>V. card (E\<^sup>*``{s\<^sub>0} - V)) <*lex*> measure size"
  
    definition "dfs \<equiv> do {
      (V,_) \<leftarrow> WHILEI 
        dfs_var
        (\<lambda>(V,W). dfs_invar V (set_mset W))
        (\<lambda>(V,W). return W\<noteq>{#})
        (\<lambda>(V,W). do {
          x \<leftarrow> spec x. x\<in>#W;
          let W = W - {#x#};
          if x\<in>V then return (V,W)
          else do {
            let V = insert x V ;
            W \<leftarrow> spec W'. set_mset W' = set_mset W \<union> E``{x};
            return (V,W)
          }
        }
        ) ({},{#s\<^sub>0#});
      return V
    }"
      
    lemma aux1: "wf local.dfs_var"
      by (auto simp: dfs_var_def)
    lemma aux2: "local.dfs_invar {} {s\<^sub>0}"
      by (auto simp: dfs_invar_def)
    lemma aux3: "\<And>V W x. \<lbrakk>local.dfs_invar V W; x \<in> W; x \<in> V\<rbrakk> \<Longrightarrow> local.dfs_invar V (W - {x})"
      by (auto simp: dfs_invar_def)
      
    lemma aux4: "\<And>V W x. \<lbrakk> x \<in># W; x \<in> V\<rbrakk> \<Longrightarrow> ((V, W - {#x#}), V, W) \<in> local.dfs_var"
      by (simp add: dfs_var_def size_Diff1_less)
    lemma aux5: "\<And>V W x. \<lbrakk>local.dfs_invar V W; x \<in> W; x \<notin> V\<rbrakk> \<Longrightarrow> local.dfs_invar (insert x V) (W - {x} \<union> (E `` {x}))"
      by (auto simp: dfs_invar_def)
    lemma aux5': "\<And>V W x. \<lbrakk>local.dfs_invar V W; x \<in> W; x \<notin> V\<rbrakk> \<Longrightarrow> local.dfs_invar (insert x V) (W \<union> (E `` {x}))"
      by (auto simp: dfs_invar_def)
      
    lemma aux6: "\<And>V W x. \<lbrakk>local.dfs_invar V (set_mset W); x \<in># W; x \<notin> V\<rbrakk> \<Longrightarrow> ((insert x V, W'), V, W) \<in> local.dfs_var"
      apply (clarsimp simp: dfs_var_def dfs_invar_def)
      by (metis Diff_iff Diff_insert card_Diff1_less fin finite_Diff in_mono)
    lemma aux7: "\<And>V x. \<lbrakk>local.dfs_invar V {}; x \<in> V\<rbrakk> \<Longrightarrow> (s\<^sub>0, x) \<in> E\<^sup>*"
      by (auto simp: dfs_invar_def)
    lemma aux8: "\<And>V x. \<lbrakk>local.dfs_invar V {}; (s\<^sub>0, x) \<in> E\<^sup>*\<rbrakk> \<Longrightarrow> x \<in> V"
      apply (clarsimp simp: dfs_invar_def)
      by (metis Image_closed_trancl rev_ImageI)

    lemmas aux = aux1 aux2 aux3 aux4 aux5 aux5' aux6 aux7 aux8
            
    lemma wp_dfs[THEN wp_cons,wp_rule]: "wp dfs (\<lambda>r. r=E\<^sup>*``{s\<^sub>0})"
      unfolding dfs_def
      apply wp_step+
      apply (auto simp: aux)
      subgoal
        by (metis at_most_one_mset_mset_diff aux5 aux5' more_than_one_mset_mset_diff)
      subgoal
        by (metis at_most_one_mset_mset_diff aux3 more_than_one_mset_mset_diff)
      done
      
  end      

  

end
