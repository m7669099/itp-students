section "Arithmetic and Boolean Expressions"

theory tut04 imports Main "HOL.Transcendental"
begin

section \<open>Context-Free Grammar\<close>

text \<open>We define a grammar for palindromes consisting of the two symbols \<open>a\<close> and \<open>b\<close>:
  S \<longrightarrow> aSa
  S \<longrightarrow> bSb
  S \<longrightarrow> \<epsilon>

We can define this in Isabelle/HOL using an inductive definition.
\<close>

datatype ab = a | b

inductive S :: "ab list \<Rightarrow> bool" 
(*<*)
(*>*)

text \<open>
  Prove that if \<open>xux\<close> (where \<open>x\<close> is a symbol and \<open>u\<close> is a word) is a word in the language,
  then \<open>u\<close> is also a word in the language:
\<close>

lemma palindromes_S:
  assumes "S (x # u @ [x])"
  shows "S u"
(*<*)
  sorry
(*>*)


text \<open>Also prove that there are no words \<open>aub\<close> or \<open>bua\<close> in the language:\<close>

lemma non_palindromes_not_S: "x \<noteq> y \<Longrightarrow> \<not> S (x # u @ [y])"
(*<*)
  sorry
(*>*)


text \<open>Show that an extension of a word is only valid iff the extension on the end is the same
      symbol as the one at the beginning.\<close>

lemma "S u \<Longrightarrow> S (x # u @ [y]) \<longleftrightarrow> x = y"
(*<*)
  sorry
(*>*)


section \<open>Paths vs. Transitive Closure\<close>


text \<open>We have defined a directed graph as a function which maps a source state and a destination
      state to a bool which evaluates to True if there is an edge starting in the source and 
      ending in the destination.\<close>

type_synonym 'v dgraph = "'v \<Rightarrow> 'v \<Rightarrow> bool"


text \<open>A path is a list of states that are connected through edges.\<close>

fun path :: "'v dgraph \<Rightarrow> 'v \<Rightarrow> 'v list \<Rightarrow> 'v \<Rightarrow> bool" where
(*<*)
  "path E u [] v \<longleftrightarrow> v = u" |
  "path E u (x # xs) v \<longleftrightarrow> (E u x \<and> path E x xs v)"
(*>*)


text \<open>The transitive closure contains an edge iff there is a path between the nodes of this edge\<close>

(*<*)
(*ROOM FOR AUXILIARY LEMMAS*)
(*>*)

lemma "E\<^sup>*\<^sup>* u v \<longleftrightarrow> (\<exists>xs. path E u xs v)"
(*<*)
  sorry
(*>*)


section \<open>Irrationality of sqrt 3\<close>

lemma sqrt_3_irrational: "sqrt 3 \<notin> \<rat>"
(*<*)
  sorry
(*>*)

end
