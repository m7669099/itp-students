section "Arithmetic and Boolean Expressions"

theory tut03_sol imports Main "~~/src/HOL/Library/Multiset" begin


section \<open>Binary Search Tree\<close>

text \<open>We give the following definition for a Binary Search Tree (BST). In this definition
      the nodes have one value and two children. The left tree may only have values lower
      than the current node. The right tree may only have greater values.\<close>

datatype bst = Leaf | Node bst int bst

text \<open>Define the function "set_of_bst" which gives the set of all integers that are in the 
      tree.\<close>

fun set_of_bst :: "bst \<Rightarrow> int set" where
  "set_of_bst Leaf = {}"
| "set_of_bst (Node l x r) = set_of_bst l \<union> {x} \<union> set_of_bst r"  


text \<open>As discussed previously, the values in the tree on the left of each node must be lower
      than the value of the particular node and the right tree must have higher values. This
      invariant must hold for each node in the tree. Define this invariant.\<close>

fun invar_bst :: "bst \<Rightarrow> bool" where
  "invar_bst Leaf = True"
| "invar_bst (Node l x r) \<longleftrightarrow> invar_bst l \<and> invar_bst r \<and> (\<forall>y\<in>set_of_bst l. y<x) \<and> (\<forall>y\<in>set_of_bst r. y>x)"  


text \<open>A tree can also be converted to a list. However, the order matters. One way to approach
      this is by doing an inorder traversal. This means that the list of a node is given by 
      the list of the tree on the left of the node, followed by the value of this node followed
      the list of the tree on the right hand side.\<close>

fun inorder_bst :: "bst \<Rightarrow> int list" where
  "inorder_bst Leaf = []"
| "inorder_bst (Node l x r) = inorder_bst l @ [x] @ inorder_bst r"  


lemma [simp]: "set (inorder_bst t) = set_of_bst t"
  apply (induction t) by auto
  
text \<open>From this we can follow that the inorder list of a tree is sorted with relation to "<".\<close>

lemma "invar_bst t \<longleftrightarrow> sorted_wrt (<) (inorder_bst t)"
  apply (induction t)
  apply (auto simp: sorted_wrt_append)
  by force


text \<open>An important feature of BST's is that it allows us to efficiently find whether a value
      in a node of the tree.\<close>

fun member_bst :: "int \<Rightarrow> bst \<Rightarrow> bool" where
  "member_bst x Leaf = False"
| "member_bst x (Node l y r) \<longleftrightarrow> (if x=y then True else if x<y then member_bst x l else member_bst x r)"  


text \<open>If the invariant holds, then the member function is equivalent to finding that value
      as an element of "set_of_bst t".\<close>

lemma "invar_bst t \<Longrightarrow> member_bst x t \<longleftrightarrow> x \<in> set_of_bst t"
  by (induction t) auto


text \<open>It's also possible to insert a value into a tree, ofcourse the bst invariant needs to 
      still hold after the insert function is called.\<close>

fun insert_bst :: "int \<Rightarrow> bst \<Rightarrow> bst" where
  "insert_bst x Leaf = Node Leaf x Leaf"
| "insert_bst x (Node l y r) = 
  (if x=y then Node l y r 
    else if x<y then Node (insert_bst x l) y r 
    else Node l y (insert_bst x r))"


text \<open>To show that we have implemented this correctly, we need to show that the "insert_bst"
      function is equivalent to inserting that element to the "set_of_bst" of this tree.\<close>

lemma aux1: "set_of_bst (insert_bst x t) = insert x (set_of_bst t)"
  by (induction t) auto


text \<open>If the invariant is satisfied for a tree, then it is also satisfied after an element
      is inserted.\<close>

lemma "invar_bst t \<Longrightarrow> invar_bst (insert_bst x t)"
  apply (induction t) 
   apply (auto simp: aux1)
  done



section \<open>Mergesort\<close>


text \<open>We have already seen Mergesort. The merge function has been introduced there.\<close>

fun merge :: "nat list \<Rightarrow> nat list \<Rightarrow> nat list" where
  "merge [] ys = ys"
| "merge xs [] = xs"
| "merge (x#xs) (y#ys) = 
    (if x<y then x # merge xs (y#ys) else y # merge (x#xs) ys)"

value "merge [2,5] [4,3]"


lemma merge_union_set: "set (merge xs ys) = set xs \<union> set ys"
  apply(induction rule: merge.induct)
  by auto


text \<open>If two sets are sorted, then the merger of those sets is also sorted.\<close>

lemma sorted_merge: "sorted xs \<Longrightarrow> sorted ys \<Longrightarrow> sorted (merge xs ys)"
  apply(induction xs ys rule: merge.induct)
  apply(auto simp: merge_union_set)
  done


lemma mset_merge: "mset (merge xs ys) = mset (xs) + mset (ys)"
  apply(induction rule: merge.induct)
    apply auto
  done


lemma mset_take_drop: "mset (take n xs) + mset (drop n xs) = mset xs"
  apply(induction xs)
  apply auto
   apply(auto simp: union_code)
  done


text \<open>We have also seen this Mergesort algorithm which divides the list into two parts
      which are sorted independently. After that those sorted lists are merged (using 
      the "merge" function.\<close>

fun msort :: "nat list \<Rightarrow> nat list" where
  "msort [] = []"
| "msort [x] = [x]"
| "msort xs = (let n = length xs div 2 in merge 
    (msort (take n xs)) 
    (msort (drop n xs)))"    
    
    
value "msort [1,5,3,4]"  


text \<open>A sorting algorithm must preserve the multiset after sorting.\<close>

lemma "mset (msort xs) = mset xs"
  apply (induction xs rule: msort.induct)
  apply(auto simp: mset_merge)
  apply(auto simp: mset_take_drop)
  done


text \<open>Also, a sorting algorithm needs to actually sort the list.\<close>

lemma "sorted (msort xs)"
  apply(induction xs rule: msort.induct)
  by(auto simp: sorted_merge)


end
