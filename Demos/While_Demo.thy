theory While_Demo
imports Main While_Lib
begin

section \<open>Initial example: Euclid's algorithm\<close>
text \<open>see \<^url>\<open>https://en.wikipedia.org/wiki/Euclidean_algorithm\<close>\<close>

text \<open>\<open>while b c s\<close> -- iterate function c on state s, as long as b holds\<close>
term while

definition "euclid a b \<equiv> fst (
  while (\<lambda>(a,b). a\<noteq>b) (\<lambda>(a,b). 
    if a<b then (b-a,a)
    else (a-b,b)
  ) (a,b))" for a b :: nat

(* Characterization of a loop: unfold rule. That's enough! *)
thm while_unfold

section \<open>Well-Founded Induction\<close>

term wf \<comment> \<open>no infinite strictly decreasing chains\<close>
(* well-founded relation gives induction principle: *)
thm wf_induct_rule
lemma 
  assumes "wf r" \<comment> \<open>well-founded relation\<close>
      and "\<And>x. (\<And>y. (y, x) \<in> r \<Longrightarrow> P y) \<Longrightarrow> P x" \<comment> \<open>IH: P holds for all y<x. To show: P x\<close>
    shows "P a"
  using assms by (rule wf_induct_rule)


(* Discourse: using Hilbert-choice and \<open>rec_nat\<close> to define contradicting sequence: *)
notepad
begin
  fix P :: "'a::ord \<Rightarrow> bool" (* Note: ord is syntactic typeclass. 
    Everything that has \<le>, <, but assumes no properties*)
  assume WF: "\<nexists>ss. \<forall>i. ss (Suc i) < (ss i :: 'a)"
  
  assume STEP: "\<And>s. \<lbrakk>\<And>s'. s'<s \<Longrightarrow> P s'\<rbrakk> \<Longrightarrow> P s"
  
  have "P s" for s
  proof (rule ccontr)
    assume A: \<open>\<not>P s\<close>
  
    text \<open>Main idea: state with \<open>\<not>P s\<close> implies \<open>\<exists>s'<s. \<not>P s'\<close> \<close>
    have aux: "\<not>P s \<Longrightarrow> \<exists>s'<s. \<not>P s'" for s
      using STEP by blast

    text \<open>The rest of the proof is technical, to inductively iterate this argument\<close>  
          
    text \<open>We define the infinite sequence of states, using the SOME operator (Hilbert Choice):\<close>
    define ss where "ss \<equiv> rec_nat s (\<lambda>i s. SOME s'. s'<s \<and> \<not>P s')" \<comment> \<open>trick to define recursive function over nat inside proof!\<close>
    have ss_simps[simp]: "ss 0 = s" "ss (Suc n) = (SOME s'. s'<ss n \<and> \<not>P s')" for n
      unfolding ss_def by auto

    have \<open>ss (Suc i) < ss i \<and> \<not>(P (ss i))\<close> for i
    proof (induction i)
      case 0
      then show ?case apply (simp add: A)
        by (metis (mono_tags, lifting) A STEP someI_ex)
    next
      case (Suc i)
      then show ?case 
        apply simp
        by (metis (mono_tags, lifting) STEP someI_ex)
      
    qed

    with WF show False by blast    
  qed  

end  
thm wf_iff_no_infinite_down_chain
  
  
  
section \<open>While rule\<close>  
(* Proving about while: invariant + induction along well-founded relation *)
thm while_rule

lemma
  assumes "I s" \<comment> \<open>Invariant holds for initial state\<close>
    and "\<And>s. \<lbrakk>I s; b s\<rbrakk> \<Longrightarrow> I (c s)"  \<comment> \<open>Invariant preserved by loop iteration\<close>
    and "\<And>s. \<lbrakk>I s; \<not> b s\<rbrakk> \<Longrightarrow> Q s"  \<comment> \<open>Invariant at end of loop implies statement\<close>
    and "wf r"                      \<comment> \<open>Relation is well-founded\<close>  
    and "\<And>s. \<lbrakk>I s; b s\<rbrakk> \<Longrightarrow> (c s, s) \<in> r" \<comment> \<open>Loop iteration decreases state (wrt. r)\<close>
  shows "Q (while b c s)"
  using assms by (rule while_rule)
 
  
section \<open>Assembling Well-Founded Relations\<close>
term measure \<comment> \<open>wf-relation based on mapping to nat\<close>
lemma "(s',s)\<in>measure f \<longleftrightarrow> f s' < ((f s) :: nat)" by (rule in_measure)

lemma "wf (measure f)" by (rule wf_measure) \<comment> \<open>Obviously well-founded: nat can only decrease finitely often!\<close>

term "measure length"  (* length of list decreases in iterations *)

  
term finite_psubset
lemma "(s',s)\<in>finite_psubset \<longleftrightarrow> s' \<subset> s \<and> finite s" by (rule in_finite_psubset)
thm wf_finite_psubset

term "inv_image r f"  
lemma "(s',s) \<in> inv_image r f \<longleftrightarrow> (f s', f s) \<in> r" by (rule in_inv_image)
lemma "wf r \<Longrightarrow> wf (inv_image r f)" by (rule wf_inv_image)

lemma "measure = inv_image less_than" by auto
    
term "r\<^sub>1 <*lex*> r\<^sub>2"
lemma "((a',b'), (a,b)) \<in> r\<^sub>1 <*lex*> r\<^sub>2 \<longleftrightarrow> (a',a)\<in>r\<^sub>1 \<or> (a'=a \<and> (b',b)\<in>r\<^sub>2)" by (rule in_lex_prod)
lemma "wf r\<^sub>1 \<Longrightarrow> wf r\<^sub>2 \<Longrightarrow> wf (r\<^sub>1 <*lex*> r\<^sub>2)" by (rule wf_lex_prod)

term "measure (\<lambda>(a,b,c). length b)"
(* TODO: tutorial exercise: convert English text into wf-relations *)

section \<open>Proving Eculid Correct\<close>
    
lemma euclid_aux1: "a < b \<Longrightarrow> gcd (b - a) a = gcd a b" for a b :: nat
  by (metis gcd.commute gcd_diff1_nat less_or_eq_imp_le)
  
lemma euclid_aux2: "\<not> a < b \<Longrightarrow> gcd (a - b) b = gcd a b" for a b :: nat
  by (meson gcd_diff1_nat not_le)
 
lemma euclid_correct: "a\<^sub>0\<noteq>0 \<Longrightarrow> b\<^sub>0\<noteq>0 \<Longrightarrow> euclid a\<^sub>0 b\<^sub>0 = gcd a\<^sub>0 b\<^sub>0" 
  unfolding euclid_def
  apply (rule while_rule[where P="\<lambda>(a,b). 
    a\<noteq>0 \<and> b\<noteq>0 \<and> \<comment> \<open>This has to be included, too\<close>
    gcd a b = gcd a\<^sub>0 b\<^sub>0
  " and r="measure (\<lambda>(a,b). a+b)"])
  apply (auto split: if_splits)
  subgoal by (simp add: euclid_aux1)
  subgoal by (simp add: euclid_aux2)
  done

section \<open>Explicit Non-termination\<close>  
  
(* Including possible non-termination:
  if while-loop does not terminate, it still yields a value!
*)  

term \<open>while (\<lambda>_. True) ((+)1) (0::nat)\<close> (* \<leftarrow> value of type nat! *)
(* But we cannot prove anything (useful) about it! *)  

(* To make non-termination explicit, we can return \<open>None\<close> on non-termination *)

term while_option
thm while_option_rule'

(* Now everything is well-defined! *)  
  
(* Note: *)
thm while_def
  
  
definition "euclid' a b \<equiv> map_option fst (
  while_option (\<lambda>(a,b). a\<noteq>b) (\<lambda>(a,b). 
    if a<b then (b-a,a)
    else (a-b,b)
  ) (a,b))" for a b :: nat


lemma euclid'_correct: "a\<noteq>0 \<Longrightarrow> b\<noteq>0 \<Longrightarrow> euclid' a b = Some (gcd a b)" 
  unfolding euclid'_def
  text \<open>Same proof as above, just using @{thm while_option_rule'}.\<close>
  apply (rule while_option_rule'[where P="\<lambda>(a',b'). 
    a'\<noteq>0 \<and> b'\<noteq>0 \<and>
    gcd a' b' = gcd a b
  " and r="measure (\<lambda>(a,b). a+b)"])
  apply (auto split: if_splits)
  subgoal by (simp add: euclid_aux1)
  subgoal by (simp add: euclid_aux2)
  done
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  

(* Discourse: unfolding equation yields while rule *)  
context
  fixes while :: "('s \<Rightarrow> bool) \<Rightarrow> ('s \<Rightarrow> 's) \<Rightarrow> 's \<Rightarrow> 's"  
  assumes while_unfold: "while b c s = (if b s then while b c (c s) else s)"
begin  

  lemma  
    assumes "P s" \<comment> \<open>Invariant holds for initial state\<close>
      and STEP: "\<And>s. \<lbrakk>P s; b s\<rbrakk> \<Longrightarrow> P (c s)"  \<comment> \<open>Invariant preserved by loop iteration\<close>
      and IMP: "\<And>s. \<lbrakk>P s; \<not> b s\<rbrakk> \<Longrightarrow> Q s"  \<comment> \<open>Invariant at end of loop implies statement\<close>
      and "wf r"                      \<comment> \<open>Relation is well-founded\<close>  
      and DECR: "\<And>s. \<lbrakk>P s; b s\<rbrakk> \<Longrightarrow> (c s, s) \<in> r" \<comment> \<open>Loop iteration decreases state (wrt. r)\<close>
    shows "Q (while b c s)"
  proof -
    have "P (while b c s) \<and> \<not>b (while b c s)"
      using \<open>wf r\<close> \<open>P s\<close>
      apply (induction s)
      apply (rule conjI)
      subgoal
        apply (subst while_unfold)
        apply (auto simp: STEP DECR)
        done
      subgoal
        apply (subst while_unfold)
        apply (auto simp: STEP DECR)
        done
      done
    thus ?thesis using IMP ..
  qed
end
  
end
