theory Refine_Examples
imports Hoare_Examples Refine Refine_Word Code_Export "HOL-Library.RBT"
begin

  subsection \<open>Implementing Specifications by actual Algorithms\<close>
  
  subsubsection \<open>Quicksort\<close>
  text \<open>We implement the partition specification of quicksort using 
    functional list operations. Note: this is less efficient than arrays, 
    but simpler to prove, such that it serves as an example here.\<close>

  definition "partition_fun xs \<equiv> do { 
    assert xs\<noteq>[]; 
    let p = hd xs;
    let xs = tl xs;
    return (filter (\<lambda>x. x\<le>p) xs,p,filter (\<lambda>x. x>p) xs) 
  } "
    
  lemma partition_fun_correct[wp_rule]: 
    "xs=xs' \<Longrightarrow> xs'\<noteq>[] \<Longrightarrow> wp (partition_fun xs) (partition_spec xs')"
    unfolding partition_fun_def partition_spec_def
    apply (wp_step)+
    apply (auto simp: neq_Nil_conv not_less)
    done
  
  text \<open>In the quicksort algorithm, we can 
    now replace \<open>partition_spec\<close> by \<open>partition_fun\<close>.
  \<close>  
  definition "quicksort_fun xs \<equiv> REC (\<lambda>quicksort xs. 
    if length xs < 2 then return xs 
    else do {
      (xs\<^sub>1,p,xs\<^sub>2) \<leftarrow> partition_fun xs;
      xs\<^sub>1 \<leftarrow> quicksort xs\<^sub>1;
      xs\<^sub>2 \<leftarrow> quicksort xs\<^sub>2;
      return xs\<^sub>1@p#xs\<^sub>2
    }
  ) xs"
  
  lemma quicksort_fun_refine: "refines (=) (quicksort_fun xs) (quicksort xs)"
    unfolding quicksort_fun_def quicksort_def
    apply wp
    apply rel
    apply (simp_all)
    by force
    
        
  text \<open>We combine the refinement and the correctness lemma, 
    to get a correctness lemma for the refined program\<close>  
  lemma quicksort_fun_correct[wp_rule]: "wp (quicksort_fun xs) (sort_spec xs)"
  proof -
    note quicksort_fun_refine
    also note quicksort_correct
    finally show ?thesis
      apply (rule refines_SPEC_wpI)
      by simp
  qed    
  
    
    
  text \<open>To export code for recursive functions, we need to 
    supply an unfolding equation to the code generator: \<close>  
  schematic_goal quicksort_fun_unfold[code]: "quicksort_fun xs = ?x"
    unfolding quicksort_fun_def
    apply (subst REC_unfold, wp)
    apply (fold quicksort_fun_def)
    by (rule refl)
  
  value "quicksort_fun [1,2,3::nat,8,1,2,3,4]"  
  export_code quicksort_fun in Scala module_name Quicksort  
  
  
  
  subsection \<open>Implementing Natural Numbers by fixed-width Words\<close>

  subsubsection \<open>Euclid\<close>

  text \<open>It's straightforward to refine Euclid's algorithm to fixed-size numbers,
    as the only operation is subtraction, where no overflow can happen.\<close>
  
  definition "euclid32 (a\<^sub>0::32 word) b\<^sub>0 \<equiv> do {
    (a,b) \<leftarrow> WHILE 
      (\<lambda>(a,b). return a\<noteq>b) 
      (\<lambda>(a,b). if a<b then return (a,b-a) else return (a-b,b))
      (a\<^sub>0,b\<^sub>0);
    return a
  }"
    
  lemma euclid32_refine: "rel_word_nat a a' \<Longrightarrow> rel_word_nat b b' \<Longrightarrow> refines rel_word_nat (euclid32 a b) (euclid a' b')"
    unfolding euclid32_def euclid_def
    apply wp_step+
    apply rel
    apply (simp_all add: word_nat_refine)
    done
    
  export_code euclid32 in SML module_name Euclid
    
  value "euclid32 (150::32 word) 53"
  
  
  subsubsection \<open>Binary Search\<close>
  
  text \<open>Implementations of binary search are prone to a subtle 
    bug, when the addition in the middle computation overflows.
    Note, this bug has been made in 
    reality \<^url>\<open>https://ai.googleblog.com/2006/06/extra-extra-read-all-about-it-nearly.html\<close>.
    Among others, it has gone undetected in the Java standard library 
    for almost a decade!
  \<close>

  text \<open>The following proof attempt illustrates the bug. 
    Quickcheck finds the counterexample for small word sizes, 
    but, the bug is actually present for all word sizes!\<close>
  lemma "LENGTH('a)>1 \<Longrightarrow> unat ((l+h) div 2) = (unat l + unat h) div 2" for l h :: "'a::len word"
    quickcheck
    oops

  text \<open>Here's the counterexample for any word length:\<close>      
  lemma compute_middle_overflow_bug: "\<exists>l h :: 'a::len word. unat ((l+h) div 2) \<noteq> (unat l + unat h) div 2"
    apply (rule exI[where x="2^LENGTH('a) - 1"])
    apply (rule exI[where x="1"])
    apply (auto simp: unat_gt_0)
    done
  
  text \<open>The standard fix to this bug is to slightly rewrite the middle computation, 
    such that the overflow cannot happen:\<close>  
  lemma mid_conv: "l\<le>h \<Longrightarrow> (l+h) div 2 = l + (h-l) div 2" for l h :: nat by simp

  text \<open>The new computation is actually overflow-free\<close>
  lemma rel_word_nat_mid[word_nat_refine]:
    fixes l h :: "'a::len word"
    assumes "2\<le>LENGTH('a)"
    assumes "l'<h'" "rel_word_nat l l'" "rel_word_nat h h'"
    shows "rel_word_nat (l + (h - l) div 2) (l' + (h' - l') div 2)"
    using assms
    apply (intro word_nat_refine)
    apply simp_all
    subgoal using le_less_trans less_exp by auto
    subgoal by (fastforce dest: rel_word_nat_bound)
    done
      
  definition "approx_upper_word l (h::_ word) P \<equiv> do {
    (l,h) \<leftarrow> WHILE 
      (\<lambda>(l,h). return (l<h))
      (\<lambda>(l,h). do {
        let m = l + (h-l) div 2;
        if P m then return (l,m) else return (m+1,h)
      }) (l,h);
  
    return h  
  }"
  

  text \<open>We assume that \<open>P\<close> is implemented correctly for 
    all values \<open><h\<close>. This allows us to prove non-overflow for the 
    implementation of \<open>P\<close>.
  \<close>  
  lemma approx_upper_word_refine[wp_rule]:
    fixes h :: "'a::len word"
    assumes "rel_word_nat l l'" "rel_word_nat h h'" 
    assumes "\<And>i i'. rel_word_nat i i' \<Longrightarrow> i'\<in>{l'..<h'} \<Longrightarrow> P i = P' i'"
    assumes "2\<le>LENGTH('a)"
    shows "refines rel_word_nat (approx_upper_word l h P) (approx_upper l' h' P')"
    unfolding approx_upper_word_def approx_upper_def
    apply wp_step+
    apply rel
    apply (simp_all add: word_nat_refine assms mid_conv pw_simp)
    apply (rule word_nat_refine)
    apply (metis Suc_eq_plus1 less_trans_Suc rel_word_nat_bound)
    apply (simp_all add: word_nat_refine assms pw_simp)
    done

  definition "approx_lower_word l h P \<equiv> do {
    r \<leftarrow> approx_upper_word l h P;
    return (r-1)
  }"  
    
  lemma approx_lower_word_refine[wp_rule]:
    fixes l h :: "'a::len word"
    assumes "rel_word_nat l l'" "rel_word_nat h h'" 
    assumes "\<And>i i'. rel_word_nat i i' \<Longrightarrow> i'\<in>{l'..<h'} \<Longrightarrow> P i = P' i'"
    assumes "2\<le>LENGTH('a)"
    shows "refines rel_word_nat (approx_lower_word l h P) (approx_lower l' h' P')"
    unfolding approx_lower_word_def approx_lower_def
    apply wp_step+
    apply rel
    apply (simp_all add: assms word_nat_refine)
    done
      
    
    
  experiment
  begin
    text \<open> EXERCISE: \<^term>\<open>(l+h) div 2\<close> gives the rounded down value of the average.
      The rounded up value can be obtained by \<^term>\<open>(l+h+1) div 2\<close>.
      Find a way to compute that on fixed-sized words!
    \<close>
    lemma mid_conv': "l<h \<Longrightarrow> (l+h+1) div 2 = l + (h-l-1) div 2 + 1" for l h :: nat by simp
  
    text \<open>The new computation is actually overflow-free\<close>
    lemma rel_word_nat_mid'[word_nat_refine]:
      fixes l h :: "'a::len word"
      assumes "2\<le>LENGTH('a)"
      assumes "l'<h'" "rel_word_nat l l'" "rel_word_nat h h'"
      shows "rel_word_nat (l + (h - l - 1) div 2 + 1) ((l' + h' + 1) div 2)"
      using assms
      apply (subst mid_conv', simp)
      apply (intro word_nat_refine)
      apply simp_all
      subgoal by (meson le_less_trans less_exp)
      apply (auto dest!: rel_word_nat_bound)
      done
  
  end    
    
  subsubsection \<open>Integer Square Root\<close> 
  text \<open>
    We use the implementation of the binary search algorithm to 
    compute integer square roots with fixed width words.
  
    Note: Newton iteration is a more standard method to 
    compute integer square roots. However, the goal here is to 
    demonstrate our binary search algorithm.
    \<close>

  text \<open>Note that the predicate \<open>P=(\<lambda>i. x<i*i)\<close> 
    used for square root computation can overflow.
    
    There may be many fixes to this problem, but we simply observe
    that for \<open>x < 2^w\<close>, we have \<open>sqrt(x) < 2^(w/2)\<close>, 
    which allows us to set the initial upper bound to a value that 
    is guaranteed not to overflow. We moreover assume that \<open>w\<close> is even, 
    such that \<open>w/2\<close> is an integer, and we don't have to handle odd 
    edge cases.
  \<close> 
    
  definition "sqrt' w x \<equiv> approx_lower 0 (min x (2^(w div 2)-1) + 1) (\<lambda>r. r^2 > x)"  

  lemma even_word_len_gt2: "even (LENGTH('a::len)) \<Longrightarrow> Suc 0<LENGTH('a::len)"
    by (metis One_nat_def Suc_lessI len_gt_0 odd_one)
  
  lemma Suc_min_conv: "Suc (min a b) = min (Suc a) (Suc b)" by simp 
  lemma power2_min_conv: "(min a b)\<^sup>2 = min (a\<^sup>2) (b\<^sup>2)" for a b :: nat
    by (auto simp: min_def)
    
    
  text \<open>In order to show implementation of \<open>P\<close>, we have to show that the 
    square computation does not overflow: \<close>
  lemma sqr_bound: fixes i :: nat assumes "i<2^(w div 2)" shows "i\<^sup>2 < 2^w"
  proof -
    have "2^(w div 2) * 2^(w div 2) = (2::nat)^(w div 2 * 2)" 
      by (auto simp flip: power_add)
    also have "w div 2 * 2 \<le> w" by simp
    finally have "(2::nat)^(w div 2) * 2^(w div 2) \<le> 2^w" by simp
    thus ?thesis using assms
      by (smt (z3) leD leI order.trans power2_eq_square power2_nat_le_eq_le)
  qed  
      
  lemma sqrt'_correct: "even w \<Longrightarrow> x<2^w \<Longrightarrow> wp (sqrt' w x) (\<lambda>r. r\<^sup>2 \<le> x \<and> (r+1)\<^sup>2 > x)"
    unfolding sqrt'_def
    apply wp_step+
    apply (auto simp: Suc_min_conv power2_min_conv)
    subgoal by (simp add: less_le_trans monoI)
    subgoal by (simp add: power2_eq_square)
    subgoal by (erule evenE; simp add: power_mult algebra_simps)
    done
      
  definition "sqrt_word (x::'a::len word) \<equiv> approx_lower_word 0 (min x (2^(LENGTH('a) div 2) - 1) + 1) (\<lambda>r. r^2 > x)"
    
  lemma sqrt_word_refines: "even (LENGTH('a)) \<Longrightarrow> rel_word_nat x x' \<Longrightarrow> refines rel_word_nat (sqrt_word (x::'a::len word)) (sqrt' LENGTH('a) x')"
    unfolding sqrt_word_def sqrt'_def
    apply wp_step+
    apply rel
    apply (all \<open>((intro word_nat_refine | assumption)+)?\<close>)
    apply (simp_all add: Suc_min_conv sqr_bound)
    subgoal by (metis le_less_trans length_not_greater_eq_2_iff less_exp odd_one)
    subgoal apply (rule min.strict_coboundedI2) by simp
    subgoal by (metis length_not_greater_eq_2_iff odd_one)
    done  
  

  lemma sqrt_word_correct:
    fixes x :: "'a::len word"
    assumes "even LENGTH('a)"
    shows "wp (sqrt_word x) (\<lambda>r. (unat r)\<^sup>2 \<le> unat x \<and> (unat r + 1)\<^sup>2 > unat x)"
  proof -
  
    (* TODO: Setup trans reasoning globally *)
  
    note [trans] = refines_trans[OF _ refines_SPEC_wp, simplified]
  
    note sqrt_word_refines
    also note sqrt'_correct
    finally show ?thesis
      apply (rule refines_SPEC_wpI)
      apply (auto simp: assms rel_word_nat_def even_word_len_gt2)
      done

  qed  
  
  value "sqrt_word (50::16 word)"
  
  text \<open>Finally, if we specialize the algorithm to a particular word size, 
    we get rid of the assumptions in the correctness lemma, 
    and can even pre-compute the high-bound constant:\<close>
  
  definition "sqrt_word32 \<equiv> sqrt_word :: _ \<Rightarrow> 32 word nres"
  definition "sqrt_word64 \<equiv> sqrt_word :: _ \<Rightarrow> 64 word nres"
  
  lemmas sqrtw32_correct = sqrt_word_correct[where 'a=32, folded sqrt_word32_def, simplified]
  lemmas sqrtw64_correct = sqrt_word_correct[where 'a=64, folded sqrt_word64_def, simplified]
  
  lemma [code]: "sqrt_word32 x = approx_lower_word 0 (min x 65535 + 1) (\<lambda>r. x < r\<^sup>2)"
    unfolding sqrt_word32_def sqrt_word_def
    by simp
  
  lemma [code]: "sqrt_word64 x = approx_lower_word 0 (min x 4294967295 + 1) (\<lambda>r. x < r\<^sup>2)"
    unfolding sqrt_word64_def sqrt_word_def
    by simp
  
  value "sqrt_word32 (26)"
  value "sqrt_word64 (42)"
  
  export_code sqrt_word64 checking SML
    
  
  (*
    TODO: export code to machine words!
  *)
  

  subsection \<open>Implementing Sets and Maps by Efficient Data Structures\<close>
    
  subsubsection \<open>DFS\<close>
  
  text \<open>
    We implement the workset and successor set by a list, and the visited set by a red-black tree. 
    The graph is implemented by a red-black tree, mapping nodes to lists of successor nodes:
  \<close>
  definition "list_mset_rel xs m \<equiv> m = mset xs"  
  
  definition "rbt_set_rel t s \<equiv> s = { x . RBT.lookup t x = Some () }"

  definition "G_succ G u \<equiv> case RBT.lookup G u of None \<Rightarrow> [] | Some xs \<Rightarrow> xs"
  definition "G_rel G E \<equiv> E = {(u,v). v \<in> set (G_succ G u)}"
  
  text \<open>We define operations on the graph\<close>  
  text \<open>Empty graph\<close>
  definition "G_empty = RBT.empty"
  
  text \<open>Add an edge\<close>
  definition "G_add_edge \<equiv> \<lambda>(u,v) G. case RBT.lookup G u of
    None \<Rightarrow> RBT.insert u [v] G
  | Some xs \<Rightarrow> RBT.insert u (v#xs) G"

  text \<open>Convert list of edges to graph\<close>
  definition "G_of_list xs \<equiv> fold G_add_edge xs G_empty"
  
  text \<open>Correctness of operations\<close>  
  lemma G_empty_refine[simp]: "G_rel G_empty {}"
    by (auto simp: rel_def G_rel_def G_succ_def G_empty_def)

  lemma G_succ_refine[wp_rule]: 
    "x = x' \<Longrightarrow> G_rel G E \<Longrightarrow> refines list_mset_rel (return G_succ G x) (SPEC (\<lambda>m. set_mset m = E `` {x'}))"
    apply pw
    by (auto simp: rel_def list_mset_rel_def G_rel_def)
  
  lemma G_add_edge_refine[simp]: "G_rel G E \<Longrightarrow> G_rel (G_add_edge e G) (insert e E)"  
    by (cases e; auto simp: rel_def list_mset_rel_def G_rel_def G_add_edge_def G_succ_def split: option.splits if_splits)
  
  lemma G_of_list_refine[simp]: "G_rel (G_of_list xs) (set xs)"
  proof -
    have "G_rel G E \<Longrightarrow> G_rel (fold G_add_edge xs G) (E \<union> set xs)" for G E
      apply (induction xs arbitrary: G E)
      apply simp_all
      by (metis G_add_edge_refine Un_insert_left)
    from this[of G_empty "{}"] show ?thesis unfolding G_of_list_def
      by simp
  qed
     
  lemma G_succ_refine'[simp]: "G_rel G E \<Longrightarrow> set (G_succ G u) = E``{u}"
    by (auto simp: G_rel_def)
   
  text \<open>The refined DFS algorithm\<close>
  definition "dfs1 G s\<^sub>0 \<equiv> do {
    (V,_) \<leftarrow> WHILE 
      (\<lambda>(V,W). return W\<noteq>[])
      (\<lambda>(V,W). do {
        x \<leftarrow> return hd W;
        let W = tl W;
        if RBT.lookup V x \<noteq> None then return (V,W)
        else do {
          let V = RBT.insert x () V ;
          let W = G_succ G x @ W;
          return (V,W)
        }
      }
      ) (RBT.empty,[s\<^sub>0]);
    return V
  }"

  text \<open>Implementation of a graph\<close>    
  locale fin_graph_impl = fin_graph E for E :: "'a::linorder rel" +
    fixes G :: "('a,'a list) rbt"
    assumes refine[simp]: "G_rel G E"
  begin  

    lemmas [simp] = G_succ_refine'[OF refine]  
  
    lemma "refines rbt_set_rel (dfs1 G s\<^sub>0) dfs"
      unfolding dfs1_def dfs_def
      supply [rel_rule] = rel[of list_mset_rel] rel[of rbt_set_rel]
      apply wp
      apply rel
      apply simp_all
      apply (simp_all add: pw_simp list_mset_rel_def rbt_set_rel_def)
      subgoal by blast
      subgoal by (auto simp: neq_Nil_conv)
      subgoal by (clarsimp simp: neq_Nil_conv)
      done
    
  end
     
  value "do { v \<leftarrow> dfs1 (G_of_list [(0,1::nat),(1,2),(3,2),(3,4),(4,5),(5,1),(0,3)]) 0; return RBT.keys v }"

  text \<open>
    EXERCISE: Show that graphs represented by \<^const>\<open>G_rel\<close> are always finite.
    
    Hint: characterize \<open>E\<close> as subset of domain and range of the tree, and use
      @{thm RBT.finite_dom_lookup finite_ran finite_set finite_Union}
      
    For the range, use \<open>\<Union>\<close> to join all elements of all successor lists. 
  \<close>
  
  lemma assumes "G_rel G E" shows "finite E"
  proof -  
    have "E \<subseteq> dom (RBT.lookup G) \<times> (\<Union> (set`ran (RBT.lookup G)))"
      using assms unfolding G_rel_def G_succ_def
      apply (auto simp: ran_def) by metis+
    also have "finite \<dots>"        
      by (auto simp: finite_ran)
    finally (finite_subset) show ?thesis .
  qed
  
  
end
