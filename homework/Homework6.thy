chapter \<open>Homework 6\<close>
theory Homework6
imports Main
begin
  
  (*
    This file is intended to be viewed within the Isabelle/jEdit IDE.
    In a standard text-editor, it is pretty unreadable!
  *)

  (*
    HOMEWORK #6
    RELEASED: Tue, Dec 21 2021
    DUE:      Tue, Jan 18, 2021, 23:59

    To be submitted via email to p.lammich@utwente.nl.
    Include [ITP-Homework] in the subject line, and make sure to
    use your utwente email address, and/or include your name, 
    such that we can identify the sender.
  *)

section \<open>1: Be creative (80 points + 40 bonus points for creative/elegant/ingenious solutions)\<close>

(*  
  Develop your own piece of interesting theory. 
  Requirement: something must be proved, i.e., 
  just a functional program in Isabelle without correctness proof is not enough!
                
  Ideally, come up with your own idea. Do not copy from existing repositories, 
    we fill find out about this!
  
  You are welcome to send me your idea for discussion/feasibility check.
          
  DEADLINE: 18.01.2022 (you have two full (non-holiday) weeks). 
    But there will be another homework from 12.01, with deadline 18.01, too.
    (We trust in your time-management capabilities ;) )
  
  If you need inspiration what to do, here are a few suggestions:
        
  E.g. some graph/automata problem:
    Add outer loop (fold) to DFS, to explore states from all roots
    Topological sorting
    Make: build recipe
    Critical path in DAG
    Shortest path from s to t (use BFS)
    Find a path from s to some node v with P v
  
  However, if you have some interesting property from a project you are working on 
    that you would like to prove, we highly encourage you to do so now

  Please document your proofs well. Add as many comments as possible so we can
    understand what you are proving. Good documentation will affect the grading
    in a positive way.

  Hint: keep it simple.
*)

end

