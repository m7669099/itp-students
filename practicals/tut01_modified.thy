(*<*)
theory tut01_modified
imports Main "HOL-Library.Multiset"
begin
(*>*)



section \<open>Calculating with natural numbers\<close>

text \<open>
Use the \textbf{value} command to turn Isabelle into
a fancy calculator and evaluate the following natural
number expressions: $2+2$, $2*(5+3)$, $3*4 - 2*(7+1)$
\<close>

text \<open>
Can you explain the last result?
\<close>

term "(+)"

value "(3*4 :: int) - 2*(7+1)"



section \<open>Counting elements of a list\<close>

text \<open>
Define a function \<open>count\<close> which counts the number of occurrences of
a particular element in a list.
\<close>


fun count :: "'a list \<Rightarrow> 'a \<Rightarrow> nat" (* \<leftarrow> add type here! *)
  where 
  "count [] _ = 0" (* \<leftarrow> add equation(s) here! *)
| "count (b#bl) a = count bl a + (if a=b then 1 else 0)"  

definition "count' xs x \<equiv> length (filter (\<lambda>y. x=y) xs)"

lemma "count xs x = count' xs x"
  unfolding count'_def
  apply (induction xs) 
  apply auto
  done

lemma "count (xs@xs') x = count xs x + count xs' x"
  apply (induction xs) 
  apply auto
  done
  

text \<open>
Test your definition of \<open>count\<close> on some examples.
\<close>

value "count [1,2,3,4,1,2,3,4] (13::nat)"


section \<open>Adding elements to the end of a list\<close>

text \<open>
Recall the definition of lists from the lecture.  Define a
function \<open>snoc\<close> that appends an element at the right
end of a list.  Do not use the existing append operator
\<open>@\<close> for lists.
\<close>

fun snoc :: "'a list \<Rightarrow> 'a \<Rightarrow> 'a list" where
  "snoc [] n = [n]"
| "snoc (x#xs) n = x # (snoc xs n)"  


text \<open>
Convince yourself on some test cases that your definition
of \<open>snoc\<close> behaves as expected, for example run:
\<close>

value "snoc [] c"
value "snoc [1,2,3] (4::int)"

text \<open>
Next define a function \<open>reverse\<close> that reverses the
order of elements in a list.  (Do not use the existing
function @{term rev} from the library.)  Hint: Define
the reverse of \<open>x # xs\<close> using the @{term snoc}
function.
\<close>



fun reverse :: "'a list \<Rightarrow> 'a list" where
  "reverse [] = []"
| "reverse (x#xs) = snoc (reverse xs) x"  



text \<open>
Demonstrate that your definition is correct by running some
test cases, and proving that those test cases are correct.
For example:
\<close>

value "reverse [a, b, c]"

lemma [simp]: "reverse (snoc xs x) = x#reverse (xs)"
  by (induction xs) auto

lemma "reverse (reverse xs) = xs"
  by (induction xs) auto

lemma [simp]: "length (snoc xs x) = length xs + 1"
  by (induction xs) auto
    
  
lemma "length (reverse xs) = length xs"  
  apply (induction xs) 
  apply auto
  done
  
lemma [simp]: "snoc (xs@ys) x = xs @ (snoc ys x)"  
  apply (induction xs) 
  apply auto
  done
  
lemma "reverse (xs\<^sub>1 @ xs\<^sub>2) = reverse xs\<^sub>2 @ reverse xs\<^sub>1"
  apply (induction xs\<^sub>1) 
  apply auto
  done
    
  

text \<open>Implement functions take and drop, that take/drop the first n elements of a list. E.g.: 
  take 3 [1,2,3,4,5] = [1,2,3], and drop 3 [1,2,3,4,5] = [4,5]
\<close>

fun mytake :: "nat \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "mytake _ [] = []"
| "mytake 0 _ = []"
| "mytake (Suc n) (x#xs) = x # mytake n xs"  

fun mydrop :: "nat \<Rightarrow> 'a list \<Rightarrow> 'a list" where
  "mydrop _ [] = []"
| "mydrop 0 xs = xs"
| "mydrop (Suc n) (x#xs) = mydrop n xs"  

lemma "length xs - n = length (mydrop n xs)"
  apply (induction n xs rule: mydrop.induct)
  apply auto
  done


lemma "length (mytake n xs) = n" if "n\<le>length xs"
  quickcheck
  oops

lemma "n\<le>length xs \<Longrightarrow> length (mytake n xs) = n"
  apply (induction n xs rule: mytake.induct)
  apply auto
  done

lemma "length (mytake n xs) \<le> length xs"  
  apply (induction n xs rule: mytake.induct)
  apply auto
  done
  
lemma "mytake n xs @ mydrop n xs = xs"
  apply (induction n xs rule: mytake.induct)
  apply auto
  done
  
  
  

text \<open>Implement merge and mergesort (on nat list)\<close>


fun merge :: "nat list \<Rightarrow> nat list \<Rightarrow> nat list" where
  "merge [] ys = ys"
| "merge xs [] = xs"
| "merge (x#xs) (y#ys) = 
    (if x<y then x # merge xs (y#ys) else y # merge (x#xs) ys)"  

lemma "sorted xs \<Longrightarrow> sorted ys \<Longrightarrow> sorted (merge xs ys)"
  quickcheck
  oops  
    
definition "stupid_merge xs ys \<equiv> []"  
  
lemma "sorted xs \<Longrightarrow> sorted ys \<Longrightarrow> sorted (stupid_merge xs ys)"
  quickcheck
  oops    

term mset  

value "mset [1,2,3::nat] = mset [3,2,1,1]"

value "set [1,2,3::nat] = set [3,2,1,1]"

lemma "mset xs = mset ys \<Longrightarrow> set xs = set ys" oops

lemma "set xs = set ys \<Longrightarrow> mset xs = mset ys" quickcheck oops



lemma "mset (merge xs ys) = mset xs + mset ys"  
  quickcheck
  oops
  
lemma "mset (stupid_merge xs ys) = mset xs + mset ys"  
  quickcheck
  oops

definition "Cons' = Cons"  
  
  
term "Cons' a Nil"  


  
fun msort :: "nat list \<Rightarrow> nat list" where
  "msort [] = []"
| "msort [x] = [x]"
| "msort xs = (let n = length xs div 2 in merge 
    (msort (take n xs)) 
    (msort (drop n xs)))"    
    
    
value "msort [1,5,3,4]"  
  

lemma "sorted (msort xs) \<and> mset (msort xs) = mset xs"
  quickcheck
  oops

  
end
